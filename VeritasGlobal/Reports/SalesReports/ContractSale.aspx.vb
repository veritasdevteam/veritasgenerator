﻿Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports xi = Telerik.Web.UI.ExportInfrastructure
Imports System.Web.UI
Imports System.Web
Imports Telerik.Web.UI.GridExcelBuilder
Imports System.Drawing

Public Class ContractSale
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            rdpEnd.SelectedDate = Today
            rdpStart.SelectedDate = DateAdd(DateInterval.Day, -7, Today)
            fillcontracttype
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
        End If
    End Sub

    Private Sub FillContractType()
        cboContractType.Items.Add("All")
        cboContractType.Items.Add("Red Shield")
        cboContractType.Items.Add("Veritas")
        cboContractType.Items.Add("Express Protect")
        cboContractType.Items.Add("AutoNation")
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub rgClaim_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgClaim.ItemCommand
        If e.CommandName = "ExportToExcel" Then
            rgClaim.ExportSettings.ExportOnlyData = False
            rgClaim.ExportSettings.IgnorePaging = True
            rgClaim.ExportSettings.OpenInNewWindow = True
            rgClaim.ExportSettings.UseItemStyles = True
            rgClaim.ExportSettings.Excel.FileExtension = "xlsx"
            rgClaim.ExportSettings.FileName = "R0018"
            rgClaim.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgClaim.MasterTableView.ExportToExcel()
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRunReport_Click(sender As Object, e As EventArgs) Handles btnRunReport.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select contractno, saledate, d.DealerNo, d.DealerName, c.status, cc2.canceldate, a.AgentNo, a.AgentName, d.State as DealerState, c.MoxyDealerCost, c.CustomerCost from contract c "
        SQL = SQL + "inner join dealer d on d.DealerID = c.dealerid "
        SQL = SQL + "left join ContractClip cc on cc.ClipID = c.ClipID "
        SQL = SQL + "left join contractcancel cc2 on c.contractid = cc2.contractid "
        SQL = SQL + "left join Agents a on a.AgentID = c.AgentsID "
        SQL = SQL + "where not ProgramID in (47,48,51,54,55,56,57,58,59,60) "
        If Not rdpStart.SelectedDate Is Nothing Then
            SQL = SQL + "and c.saledate >= '" & rdpStart.SelectedDate & "' "
        End If
        If Not rdpEnd.SelectedDate Is Nothing Then
            SQL = SQL + "and c.saledate <= '" & rdpEnd.SelectedDate & "' "
        End If
        If cboContractType.Text = "Red Shield" Then
            SQL = SQL + "and (contractno like 'r%' or contractno like 'Vel%') "
        End If
        If cboContractType.Text = "Veritas" Then
            SQL = SQL + "and (contractno like 'v%' and not contractno like 'Vel%') "
        End If
        If cboContractType.Text = "Express Protect" Then
            SQL = SQL + "and dealerno like '2%' and dealerno like '%cc' "
        End If
        If cboContractType.Text = "AutoNation" Then
            SQL = SQL + "and dealerno like '2%' and not dealerno like '%cc' "
        End If
        SQL = SQL + "order by SaleDate desc "
        rgClaim.DataSource = clR.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring")).Tables(0)
        rgClaim.Rebind()
    End Sub
End Class