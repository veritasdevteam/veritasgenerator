﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager

Public Class CancellationShift
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetServerInfo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            rdpFrom.SelectedDate = DateAdd(DateInterval.Day, -7, Today)
            rdpTo.SelectedDate = Today
            fillprogram
            FillGrid()
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If

        End If
    End Sub

    Private Sub FillProgram()
        Dim SQL As String
        Dim clR As New clsDBO
        cboProgram.Items.Clear()
        cboProgram.Items.Add("All")
        SQL = "select left(contractno,3) as Program from contract "
        SQL = SQL + "where dealerid = 5193 "
        SQL = SQL + "group by left(contractno,3) "
        SQL = SQL + "order by left(contractno,3) "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                cboProgram.Items.Add(clR.Fields("program"))
            Next
        End If
    End Sub

    Private Sub FillGrid()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select c.ContractID, c.ContractNo, c.SaleDate, c.datepaid, cc.canceleffdate, cc.CancelDate, d.DealerName, "
        SQL = SQL + "c.FName + ' ' + c.LName as Customer, c.LIenHolder, c.MoxyDealerCost, c.CustomerCost, "
        SQL = SQL + "case when d.DealerStatusID = 5 then cc.AdminAmt + cc.DealerAmt else cc.AdminAmt end as AdminAmt, "
        SQL = SQL + "cc.DealerAmt, "
        SQL = SQL + "cc.AdminAmt + cc.DealerAmt as CustomerAmt, cc.cancelfactor from contract c "
        SQL = SQL + "inner join contractcancel cc on cc.ContractID = c.contractid "
        SQL = SQL + "left join dealer d on d.DealerID = c.DealerID "
        SQL = SQL + "where cc.CancelStatus = 'Cancelled' "
        If cboProgram.Text <> "All" Then
            SQL = SQL + "and c.contractno like '" & cboProgram.Text & "%' "
        End If
        SQL = SQL + "and c.dealerid = 5193 "
        SQL = SQL + "and cc.CancelPaidDate is null "
        SQL = SQL + "and canceldate >= '" & Format(rdpFrom.SelectedDate, "M/d/yyyy") & "' "
        SQL = SQL + "and canceldate <= '" & Format(rdpTo.SelectedDate, "M/d/yyyy") & "' "
        rgDealer.DataSource = clR.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        rgDealer.Rebind()
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        FillGrid()
    End Sub
End Class