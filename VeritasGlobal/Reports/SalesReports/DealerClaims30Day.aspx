﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DealerClaims30Day.aspx.vb" Inherits="VeritasGlobal.DealerClaims30Day" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgClaim">
                    <telerik:RadGrid ID="rgClaim" runat="server" AutoGenerateColumns="false" AllowSorting="true" Width="1000">
                        <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true"
                            CommandItemDisplay="TopAndBottom">
                            <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                            <Columns>
                                <telerik:GridBoundColumn DataField="claimid" UniqueName="claimid" Visible="false"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="contractno" UniqueName="contractno" HeaderText="Contract No"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="status" UniqueName="status" HeaderText="status"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="SaleDate" UniqueName="SaleDate" HeaderText="Sale Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Customer" UniqueName="Customer" HeaderText="Customer"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PlanType" UniqueName="PlanType" HeaderText="Plan Type"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="claimno" UniqueName="claimno" HeaderText="Claim No"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="days" UniqueName="days" HeaderText="Days" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Miles" UniqueName="Miles" HeaderText="Miles"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ClaimDetailStatus" UniqueName="ClaimDetailStatus" HeaderText="Claim Status"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PaidAmt" UniqueName="PaidAmt" HeaderText="Paid Amt" DataFormatString="{0:#,##0.00}"></telerik:GridBoundColumn>

                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </telerik:RadAjaxPanel>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfDealerID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
