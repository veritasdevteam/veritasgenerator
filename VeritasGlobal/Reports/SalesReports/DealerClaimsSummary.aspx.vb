﻿Imports System.Configuration.ConfigurationManager

Public Class DealerClaimsSummary
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetServerInfo()
        If Not IsPostBack Then
            hfDealerID.Value = Request.QueryString("dealerid")
            FillPage()
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillPage()
        Dim dEarn As Double
        Dim dClaim As Double
        Dim dLoss As Double
        dEarn = CalcEarn()
        dClaim = CalcClaim()
        dLoss = dClaim / dEarn
        lblContractEarn.Text = Format(dEarn, "#,##0.00")
        lblClaims.Text = Format(dClaim, "#,##0.00")
        lblLossRatio.Text = Format(dLoss, "#,##0.00%")

    End Sub

    Public Function CalcEarn() As Double
        CalcEarn = 0
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select sum(EarnAmt) as EA from contract c
        inner join PlanType pt on pt.PlanTypeID = c.PlanTypeID
        where dealerid = " & hfDealerID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            CalcEarn = clR.Fields("ea")
        End If

    End Function

    Private Function CalcClaim() As Double
        CalcClaim = 0
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select sum(cd.paidamt) as PA from claim cl
        inner join claimdetail cd on cd.claimid = cl.claimid
        inner join contract c on c.contractid = cl.contractid
        inner join ClaimReason cr on cr.ClaimReasonID = cd.ClaimReasonID
        inner join PlanType pt on pt.PlanTypeID = c.PlanTypeID
        where cl.ContractID in (select contractid from contract where dealerid = " & hfDealerID.Value & ") "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            CalcClaim = clR.Fields("pa")
        End If
    End Function
End Class