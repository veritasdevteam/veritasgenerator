﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class GAPCancellationOOB
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SqlDataSource1.ConnectionString = AppSettings("connstring")
        SqlDataSource2.ConnectionString = AppSettings("connstring")
        hfDealerID.Value = 0
        hfAgentID.Value = 0
        If Not IsPostBack Then
            tbTodo.Width = pnlHeader.Width
            GetServerInfo()
            CheckToDo()
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            rdpStartDate.SelectedDate = DateAdd(DateInterval.Day, -7, Today)
            rdpEndDate.SelectedDate = Today
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            pnlAgentSearch.Visible = False
            pnlDealerSearch.Visible = False
            trExport.Visible = False
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccountingReports_Click(sender As Object, e As EventArgs) Handles btnAccountingReports.Click
        Response.Redirect("~/reports/AccountingReports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub rgDealer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgDealer.SelectedIndexChanged
        hfDealerID.Value = rgDealer.SelectedValue
        pnlDealerSearch.Visible = False
        pnlBuildQuery.Visible = True
        pnlReport.Visible = True
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from DEALER "
        SQL = SQL + "where dealerid = " & hfDealerID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtDealerNo.Text = clR.Fields("dealerno")
            txtDealerName.Text = clR.Fields("dealername")
        End If
    End Sub

    Private Sub btnSeekDealer_Click(sender As Object, e As EventArgs) Handles btnSeekDealer.Click
        hfDealerID.Value = 0
        pnlDealerSearch.Visible = True
        pnlBuildQuery.Visible = False
        pnlReport.Visible = False
    End Sub

    Private Sub btnClearDealer_Click(sender As Object, e As EventArgs) Handles btnClearDealer.Click
        pnlDealerSearch.Visible = False
        pnlBuildQuery.Visible = True
        pnlReport.Visible = True
        hfDealerID.Value = 0
        txtDealerName.Text = ""
        txtDealerNo.Text = ""
    End Sub

    Private Sub btnSeekAgent_Click(sender As Object, e As EventArgs) Handles btnSeekAgent.Click
        pnlBuildQuery.Visible = False
        pnlReport.Visible = False
        pnlAgentSearch.Visible = True
    End Sub

    Private Sub btnClearAgents_Click(sender As Object, e As EventArgs) Handles btnClearAgents.Click
        pnlBuildQuery.Visible = True
        pnlReport.Visible = True
        pnlAgentSearch.Visible = False
        hfAgentID.Value = 0
        txtAgentName.Text = ""
        txtAgentNo.Text = ""
    End Sub

    Private Sub rgAgents_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAgents.SelectedIndexChanged
        hfAgentID.Value = rgAgents.SelectedValue
        pnlBuildQuery.Visible = True
        pnlReport.Visible = True
        pnlAgentSearch.Visible = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from agents "
        SQL = SQL + "where agentid = " & hfAgentID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtAgentNo.Text = clR.Fields("agentno")
            txtAgentName.Text = clR.Fields("agentname")
        End If
    End Sub

    Private Sub btnRunReport_Click(sender As Object, e As EventArgs) Handles btnRunReport.Click
        FillGrid()
        trExport.Visible = False
    End Sub

    Private Sub FillGrid()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select contractno, c.fname + ' ' + c.lname as CustomerName, d.DealerName, SaleDate, CancelEffDate, DatePaid, c.CustomerCost, 
                    c.MoxyDealerCost as DealerCost , cc.CancelFeeAmt, cc.CancelFactor * 100 as CancelPer, 
                    cc.AdminAmt, cc.DealerAmt, cc.AdminAmt + cc.DealerAmt as CustAmt, csr.Regulated, p.CompanyName,
					p.Addr1 + ' ' + p.City + ' ' + p.State + ' ' + p.Zip as PayeeAddress,
					cc.Note, '' as CheckNo, '' as MailDate,
                    a.AgentNo, a.AgentName, c.LIenHolder
                    from contract c
                    inner join contractcancel cc on cc.contractid = c.ContractID
                    inner join dealer d on d.DealerID = c.DealerID
                    inner join agents a on a.AgentID = c.AgentsID
					left join CancelStateRules csr on csr.state = c.State
					left join payee p on p.PayeeID = cc.PayeeID
                    where plantypeid in (40, 41,42,43,71,118,127,133,134,135)
                    and dealerstatusid = 5
                    and status = 'Cancelled'  "
        SQL = SQL + "and canceldate >= '" & Format(rdpStartDate.SelectedDate, "M/d/yyyy") & "' "
        SQL = SQL + "and canceldate <= '" & Format(rdpEndDate.SelectedDate, "M/d/yyyy") & "' "
        If hfAgentID.Value > "0" Then
            SQL = SQL + "and c.agentsid > " & hfAgentID.Value & " "
        End If
        If hfDealerID.Value > "0" Then
            SQL = SQL + "and c.dealerid > " & hfDealerID.Value & " "
        End If
        rgCancel.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgCancel.DataBind()
    End Sub

    Private Sub rgCancel_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgCancel.ItemCommand
        If e.CommandName = "ExportToExcel" Then
            Dim SQL As String
            Dim SQL2 As String
            Dim clR As New clsDBO
            Dim clRR As New clsDBO
            SQL = "select * from userinfo "
            SQL = SQL + "where userid = " & hfUserID.Value
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                clR.GetRow()
                SQL = "select * from veritasreports.dbo.reportrequest "
                SQL = SQL + "where idx = 0 "
                clRR.OpenDB(SQL, AppSettings("connstring"))
                If clRR.RowCount = 0 Then
                    clRR.NewRow()
                    SQL2 = "select contractno, c.fname + ' ' + c.lname as CustomerName, d.DealerName, SaleDate, CancelEffDate, DatePaid, c.CustomerCost, 
                    c.MoxyDealerCost as DealerCost , cc.CancelFeeAmt, cc.CancelFactor * 100 as CancelPer, 
                    cc.AdminAmt, cc.DealerAmt, cc.AdminAmt + cc.DealerAmt as CustAmt, csr.Regulated, p.CompanyName,
					p.Addr1 + ' ' + p.City + ' ' + p.State + ' ' + p.Zip as PayeeAddress,
					cc.Note, '' as CheckNo, '' as MailDate,
                    a.AgentNo, a.AgentName, c.LIenHolder
                    from contract c
                    inner join contractcancel cc on cc.contractid = c.ContractID
                    inner join dealer d on d.DealerID = c.DealerID
                    inner join agents a on a.AgentID = c.AgentsID
					left join CancelStateRules csr on csr.state = c.State
					left join payee p on p.PayeeID = cc.PayeeID
                    where plantypeid in (40, 41,42,43,71,118,127,133,134,135)
                    and dealerstatusid = 5
                    and status = 'Cancelled' "
                    SQL2 = SQL2 + "and canceldate >= '" & Format(rdpStartDate.SelectedDate, "M/d/yyyy") & "' "
                    SQL2 = SQL2 + "and canceldate <= '" & Format(rdpEndDate.SelectedDate, "M/d/yyyy") & "' "
                    If hfAgentID.Value > "0" Then
                        SQL2 = SQL2 + "and c.agentsid > " & hfAgentID.Value & " "
                    End If
                    If hfDealerID.Value > "0" Then
                        SQL2 = SQL2 + "and c.dealerid > " & hfDealerID.Value & " "
                    End If
                    clRR.Fields("ReportName") = "GAP Cancellations"
                    clRR.Fields("ReportFile") = "GAPCancellations"
                    clRR.Fields("RequestByName") = clR.Fields("fname") & " " & clR.Fields("lname")
                    clRR.Fields("Requestbyemail") = clR.Fields("email")
                    clRR.Fields("requestdate") = Now
                    clRR.Fields("reportheader") = "contractno, CustomerName, DealerName, SaleDate, CancelEffDate, DatePaid, CustomerCost,DealerCost,CancelFeeAmt,CancelPer,AdminAmt,DealerAmt,CustAmt,Regulated, CompanyName, PayeeAddress,Note,CheckNo,MailDate,AgentNo,AgentName,LIenHolder "
                    clRR.FieldsWith1quote("ReportSQL") = SQL2
                    clRR.AddRow()
                    clRR.SaveDB()
                End If
            End If
        End If
    End Sub

End Class