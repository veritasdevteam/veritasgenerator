﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager

Public Class DealerClaims
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        tbTodo.Width = pnlHeader.Width
        If Not IsPostBack Then
            GetServerInfo()
            CheckToDo()
            pnlDealerSearch.Visible = True
            pnlReport.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub rgDealer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgDealer.SelectedIndexChanged
        pnlReport.Visible = True
        pnlDealerSearch.Visible = False
        hfDealerID.Value = rgDealer.SelectedValue
        pvSummary.ContentUrl = "~\reports\SalesReports\DealerClaimsSummary.aspx?sid=" & hfID.Value & "&dealerid=" & rgDealer.SelectedValue
        pvContract.ContentUrl = "~\reports\SalesReports\DealerClaimsContract.aspx?sid=" & hfID.Value & "&dealerid=" & rgDealer.SelectedValue
        pv30Day.ContentUrl = "~\reports\SalesReports\DealerClaims30Day.aspx?sid=" & hfID.Value & "&dealerid=" & rgDealer.SelectedValue
        pv60Day.ContentUrl = "~\reports\SalesReports\DealerClaims60Day.aspx?sid=" & hfID.Value & "&dealerid=" & rgDealer.SelectedValue
        pv90Plus.ContentUrl = "~\reports\SalesReports\DealerClaims90plus.aspx?sid=" & hfID.Value & "&dealerid=" & rgDealer.SelectedValue
        tsDealer.Tabs(0).Selected = True
        pvSummary.Selected = True
        getdealerinfo
    End Sub

    Private Sub tsDealer_TabClick(sender As Object, e As RadTabStripEventArgs) Handles tsDealer.TabClick
        If tsDealer.SelectedTab.Value = "Summary" Then
            pvSummary.Selected = True
        End If
        If tsDealer.SelectedTab.Value = "Contract" Then
            pvContract.Selected = True
        End If
        If tsDealer.SelectedTab.Value = "30Day" Then
            pv30Day.Selected = True
        End If
        If tsDealer.SelectedTab.Value = "60Day" Then
            pv60Day.Selected = True
        End If
        If tsDealer.SelectedTab.Value = "90Plus" Then
            pv90Plus.Selected = True
        End If
    End Sub

    Private Sub GetDealerInfo()
        Dim SQL As String
        Dim clD As New clsDBO
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & hfDealerID.Value
        clD.OpenDB(SQL, AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
            lblDealerNo.Text = clD.Fields("dealerno")
            lblDealerName.Text = clD.Fields("dealername")
        End If

    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        pnlDealerSearch.Visible = True
        pnlReport.Visible = False
        rgDealer.Rebind()
    End Sub
End Class