﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportR0002.aspx.vb" Inherits="VeritasGlobal.ReportR0002" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="Veritas.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>R0002</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1a4688" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSalesReports" runat="server" Text="Sales Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsReports" runat="server" Text="Claims Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccountingReports" runat="server" Text="Accounting Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCustomReports" runat="server" Text="Custom Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" runat="server" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    Customer State:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txtCustState" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Dealer No:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txtDealerNo" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Dealer Name:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txtDealerName" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    VIN:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txtVIN" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Make:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txtMake" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Model:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txtModel" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    Sold Date Start:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadDatePicker ID="rdpSoldStart" runat="server"></telerik:RadDatePicker>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Sold Date End:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadDatePicker ID="rdpSoldEnd" runat="server"></telerik:RadDatePicker>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Paid Date Start:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadDatePicker ID="rdpPaidDate" runat="server"></telerik:RadDatePicker>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Paid Date End:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadDatePicker ID="rdpPaidEnd" runat="server"></telerik:RadDatePicker>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Button ID="btnClear" runat="server" Text="Clear Search" />
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Button ID="btnSearch" runat="server" Text="Search" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    Record Count:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txtRecCnt" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgReport">
                                            <telerik:RadGrid ID="rgReport" runat="server" AutoGenerateColumns="false" AllowSorting="true" DataSourceID="dsSales">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView DataKeyNames="ContractID" AutoGenerateColumns="false" AllowSorting="true" CommandItemDisplay="Top" DataSourceID="dsSales">
                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToCsvButton="true"/>
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ContractID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CustomerID" HeaderText="Customer ID" UniqueName="CustomerID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CustomerFName" HeaderText="First Name" UniqueName="CustomerFName"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CustomerLName" HeaderText="Last Name" UniqueName="CustomerLName"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CustomerAddr1" HeaderText="Addr 1" UniqueName="CustomerAddr1"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CustomerAddr2" HeaderText="Addr 2" UniqueName="CustomerAddr2"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CustomerCity" HeaderText="City" UniqueName="CustomerCity"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CustomerState" HeaderText="State" UniqueName="CustomerState"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CustomerZip" HeaderText="Zip" UniqueName="CustomerZip"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CustomerPhone" HeaderText="Phone" UniqueName="CustomerPhone"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AgentName" HeaderText="Agent Name" UniqueName="AgentName"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="MGA" HeaderText="MGA" UniqueName="MGA"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerNo" HeaderText="Dealer No" UniqueName="DealerNo"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerName" HeaderText="Dealer Name" UniqueName="DealerName"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerCost" HeaderText="Dealer Cost" UniqueName="DealerCost"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CustomerCost" HeaderText="Customer Cost" UniqueName="CustomerCost"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Markup" HeaderText="Markup" UniqueName="Markup"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="TotalSurcharge" HeaderText="Total Surcharge" UniqueName="TotalSurcharge"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AgentFee" HeaderText="Agent Fee" UniqueName="AgentFee"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClassFee" HeaderText="Class Fee" UniqueName="ClassFee"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PPA" HeaderText="PPA" UniqueName="PPA"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PPAFee" HeaderText="PPA Fee" UniqueName="PPAFee"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="SoldDate" HeaderText="Sold Date" UniqueName="SoldDate" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Deductible" HeaderText="Deductible" UniqueName="Deductible"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="VSCPlan" HeaderText="VSC Plan" UniqueName="VSCPlan"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="TermMonth" HeaderText="Term Month" UniqueName="TermMonth"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="TermMile" HeaderText="Term Mile" UniqueName="TermMile"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="FINTerm" HeaderText="FINTerm" UniqueName="FINTerm"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DecForm" HeaderText="Dec Form" UniqueName="DecForm"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="TC" HeaderText="Term / Condition" UniqueName="TC"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ContractNo" HeaderText="Contract No" UniqueName="ContractNo"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Class" HeaderText="Class" UniqueName="Class"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="VIN" HeaderText="VIN" UniqueName="VIN"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Make" HeaderText="Make" UniqueName="Make"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Model" HeaderText="Model" UniqueName="Model"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Trim" HeaderText="Trim" UniqueName="Trim"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="surDiesel" HeaderText="Diesel" UniqueName="surDiesel"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="sur4WD" HeaderText="4WD" UniqueName="sur4WD"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="surTurbo" HeaderText="Turbo" UniqueName="surTurbo"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="surHybrid" HeaderText="Hybrid" UniqueName="surHybrid"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="surCommercial" HeaderText="Commercial" UniqueName="surCommercial"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="surHydraulic" HeaderText="Hydraulic" UniqueName="surHydraulic"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="surAirBladder" HeaderText="Air Bladder" UniqueName="surAirBladder"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="surLiftKit" HeaderText="Lift Kit" UniqueName="surLiftKit"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="surLiftKit78" HeaderText="Lift Kit 78" UniqueName="surLiftKit78"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="surLuxury" HeaderText="Luxury" UniqueName="surLuxury"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="surSeals" HeaderText="Seals" UniqueName="surSeals"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="surSnowPlow" HeaderText="Snow Plow" UniqueName="surSnowPlow"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealStatus" HeaderText="Deal Status" UniqueName="DealStatus"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PaidDate" HeaderText="Paid Date" UniqueName="PaidDate"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PaidStatus" HeaderText="Paid Status" UniqueName="PaidStatus"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="BasePrice" HeaderText="Base Price" UniqueName="BasePrice"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DateAdded" HeaderText="Date Added" UniqueName="DateAdded"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DateUpdated" HeaderText="DateUpdated" UniqueName="DateUpdated"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Program" HeaderText="Program" UniqueName="Program"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="SaleMiles" HeaderText="Sale Miles" UniqueName="SaleMiles"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="EffDate" HeaderText="Eff Date" UniqueName="EffDate"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="EffMile" HeaderText="Eff Mile" UniqueName="EffMile"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="CancelDate" HeaderText="Cancel Date" UniqueName="CancelDate"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AgentNo" HeaderText="Agent No" UniqueName="AgentNo"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PlanCode" HeaderText="Plan Code" UniqueName="PlanCode"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DatePaid" HeaderText="Paid Date" UniqueName="DatePaid" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                        </telerik:RadAjaxPanel>
                                        <asp:SqlDataSource ID="dsSales" runat="server"></asp:SqlDataSource>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfSQL" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
    </form>
</body>
</html>
