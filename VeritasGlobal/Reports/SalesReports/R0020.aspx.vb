﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class R0020
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        dsDealerType.ConnectionString = AppSettings("reportstring")
        dsFilterBy.ConnectionString = AppSettings("reportstring")
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            rdpEnd.SelectedDate = Today
            rdpStart.SelectedDate = Today
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
        End If

        If hfError.Value = "Visible" Then
            rwError.VisibleOnPageLoad = True
        Else
            rwError.VisibleOnPageLoad = False
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccountingReports_Click(sender As Object, e As EventArgs) Handles btnAccountingReports.Click
        Response.Redirect("~/reports/AccountingReports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select c.contractid, contractno, fname + ' ' + lname as Customer, vin, cast(year as nvarchar(4)) + ' ' + make + ' ' + model as vehicle, p.programname + ' ' + pt.plantype,  "
        SQL = SQL + "cast(termmonth as nvarchar(4)) + '/' + cast(c.TermMile as nvarchar(10)) as term, saledate, c.datepaid, cc.canceldate, moxydealercost as cost, d.dealerno, d.dealername, a.agentno, a.agentname "
        SQL = SQL + "from contract c "
        SQL = SQL + "inner join dealer d on d.dealerid = c.dealerid "
        SQL = SQL + "inner join agents a on a.agentid = c.agentsid "
        SQL = SQL + "left join contractcancel cc on cc.contractid = c.contractid "
        SQL = SQL + "left join program p on p.programid = c.programid "
        SQL = SQL + "left join plantype pt on pt.plantypeid = c.plantypeid "
        SQL = SQL + "where c.contractid > 0 "
        If cboDealerGroup.SelectedValue = 2 Then
            SQL = SQL + "and not dealerno like '2%' "
        End If
        If cboDealerGroup.SelectedValue = 3 Then
            SQL = SQL + "and dealerno like '2%' "
            SQL = SQL + "and not dealerno like '%cc' "
        End If
        If cboDealerGroup.SelectedValue = 4 Then
            SQL = SQL + "and dealerno like '%cc' "
        End If
        If cboFilterBy.SelectedValue = 1 Then
            SQL = SQL + "and saledate >= '" & rdpStart.SelectedDate & "' "
            SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, 1, CDate(rdpEnd.SelectedDate)) & "' "
        End If
        If cboFilterBy.SelectedValue = 2 Then
            SQL = SQL + "and datepaid >= '" & rdpStart.SelectedDate & "' "
            SQL = SQL + "and datepaid < '" & DateAdd(DateInterval.Day, 1, CDate(rdpEnd.SelectedDate)) & "' "
        End If
        If cboFilterBy.SelectedValue = 3 Then
            SQL = SQL + "and canceldate >= '" & rdpStart.SelectedDate & "' "
            SQL = SQL + "and canceldate < '" & DateAdd(DateInterval.Day, 1, CDate(rdpEnd.SelectedDate)) & "' "
        End If
        dsSales.ConnectionString = AppSettings("connstring")
        dsSales.SelectCommand = SQL

        rgSales.Rebind()

    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Dim SQL As String
        Dim SQLTemp As String
        Dim clR As New clsDBO
        SQLTemp = "select c.contractid, contractno, fname + ' ' + lname as Customer, vin, cast(year as nvarchar(4)) + ' ' + make + ' ' + model as vehicle, p.programname + ' ' + pt.plantype as ProgramPlan,  "
        SQLTemp = SQLTemp + "cast(termmonth as nvarchar(4)) + '/' + cast(c.TermMile as nvarchar(10)) as term, saledate, c.datepaid, cc.canceldate, moxydealercost as cost, d.dealerno, d.dealername, a.agentno, a.agentname "
        SQLTemp = SQLTemp + "from contract c "
        SQLTemp = SQLTemp + "inner join dealer d on d.dealerid = c.dealerid "
        SQLTemp = SQLTemp + "inner join agents a on a.agentid = c.agentsid "
        SQLTemp = SQLTemp + "left join contractcancel cc on cc.contractid = c.contractid "
        SQLTemp = SQLTemp + "left join program p on p.programid = c.programid "
        SQLTemp = SQLTemp + "left join plantype pt on pt.plantypeid = c.plantypeid "
        SQLTemp = SQLTemp + "where c.contractid > 0 "
        If cboDealerGroup.SelectedValue = 2 Then
            SQLTemp = SQLTemp + "and not dealerno like '2%' "
        End If
        If cboDealerGroup.SelectedValue = 3 Then
            SQLTemp = SQLTemp + "and dealerno like '2%' "
            SQLTemp = SQLTemp + "and not dealerno like '%cc' "
        End If
        If cboDealerGroup.SelectedValue = 4 Then
            SQLTemp = SQLTemp + "and dealerno like '%cc' "
        End If
        If cboFilterBy.SelectedValue = 1 Then
            SQLTemp = SQLTemp + "and saledate >= '" & rdpStart.SelectedDate & "' "
            SQLTemp = SQLTemp + "and saledate < '" & DateAdd(DateInterval.Day, 1, CDate(rdpEnd.SelectedDate)) & "' "
        End If
        If cboFilterBy.SelectedValue = 2 Then
            SQLTemp = SQLTemp + "and datepaid >= '" & rdpStart.SelectedDate & "' "
            SQLTemp = SQLTemp + "and datepaid < '" & DateAdd(DateInterval.Day, 1, CDate(rdpEnd.SelectedDate)) & "' "
        End If
        If cboFilterBy.SelectedValue = 3 Then
            SQLTemp = SQLTemp + "and canceldate >= '" & rdpStart.SelectedDate & "' "
            SQLTemp = SQLTemp + "and canceldate < '" & DateAdd(DateInterval.Day, 1, CDate(rdpEnd.SelectedDate)) & "' "
        End If
        SQL = "select * from veritasreports.dbo.reportrequest "
        SQL = SQL + "where idx = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("reportname") = "Sales Report"
            clR.Fields("reportfile") = "salesreport.csv"
            clR.Fields("requestbyname") = GetUserInfo(hfUserID.Value)
            clR.Fields("requestbyemail") = GetUserEmail(hfUserID.Value)
            clR.Fields("requestdate") = Now
            clR.Fields("reportheader") = "contractid,contractno,Customer,vin,vehicle,ProgramPlan,term,saledate,datepaid,canceldate,cost,dealerno, dealername, agentno, agentname"
            clR.FieldsWith1quote("reportsql") = SQLTemp
            clR.AddRow()
            clR.SaveDB()
        End If
    End Sub
End Class