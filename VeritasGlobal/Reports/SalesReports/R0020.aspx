﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="R0020.aspx.vb" Inherits="VeritasGlobal.R0020" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSalesReports" runat="server" Text="Sales Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsReports" runat="server" Text="Claims Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccountingReports" runat="server" Text="Accounting Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCustomReports" runat="server" Text="Custom Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" runat="server" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel runat="server">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Dealer Group:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:DropDownList ID="cboDealerGroup" DataSourceID="dsDealerType" DataTextField="DealerType" DataValueField="DealerTypeID" runat="server"></asp:DropDownList>
                                                        <asp:SqlDataSource ID="dsDealerType" ProviderName="System.Data.SqlClient"
                                                            SelectCommand="select dealertypeid, dealertype from dealertype order by dealertypeid" runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Filter By:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:DropDownList ID="cboFilterBy" DataSourceid="dsFilterBy" DataTextField="filterby" DataValueField="filterbyid" runat="server"></asp:DropDownList>
                                                        <asp:SqlDataSource ID="dsFilterBy" ProviderName="System.Data.SqlClient"
                                                            SelectCommand="select filterbyid, filterby from filterby order by filterbyid" runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Start Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadDatePicker ID="rdpStart" runat="server"></telerik:RadDatePicker>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        End Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadDatePicker ID="rdpEnd" runat="server"></telerik:RadDatePicker>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Dealer Group:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnSubmit" runat="server" Text="Run Report" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right">
                                            <asp:Button ID="btnExport" runat="server" Text="Export" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" PostBackControls="rgSales">
                                            <telerik:RadGrid ID="rgSales" runat="server" AutoGenerateColumns="false" DataSourceID="dsSales"
                                                AllowSorting="true" AllowPaging="true" ShowFooter="true"  MasterTableView-CommandItemDisplay="TopAndBottom"
                                                    Font-Names="Calibri" Font-Size="Small" AllowMultiRowSelection="true">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" Width="1500" DataKeyNames="ContractID">
                                                    <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="false" ShowAddNewRecordButton="false" />
                                                    <Columns>
                                                            <telerik:GridBoundColumn DataField="ContractID"  ReadOnly="true" Visible="false" UniqueName="ContractID"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ContractNo" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" UniqueName="ContractNo" AllowFiltering="True" HeaderText="Customer" ItemStyle-Width="100" HeaderStyle-Width ="100"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Customer" UniqueName="Customer" HeaderText="Dealer Name" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="300" HeaderStyle-Width="300"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="VIN" UniqueName="VIN" HeaderText="VIN" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Vehicle" UniqueName="Vehicle" AllowFiltering="true" HeaderText="Vehicle" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Coverage" UniqueName="Coverage" AllowFiltering="true" HeaderText="Coverage" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Term" UniqueName="Term" AllowFiltering="true" HeaderText="Term" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="SaleDate" UniqueName="SaleDate" HeaderText="SaleDate" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="150" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PaidDate" UniqueName="paidDate" HeaderText="PaidDate" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="150" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CancelDate" UniqueName="CancelDate" HeaderText="CancelDate" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="150" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Cost" UniqueName="Cost" HeaderText="Cost" AllowFiltering="false" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="DealerNo" UniqueName="DealerNo" HeaderText="Dealer No" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="DealerName" UniqueName="DealerName" HeaderText="Dealer Name" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AgentNo" UniqueName="AgentNo" HeaderText="Agent No" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AgentName" UniqueName="AgentName" HeaderText="Agent Name" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings>
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                        </telerik:RadAjaxPanel>
                                                <asp:SqlDataSource ID="dsSales" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
    </form>
</body>
</html>
