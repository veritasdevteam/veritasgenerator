﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OpenedClaimsV2.aspx.vb" Inherits="VeritasGlobal.OpenedClaimsV2" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSalesReports" runat="server" Text="Sales Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsReports" runat="server" Text="Claims Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccountingReports" runat="server" Text="Accounting Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCustomReports" runat="server" Text="Custom Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" runat="server" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel runat="server" ID="pnlBuildQuery">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Dealer No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtDealerNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Dealer Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtDealerName" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnSeekDealer" BackColor="#1eabe2" runat="server" Text="..." />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Agent No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtAgentNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Agent Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtAgentName" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnSeekAgent" BackColor="#1eabe2" runat="server" Text="..." />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Start Date:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDatePicker ID="rdpStartDate" runat="server"></telerik:RadDatePicker>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            End Date:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDatePicker ID="rdpEndDate" runat="server"></telerik:RadDatePicker>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnRunReport" runat="server" Text="Run Report" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trExport">
                                        <asp:TableCell>
                                            <asp:Label ID="lblExport" runat="server" ForeColor="Green" Text="Report will be sent within 15 min."></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlReport">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadGrid ID="rgClaim" runat="server" AutoGenerateColumns="false" AllowSorting="true" DataSourceID="dsClaimDailyPayment" Width="1000">
                                                <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" CommandItemDisplay="TopAndBottom">
                                                    <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" />
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="StartDate" UniqueName="StartDate" HeaderText="Start Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="SaleDate" UniqueName="SaleDate" HeaderText="Sale Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerNo" UniqueName="DealerNo" HeaderText="Dealer No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerName" UniqueName="DealerName" HeaderText="Dealer Name"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AgentNo" UniqueName="AgentNo" HeaderText="Agent No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AgentName" UniqueName="AgentName" HeaderText="Agent Name"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="UserName" UniqueName="UserName" HeaderText="Open By"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="dsClaimDailyPayment" 
                                                runat="server">
                                                <selectparameters>
                                                    <asp:controlparameter name="StartDate" controlid="hfStartDate" propertyname="Value"/>
                                                    <asp:controlparameter name="EndDate" controlid="hfEndDate" propertyname="Value"/>
                                                </selectparameters>
                                            </asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlDealerSearch">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnClearDealer" runat="server" Text="Clear Dealer Search" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadGrid ID="rgDealer" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="SQLDataSource1">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="DealerID" PageSize="10" ShowFooter="true">
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="DealerID"  ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerNo" FilterCheckListWebServiceMethod="LoadDealerNo" UniqueName="DealerNo" HeaderText="Dealer No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerName" FilterCheckListWebServiceMethod="LoadDealerName" UniqueName="DealerName" HeaderText="Dealer Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="City" FilterCheckListWebServiceMethod="LoadCity" UniqueName="DealerCity" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="State" FilterCheckListWebServiceMethod="LoadState" UniqueName="AgentState" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AgentName" FilterCheckListWebServiceMethod="LoadAgentsName" UniqueName="AgentName" HeaderText="Agent Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="SubAgentName" FilterCheckListWebServiceMethod="LoadSubAgentName" UniqueName="SubAgentName" HeaderText="Sub Agent Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="SqlDataSource1"
                                            ProviderName="System.Data.SqlClient" SelectCommand="select dealerid, dealerno, dealername, d.city, d.state, a.agentname, 
                                            ay.subagentname from dealer d 
                                            left join agents a on d.agentsid = a.agentid 
                                            left join subagents ay on d.subagentid = ay.subAgentID" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlAgentSearch">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnClearAgents" runat="server" Text="Clear Agent Search" BackColor="#1eabe2" BorderColor="#1eabe2" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadGrid ID="rgAgents" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                            AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="SQLDataSource2">
                                            <GroupingSettings CaseSensitive="false" />
                                            <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="AgentID" PageSize="10" ShowFooter="true">
                                                <Columns>
                                                    <telerik:GridBoundColumn DataField="AgentID" ReadOnly="true" Visible="false" UniqueName="AgentsID"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="AgentNo" UniqueName="AgentsNo" FilterCheckListWebServiceMethod="LoadAgentsNo" HeaderText="Agents No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="AgentName" UniqueName="AgentsName" FilterCheckListWebServiceMethod="LoadAgentsName" HeaderText="Agents Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="City" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" FilterCheckListWebServiceMethod="LoadCity" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="State" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" FilterCheckListWebServiceMethod="State" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                </Columns>
                                            </MasterTableView>
                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                <Selecting AllowRowSelect="true" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                        <asp:SqlDataSource ID="SqlDataSource2"
                                        ProviderName="System.Data.SqlClient" 
                                        SelectCommand="select Agentid, Agentno, Agentname, city, state from Agents "
                                        runat="server"></asp:SqlDataSource>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfStartDate" runat="server" />
                <asp:HiddenField ID="hfEndDate" runat="server" />
                <asp:HiddenField ID="hfDealerID" runat="server" />
                <asp:HiddenField ID="hfAgentID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
    </form>
</body>
</html>
