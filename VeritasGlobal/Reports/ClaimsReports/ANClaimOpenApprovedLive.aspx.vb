﻿Imports Telerik.Web.UI

Public Class ANClaimOpenApprovedLive
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            rgClaim.Rebind()
            RunQuery()
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub rgClaim_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgClaim.ItemCommand
        If e.CommandName = "ExportToExcel" Then
            rgClaim.ExportSettings.ExportOnlyData = False
            rgClaim.ExportSettings.IgnorePaging = True
            rgClaim.ExportSettings.OpenInNewWindow = True
            rgClaim.ExportSettings.UseItemStyles = True
            rgClaim.ExportSettings.Excel.FileExtension = "xlsx"
            rgClaim.ExportSettings.FileName = "R0032"
            rgClaim.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgClaim.MasterTableView.ExportToExcel()
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        RunQuery()
    End Sub

    Private Sub RunQuery()
        Dim SQL As String
        Dim clU As New clsDBO
        Dim clR As New clsDBO
        SQL = "select * from userinfo ui "
        SQL = SQL + "inner join usersecurityinfo usi on ui.userid = usi.userid "
        SQL = SQL + "where ui.userid = " & hfUserID.Value
        clU.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clU.RowCount > 0 Then
            clU.GetRow()

        End If
        SQL = "select cl.ClaimID, d.dealerno, DealerName, contractno, c.FName, c.lname, vin, claimno, cl.CreDate as Reportdate, 
            cl.Moddate as lastmaint, case when ca.ActivityDesc = 'Denied' then 'Approved - Waiting on Invoice' else ca.ActivityDesc end as ActivityDesc, vcdsta.totalamt as AmtDue,  a.agentname,
			case when vcd.cnt is null then 0 else vcd.cnt end as ClaimAge, 
            case when vna.cnt is null then 0 else vna.cnt end as noactivity,
			sc.ServiceCenterName, cl.OpenDate, 
			ci.RequestDate as InspectionRequest,
			um.MessageDate as InspectionComplete
			from claim cl
            inner join contract c on c.contractid = cl.ContractID
            inner join dealer d on c.DealerID = d.DealerID
			inner join claimdetail cd on cd.claimid = cl.Claimid
            left join agents a on d.agentsid = a.agentid
			left join vwClaimAgeClosed vcd on cl.ClaimID = vcd.claimid
			left join vwNoActivityClosed vna on cl.claimid = vna.claimid
            left join ClaimActivity ca on ca.ClaimActivityID = cl.ClaimActivityID
            left join vwClaimDetailSumTotalAmt as VCDSTA on vcdsta.claimid = cl.claimid
            left join ServiceCenter sc on cl.ServiceCenterID = sc.ServiceCenterID
			left join vwClaimInspection ci on ci.ClaimID = cl.ClaimID and not InspectionID is null
			left join UserMessage um on ci.InspectionID = um.InspectionID "
        If clU.Fields("autonationonly") Then
            SQL = SQL + "where ((d.dealerno like '2%'
            and cl.status = 'denied'
            and (cd.ClaimDetailStatus = 'authorized'
            or cd.ClaimDetailStatus = 'approved')
            and not cd.LossCode like 'if%') "
            SQL = SQL + "or (not cl.CloseDate is null
            and cl.ClaimActivityID in (11,15,17,21,22,23,24,25,26,32,40,44)
            and cl.Status = 'Open'
            and cl.claimid in (select claimid from vwClaimDetailRequested)
			and sc.dealerno like '2%'))
            group by cl.ClaimID, d.dealerno, DealerName, contractno, c.FName, c.lname, vin, claimno, cl.CreDate, 
            cl.Moddate, case when ca.ActivityDesc = 'Denied' then 'Approved - Waiting on Invoice' else ca.ActivityDesc end, vcdsta.totalamt,  a.agentname,
			case when vcd.cnt is null then 0 else vcd.cnt end, 
            case when vna.cnt is null then 0 else vna.cnt end,
			sc.ServiceCenterName, cl.OpenDate, 
			ci.RequestDate,
			um.MessageDate            order by ClaimAge desc"
        Else
            SQL = SQL + "where not cl.CloseDate is null
            and cl.ClaimActivityID in (11,15,17,21,22,23,24,25,26,32,40,44)
            and cl.Status = 'Open'
            and cl.claimid in (select claimid from vwClaimDetailRequested)
			and sc.dealerno like '2%'
            order by ClaimAge desc"
        End If
        rgClaim.DataSource = clR.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring")).Tables(0)
        rgClaim.Rebind()
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        lblRecordCount.Text = clR.RowCount
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub
End Class