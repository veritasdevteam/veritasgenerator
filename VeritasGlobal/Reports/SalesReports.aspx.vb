﻿Public Class SalesReports
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
        End If

        If hfError.Value = "Visible" Then
            rwError.VisibleOnPageLoad = True
        Else
            rwError.VisibleOnPageLoad = False
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("phonereports") = True Then
                btnPhoneReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
        btnPhoneReports.Enabled = False
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub rgSalesReport_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgSalesReport.SelectedIndexChanged
        If rgSalesReport.SelectedValue = 1 Then
            Response.Redirect("~/reports/salesreports/reportR0001.aspx?sid=" & hfID.Value & "&ReportID=" & rgSalesReport.SelectedValue)
        End If
        If rgSalesReport.SelectedValue = 2 Then
            Response.Redirect("~/reports/salesreports/reportR0002.aspx?sid=" & hfID.Value & "&ReportID=" & rgSalesReport.SelectedValue)
        End If
        If rgSalesReport.SelectedValue = 1003 Then
            Response.Redirect("~/reports/salesreports/r0004.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1004 Then
            Response.Redirect("~/reports/salesreports/r0005.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1005 Then
            Response.Redirect("~/reports/salesreports/r0006.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1009 Then
            Response.Redirect("~/reports/salesreports/r0010.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1013 Then
            Response.Redirect("~/reports/salesreports/CancellationsV2.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1016 Then
            Response.Redirect("~/reports/salesreports/PaidCancel.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1019 Then
            Response.Redirect("~/reports/salesreports/r0020.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1023 Then
            Response.Redirect("~/reports/salesreports/ContractSale.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1024 Then
            Response.Redirect("~/reports/salesreports/ContractsPaid.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1025 Then
            Response.Redirect("~/reports/salesreports/ContractBucket.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1050 Then
            Response.Redirect("~/reports/salesreports/CancellationShift.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1070 Then
            Response.Redirect("~/reports/salesreports/FandIReport.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1071 Then
            Response.Redirect("~/reports/salesreports/DealerClaims.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1073 Then
            Response.Redirect("~/reports/salesreports/DealerNoProduction.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1075 Then
            Response.Redirect("~/reports/salesreports/GapCancellation.aspx?sid=" & hfID.Value)
        End If
        If rgSalesReport.SelectedValue = 1076 Then
            Response.Redirect("~/reports/salesreports/GapCancellationOOB.aspx?sid=" & hfID.Value)
        End If
    End Sub


    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccountingReports_Click(sender As Object, e As EventArgs) Handles btnAccountingReports.Click
        Response.Redirect("~/reports/AccountingReports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnPhoneReports_Click(sender As Object, e As EventArgs) Handles btnPhoneReports.Click
        Response.Redirect("~/reports/phonereports.aspx?sid=" & hfID.Value)
    End Sub

End Class
