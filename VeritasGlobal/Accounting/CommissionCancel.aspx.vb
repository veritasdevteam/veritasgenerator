﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class CommissionCancel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        dsCommissionCancelType.ConnectionString = AppSettings("connstring")
        SqlDataSource1.ConnectionString = AppSettings("connstring")
        SqlDataSource2.ConnectionString = AppSettings("connstring")
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnCommission_Click(sender As Object, e As EventArgs) Handles btnCommission.Click
        Response.Redirect("~/accounting/Commission.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnPaylink_Click(sender As Object, e As EventArgs) Handles btnPaylink.Click
        Response.Redirect("~/accounting/Paylink.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAutoNationACH_Click(sender As Object, e As EventArgs) Handles btnAutoNationACH.Click
        Response.Redirect("~/accounting/autonationach.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnWex_Click(sender As Object, e As EventArgs) Handles btnWex.Click
        Response.Redirect("~/accounting/wex.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccountAdj_Click(sender As Object, e As EventArgs) Handles btnAccountAdj.Click
        Response.Redirect("~/accounting/AccountAdjustments.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnCommissionCancel_Click(sender As Object, e As EventArgs) Handles btnCommissionCancel.Click
        Response.Redirect("~/accounting/CommissionCancel.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub cboCommissionCancelType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCommissionCancelType.SelectedIndexChanged
        FillCommission()
        FillPayment()
    End Sub

    Private Sub FillCommission()
        Dim SQL As String
        Dim clR As New clsDBO
        If cboCommissionCancelType.SelectedValue = 1 Then
            SQL = ""
            SqlDataSource1.SelectCommand = SQL
            rgCommission.Rebind()
        End If
        If cboCommissionCancelType.SelectedValue = 2 Then
            SQL = "select distinct c.contractid, c.ContractNo,d.DealerNo,d.DealerName,c.FName + ' '+c.LName as customerName, c.SaleDate,c.DatePaid, cc.CancelEffDate, cc.CancelDate, c.MoxyDealerCost,ca.CancelAmt * -1 as Amt,a.AgentName,a.Addr1,a.Addr2,a.City + ',' + a.State + ',' +a.Zip as addr3 
			from Veritas.dbo.Contract c
            left join Veritas.dbo.Dealer d on d.DealerID = c.DealerID 
            left join Veritas.dbo.Agents a on a.agentID = c.agentsID 
			left join veritas.dbo.contractamt ca on ca.ContractID = c.ContractID
			left join veritas.dbo.ContractCancel cc on cc.ContractID = c.ContractID
            where ca.rateTypeId = 3047
            and c.contractno like '%an%' 
            and c.status = 'Cancelled'
			and cc.CancelStatus = 'Cancelled'
            and not c.contractno in (select contractno from CommissionSheet where IsCancelled <> 0)
            and not c.ContractID in (select contractid from commissioncancel)
			and ca.CancelAmt <> 0
            order by cc.CancelDate desc "
            SqlDataSource1.SelectCommand = SQL
            rgCommission.Rebind()
        End If
        If cboCommissionCancelType.SelectedValue = 3 Then
            SQL = "select distinct c.contractid, c.ContractNo,d.DealerNo,d.DealerName,c.FName + ' '+c.LName as customerName, c.SaleDate,c.DatePaid, cc.CancelEffDate, cc.CancelDate, c.MoxyDealerCost,ca.CancelAmt * -1 as Amt,a.AgentName,a.Addr1,a.Addr2,a.City + ',' + a.State + ',' +a.Zip as addr3 
			from Veritas.dbo.Contract c
            left join Veritas.dbo.Dealer d on d.DealerID = c.DealerID 
            left join Veritas.dbo.Agents a on a.agentID = c.agentsID 
			left join veritas.dbo.contractamt ca on ca.ContractID = c.ContractID
			left join veritas.dbo.ContractCancel cc on cc.ContractID = c.ContractID
            where ca.rateTypeId = 3047
            and not c.contractno like '%an%' and not c.contractno like '%EP%'  
            and c.status = 'Cancelled'
			and cc.CancelStatus = 'Cancelled'
            and not c.contractno in (select contractno from CommissionSheet where IsCancelled <> 0)
            and not c.ContractID in (select contractid from commissioncancel)
			and ca.CancelAmt <> 0
            order by cc.CancelDate desc "
            SqlDataSource1.SelectCommand = SQL
            rgCommission.Rebind()
        End If
        If cboCommissionCancelType.SelectedValue = 4 Then
            SQL = "select distinct c.contractid, c.ContractNo,d.DealerNo,d.DealerName,c.FName + ' '+c.LName as customerName, c.SaleDate,c.DatePaid, cc.CancelEffDate, cc.CancelDate, c.MoxyDealerCost,ca.CancelAmt * -1 as Amt,a.AgentName,a.Addr1,a.Addr2,a.City + ',' + a.State + ',' +a.Zip as addr3 
			from Veritas.dbo.Contract c
            left join Veritas.dbo.Dealer d on d.DealerID = c.DealerID 
            left join Veritas.dbo.Agents a on a.agentID = c.agentsID 
			left join veritas.dbo.contractamt ca on ca.ContractID = c.ContractID
			left join veritas.dbo.ContractCancel cc on cc.ContractID = c.ContractID 
            where c.AgentsID = 36 
            and rateTypeId = 13 
            and c.contractno like '%EP%'  
            and c.status = 'Cancelled'
            and not c.contractno in (select contractno from CommissionSheet where IsCancelled <> 0)
            and not c.ContractID in (select contractid from commissioncancel)
            order by cc.CancelDate desc "
            SqlDataSource1.SelectCommand = SQL
            rgCommission.Rebind()
        End If
        If cboCommissionCancelType.SelectedValue = 5 Then
            SQL = "select distinct c.contractid, c.ContractNo,d.DealerNo,d.DealerName,c.FName + ' '+c.LName as customerName, c.SaleDate,c.DatePaid, cc.CancelEffDate, cc.CancelDate, c.MoxyDealerCost,ca.CancelAmt * -1 as Amt,a.AgentName,a.Addr1,a.Addr2,a.City + ',' + a.State + ',' +a.Zip as addr3 
			from Veritas.dbo.Contract c
            left join Veritas.dbo.Dealer d on d.DealerID = c.DealerID 
            left join Veritas.dbo.Agents a on a.agentID = c.agentsID 
			left join veritas.dbo.contractamt ca on ca.ContractID = c.ContractID
			left join veritas.dbo.ContractCancel cc on cc.ContractID = c.ContractID
            where c.AgentsID = 36 
            and rateTypeId = 13 
            and not c.contractno like '%an%' and not c.contractno like '%EP%' 
            and c.status = 'Cancelled'
            and not c.contractno in (select contractno from Veritas.dbo.CommissionSheet where IsCancelled <> 0)
            and not c.ContractID in (select contractid from Veritas.dbo.commissioncancel)
            order by cc.CancelDate desc "
            SqlDataSource1.SelectCommand = SQL
            rgCommission.Rebind()
        End If
        If cboCommissionCancelType.SelectedValue = 6 Then
            SQL = "select distinct c.contractid, c.ContractNo,d.DealerNo,d.DealerName,c.FName + ' '+c.LName as customerName, c.SaleDate,c.DatePaid, cc.CancelEffDate, cc.CancelDate, c.MoxyDealerCost,ca.CancelAmt * -1 as Amt,a.AgentName,a.Addr1,a.Addr2,a.City + ',' + a.State + ',' +a.Zip as addr3 
			from Veritas.dbo.Contract c
            left join Veritas.dbo.Dealer d on d.DealerID = c.DealerID 
            left join Veritas.dbo.Agents a on a.agentID = c.agentsID 
			left join veritas.dbo.contractamt ca on ca.ContractID = c.ContractID
			left join veritas.dbo.ContractCancel cc on cc.ContractID = c.ContractID
            where c.contractno like '%an%' 
            and rateTypeId = 2907
            and c.status = 'Cancelled'
            and not c.contractno in (select contractno from Veritas.dbo.CommissionSheet where IsCancelled <> 0)
            and not c.ContractID in (select contractid from Veritas.dbo.commissioncancel)
            order by cc.CancelDate desc "
            SqlDataSource1.SelectCommand = SQL
            rgCommission.Rebind()
        End If


    End Sub

    Private Sub rgCommission_PageIndexChanged(sender As Object, e As GridPageChangedEventArgs) Handles rgCommission.PageIndexChanged
        FillCommission()
        FillPayment()
    End Sub

    Private Sub btnMoveToPayment_Click(sender As Object, e As EventArgs) Handles btnMoveToPayment.Click
        Dim gdi As Telerik.Web.UI.GridDataItem

        For Each gdi In rgCommission.SelectedItems
            AddPayment(gdi.GetDataKeyValue("ContractID"))
        Next
        FillCommission()
        fillpayment
    End Sub

    Private Sub FillPayment()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select contractid, ContractNo,DealerNo,DealerName,customerName, SaleDate,DatePaid,CancelEffDate,CancelDate,MoxyDealerCost,Amt,AgentName,Addr1,Addr2,addr3 
			from Veritas.dbo.commissioncancel "
        SQL = SQL + "where commissioncanceltypeid = " & cboCommissionCancelType.SelectedValue
        SqlDataSource2.SelectCommand = SQL
        rgPayment.Rebind()
    End Sub

    Private Sub AddPayment(xID As Long)
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clP As New clsDBO
        SQL = "select distinct c.contractid, c.ContractNo,d.DealerNo,d.DealerName,c.FName + ' '+c.LName as customerName, c.SaleDate,c.DatePaid, cc.CancelEffDate, cc.CancelDate, c.MoxyDealerCost,ca.CancelAmt * -1 as Amt,a.AgentName,a.Addr1,a.Addr2,a.City + ',' + a.State + ',' +a.Zip as addr3 
			from Veritas.dbo.Contract c
            left join Veritas.dbo.Dealer d on d.DealerID = c.DealerID 
            left join Veritas.dbo.Agents a on a.agentID = c.agentsID 
			left join veritas.dbo.contractamt ca on ca.ContractID = c.ContractID
			left join veritas.dbo.ContractCancel cc on cc.ContractID = c.ContractID "
        SQL = SQL + "where c.contractid = " & xID
        If cboCommissionCancelType.SelectedValue = 2 Then
            SQL = SQL + "and ca.rateTypeId = 3047"
        End If
        If cboCommissionCancelType.SelectedValue = 3 Then
            SQL = SQL + "and ca.rateTypeId = 3047 "
        End If
        If cboCommissionCancelType.SelectedValue = 4 Then
            SQL = SQL + "and rateTypeId = 13 "
        End If
        If cboCommissionCancelType.SelectedValue = 5 Then
            SQL = SQL + "and rateTypeId = 13 "
        End If
        If cboCommissionCancelType.SelectedValue = 6 Then
            SQL = SQL + "and rateTypeId = 2907 "
        End If
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            SQL = "select * from commissioncancel "
            SQL = SQL + "where commissioncanceltypeid = " & cboCommissionCancelType.SelectedValue & " "
            SQL = SQL + "and contractid = " & xID
            clP.OpenDB(SQL, AppSettings("connstring"))
            If clP.RowCount > 0 Then
                clP.GetRow()
            Else
                clP.NewRow()
                clP.Fields("commissioncanceltypeid") = cboCommissionCancelType.SelectedValue
                clP.Fields("contractid") = xID
            End If
            clP.Fields("contractno") = clR.Fields("contractno")
            clP.Fields("dealerno") = clR.Fields("dealerno")
            clP.Fields("dealername") = clR.Fields("dealername")
            clP.Fields("customername") = clR.Fields("customername")
            clP.Fields("saledate") = clR.Fields("saledate")
            clP.Fields("datepaid") = clR.Fields("datepaid")
            clP.Fields("canceleffdate") = clR.Fields("canceleffdate")
            clP.Fields("canceldate") = clR.Fields("canceldate")
            clP.Fields("moxydealercost") = clR.Fields("moxydealercost")
            clP.Fields("amt") = clR.Fields("amt")
            clP.Fields("agentname") = clR.Fields("agentname")
            clP.Fields("addr1") = clR.Fields("addr1")
            clP.Fields("addr2") = clR.Fields("addr2")
            clP.Fields("addr3") = clR.Fields("addr3")
            If clP.RowCount = 0 Then
                clP.AddRow()
            End If
            clP.SaveDB()
        End If
    End Sub

    Private Sub rgPayment_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgPayment.ItemCommand
        If e.CommandName = "ExportToCsv" Then
            rgPayment.ExportSettings.ExportOnlyData = False
            rgPayment.ExportSettings.IgnorePaging = True
            rgPayment.ExportSettings.OpenInNewWindow = True
            rgPayment.ExportSettings.UseItemStyles = True
            'rgDealer.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgPayment.MasterTableView.ExportToCSV()
        End If
        If e.CommandName = "DeleteRow" Then
            DeletePayment(rgPayment.Items(e.Item.ItemIndex).GetDataKeyValue("ContractID"))
            FillCommission()
            FillPayment()
        End If
    End Sub

    Private Sub DeletePayment(xID As Long)
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "delete commissioncancel "
        SQL = SQL + "where contractid = " & xID & " "
        SQL = SQL + "and commissioncanceltypeid = " & cboCommissionCancelType.SelectedValue
        clR.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub
End Class