﻿Imports Telerik.Web.UI

Public Class Commission
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsAgent.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        tbTodo.Width = pnlHeader.Width
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            pnlAgentSearch.Visible = False
            FillCycleMonth()
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub FillCycleMonth()
        Dim SQL As String
        Dim clCM As New clsDBO
        SQL = "select cast(month(finaldate) as nvarchar(2)) + '/' + cast(day(finaldate) as nvarchar(2)) + '/' + cast(year(finaldate) as nvarchar(4))as FinalDateout from contractcommissions "
        SQL = SQL + "group by finaldate "
        SQL = SQL + "order by finaldate desc "
        cboCycleMonth.DataSource = clCM.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        cboCycleMonth.DataTextField = "FinalDateOut"
        cboCycleMonth.DataValueField = "FinalDateOut"
        cboCycleMonth.DataBind()
        cboCycleMonth.Items.Insert(0, "")
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnCommission_Click(sender As Object, e As EventArgs) Handles btnCommission.Click
        Response.Redirect("~/accounting/Commission.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgentSearch_Click(sender As Object, e As EventArgs) Handles btnAgentSearch.Click
        pnlAgentSearch.Visible = True
        pnlCommission.Visible = False
    End Sub

    Private Sub btnAgentClose_Click(sender As Object, e As EventArgs) Handles btnAgentClose.Click
        pnlAgentSearch.Visible = False
        pnlCommission.Visible = True
    End Sub

    Private Sub rgAgent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAgent.SelectedIndexChanged
        pnlAgentSearch.Visible = False
        pnlCommission.Visible = True
        hfAgentID.Value = rgAgent.SelectedValue
        txtAgent.Text = GetAgentInfo(rgAgent.SelectedValue)

    End Sub

    Private Sub btnGetCommission_Click(sender As Object, e As EventArgs) Handles btnGetCommission.Click
        FillGrid()
    End Sub

    Private Sub FillGrid()
        Dim SQL As String
        Dim clR As New clsDBO

        If Not chkCancel.Checked Then
            SQL = "select cc.prefinal, cc.contractcommissionid, c.contractno, a.agentno, a.agentname, rt.ratetypename, cc.amt  "
            SQL = SQL + "from contractcommissions cc "
            SQL = SQL + "inner join contract c on c.contractid = cc.contractid "
            SQL = SQL + "inner join agents a on a.agentid = cc.agentid "
            SQL = SQL + "inner join RateType rt on cc.RateTypeID = rt.RateTypeID "
            SQL = SQL + "where a.agentid = " & hfAgentID.Value & " "
            SQL = SQL + "and c.status = 'Paid' "
            If cboCycleMonth.Text.Length > 0 Then
                SQL = SQL + "and finaldate = '" & cboCycleMonth.Text & "' "
            Else
                SQL = SQL + "and finaldate is null "
            End If
        Else
            SQL = "select cc.prefinal, cc.contractcommissionid, c.contractno, a.agentno, a.agentname, rt.ratetypename, cc.cancelamt  "
            SQL = SQL + "from contractcommissions cc "
            SQL = SQL + "inner join contract c on c.contractid = cc.contractid "
            SQL = SQL + "inner join agents a on a.agentid = cc.agentid "
            SQL = SQL + "inner join RateType rt on cc.RateTypeID = rt.RateTypeID "
            SQL = SQL + "where a.agentid = " & hfAgentID.Value & " "
            SQL = SQL + "and not finaldate is null "
            SQL = SQL + "and cancelfinaldate is null "
            SQL = SQL + "and c.status = 'Cancelled' "
            If cboCycleMonth.Text.Length > 0 Then
                SQL = SQL + "and finaldate = '" & cboCycleMonth.Text & "' "
            End If
        End If
        rgCommission.DataSource = clR.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        rgCommission.DataBind()
    End Sub

    Private Sub rgCommission_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgCommission.SelectedIndexChanged
        Dim a As String
        a = "test"
    End Sub

    Protected Sub chkPreFinal_CheckedChanged(sender As Object, e As EventArgs)
        Dim GridItem As Telerik.Web.UI.GridDataItem
        Dim chk As CheckBox
        Dim sTotal As Double
        For Each GridItem In rgCommission.MasterTableView.Items
            chk = GridItem.FindControl("chkPreFinal")
            If chk.Checked Then
                sTotal = sTotal + CDbl(GridItem.Item("Amt").Text)
                'AddCheckToCommission(GridItem.Item("ContractCommission").Text)
            Else
                'RemoveCheckToCommission(GridItem.Item("ContractCommission").Text)
            End If
        Next
        txtTotalAmt.Text = Format(sTotal, "#,###.00")
    End Sub

    Protected Sub headerPreFinal_CheckedChanged(sender As Object, e As EventArgs)
        Dim GridItem As Telerik.Web.UI.GridDataItem
        Dim chk As CheckBox
        Dim chkHeader As CheckBox
        Dim sTotal As Double
        chkHeader = sender
        For Each GridItem In rgCommission.MasterTableView.Items
            chk = GridItem.FindControl("chkPreFinal")
            If chkHeader.Checked Then
                chk.Checked = True
                AddCheckToCommission(GridItem.Item("ContractCommission").Text)
            Else
                chk.Checked = False
                RemoveCheckToCommission(GridItem.Item("ContractCommission").Text)
            End If
            If chk.Checked Then
                sTotal = sTotal + CDbl(GridItem.Item("Amt").Text)
            End If
        Next
        txtTotalAmt.Text = Format(sTotal, "#,###.00")
    End Sub

    Private Sub AddCheckToCommission(xCommissionID As Long)
        Dim SQL As String
        Dim clCC As New clsDBO
        SQL = "select * from contractcommissions "
        SQL = SQL + "where contractcommissionid = " & xCommissionID
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount > 0 Then
            clCC.GetRow()
            If chkCancel.Checked Then
                clCC.Fields("prefinalcancel") = True
            Else
                clCC.Fields("prefinal") = True
            End If
            clCC.SaveDB()
        End If
    End Sub

    Private Sub RemoveCheckToCommission(xCommissionID As Long)
        Dim SQL As String
        Dim clCC As New clsDBO
        SQL = "select * from contractcommissions "
        SQL = SQL + "where contractcommissionid = " & xCommissionID
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount > 0 Then
            clCC.GetRow()
            If chkCancel.Checked Then
                clCC.Fields("prefinalcancel") = False
            Else
                clCC.Fields("prefinal") = False
            End If
            clCC.SaveDB()
        End If
    End Sub

    Private Sub btnAddCycle_Click(sender As Object, e As EventArgs) Handles btnAddCycle.Click
        Dim SQL As String
        Dim clCC As New clsDBO
        Dim clCC2 As New clsDBO
        Dim cnt As Long
        If rdpCycleDate.SelectedDate Is Nothing Then
            Exit Sub
        End If
        SQL = "select * from contractcommissions "
        SQL = SQL + "where agentid = " & hfAgentID.Value & " "
        SQL = SQL + "and prefinal = 1 "
        SQL = SQL + "and finaldate is null "
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount > 0 Then
            For cnt = 0 To clCC.RowCount - 1
                clCC.GetRowNo(cnt)
                SQL = "select * from contractcommissions "
                SQL = SQL + "where contractcommissionid = " & clCC.Fields("contractcommissionid")
                clCC2.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                If clCC2.RowCount > 0 Then
                    clCC2.GetRow()
                    clCC2.Fields("finaldate") = rdpCycleDate.SelectedDate
                    clCC2.SaveDB()
                End If
            Next
        End If
        SQL = "select * from contractcommissions "
        SQL = SQL + "where agentid = " & hfAgentID.Value & " "
        SQL = SQL + "and prefinalcancel = 1 "
        SQL = SQL + "and cancelfinaldate is null "
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount > 0 Then
            For cnt = 0 To clCC.RowCount - 1
                clCC.GetRowNo(cnt)
                SQL = "select * from contractcommissions "
                SQL = SQL + "where contractcommissionid = " & clCC.Fields("contractcommissionid")
                clCC2.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                If clCC2.RowCount > 0 Then
                    clCC2.GetRow()
                    clCC2.Fields("cancelfinaldate") = rdpCycleDate.SelectedDate
                    clCC2.SaveDB()
                End If
            Next
        End If
    End Sub

    Private Sub rgCommission_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgCommission.ItemCommand
        Dim SQL As String
        Dim clR As New clsDBO
        Dim item As GridEditableItem = e.Item
        If e.CommandName = "Remove" Then
            SQL = "update contractcommissions "
            If chkCancel.Checked Then
                SQL = SQL + "set cancelfinaldate = null "
            Else
                SQL = SQL + "set finaldate = null "
            End If
            SQL = SQL + "where contractcommissionid = " & item("ContractCommissionID").Text
            clR.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            rgCommission.Rebind()
            FillGrid()
        End If
    End Sub

    Private Sub btnPaylink_Click(sender As Object, e As EventArgs) Handles btnPaylink.Click
        Response.Redirect("~/accounting/Paylink.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnWex_Click(sender As Object, e As EventArgs) Handles btnWex.Click
        Response.Redirect("~/accounting/wex.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccountAdj_Click(sender As Object, e As EventArgs) Handles btnAccountAdj.Click
        Response.Redirect("~/accounting/AccountAdjustments.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnCommissionCancel_Click(sender As Object, e As EventArgs) Handles btnCommissionCancel.Click
        Response.Redirect("~/accounting/CommissionCancel.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub
End Class