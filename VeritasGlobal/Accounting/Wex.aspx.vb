﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI.Calendar

Public Class Wex
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsWexAcct.ConnectionString = AppSettings("connstring")
        dsWexTransfer.ConnectionString = AppSettings("connstring")
        dsPaid.ConnectionString = AppSettings("connstring")
        dsPending.ConnectionString = AppSettings("connstring")
        dsVoid.ConnectionString = AppSettings("connstring")
        tbTodo.Width = pnlHeader.Width
        If Not IsPostBack Then
            GetServerInfo()
            CheckToDo()
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            FillTotals()
            pnlAdd.Visible = False
            pnlPaid.Visible = False
            rdpTransferDate.SelectedDate = Today
            txtTransferAmt.Text = 0
            cboWexAcct.SelectedValue = 1
            hfWexAPIID.Value = 1
            pnlTransferList.Visible = False
            pnlPending.Visible = False
            pnlVoid.Visible = False
        End If

    End Sub

    Private Sub FillTotals()
        GetTotalTransfer()
        GetTotalPaid()
        GetTotalPending()
        GetTotalVoid()
        txtBalance.Text = Format(CDbl(txtDeposit.Text) - CDbl(txtPaid.Text) - CDbl(txtPending.Text), "#,##0.00")
    End Sub

    Private Sub GetTotalVoid()
        Dim SQL As String
        Dim clWA As New clsDBO
        SQL = "select sum(cast(paymentamt as money)) as Amt  from ClaimWexPayment cwp "
        SQL = SQL + "inner join wexapi wa on wa.CompanyNo = cwp.CompanyNo "
        SQL = SQL + "where reasoncode = 'success' "
        SQL = SQL + "and not VoidDate is null "
        SQL = SQL + "and paiddate is null "
        If cboWexAcct.SelectedValue.Length > 0 Then
            SQL = SQL + "and wexapiid = " & cboWexAcct.SelectedValue
        Else
            SQL = SQL + "and wexapiid = 1 "
        End If
        clWA.OpenDB(SQL, AppSettings("connstring"))
        If clWA.RowCount > 0 Then
            clWA.GetRow()
            If clWA.Fields("amt").Length > 0 Then
                txtVoid.Text = Format(CDbl(clWA.Fields("amt")), "#,##0.00")
            Else
                txtVoid.Text = Format(0, "#,##0.00")
            End If
        End If
    End Sub

    Private Sub GetTotalPending()
        Dim SQL As String
        Dim clWA As New clsDBO
        SQL = "select sum(cast(paymentamt as money)) as Amt  from ClaimWexPayment cwp "
        SQL = SQL + "inner join wexapi wa on wa.CompanyNo = cwp.CompanyNo "
        SQL = SQL + "where reasoncode = 'success' "
        SQL = SQL + "and paiddate is null "
        SQL = SQL + "and VoidDate is null "
        If cboWexAcct.SelectedValue.Length > 0 Then
            SQL = SQL + "and wexapiid = " & cboWexAcct.SelectedValue
        Else
            SQL = SQL + "and wexapiid = 1 "
        End If
        clWA.OpenDB(SQL, AppSettings("connstring"))
        If clWA.RowCount > 0 Then
            clWA.GetRow()
            If clWA.Fields("amt").Length > 0 Then
                txtPending.Text = Format(CDbl(clWA.Fields("amt")), "#,##0.00")
            Else
                txtPending.Text = Format(0, "#,##0.00")
            End If
        End If

    End Sub

    Private Sub GetTotalPaid()
        Dim SQL As String
        Dim clWA As New clsDBO
        SQL = "select sum(cast(paymentamt as money)) as Amt  from ClaimWexPayment cwp "
        SQL = SQL + "inner join wexapi wa on wa.CompanyNo = cwp.CompanyNo "
        SQL = SQL + "where reasoncode = 'success' "
        SQL = SQL + "and not paiddate is null "
        If cboWexAcct.SelectedValue.Length > 0 Then
            SQL = SQL + "and wexapiid = " & cboWexAcct.SelectedValue
        Else
            SQL = SQL + "and wexapiid = 1 "
        End If
        clWA.OpenDB(SQL, AppSettings("connstring"))
        If clWA.RowCount > 0 Then
            clWA.GetRow()
            If clWA.Fields("amt").Length > 0 Then
                txtPaid.Text = Format(CDbl(clWA.Fields("amt")), "#,##0.00")
            Else
                txtPaid.Text = Format(0, "#,##0.00")
            End If
        End If
    End Sub

    Private Sub GetTotalTransfer()
        Dim SQL As String
        Dim clWT As New clsDBO
        SQL = "select sum(amt) as Amt from wextransfer "
        If cboWexAcct.SelectedValue.Length > 0 Then
            SQL = SQL + "where wexapiid = " & cboWexAcct.SelectedValue
        Else
            SQL = SQL + "where wexapiid = 1 "
        End If
        clWT.OpenDB(SQL, AppSettings("connstring"))
        If clWT.RowCount > 0 Then
            clWT.GetRow()
            If clWT.Fields("amt").Length > 0 Then
                txtDeposit.Text = Format(CDbl(clWT.Fields("amt")), "#,##0.00")
            Else
                txtDeposit.Text = Format(0, "#,##0.00")
            End If
        End If

    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnCommission_Click(sender As Object, e As EventArgs) Handles btnCommission.Click
        Response.Redirect("~/accounting/Commission.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnPaylink_Click(sender As Object, e As EventArgs) Handles btnPaylink.Click
        Response.Redirect("~/accounting/Paylink.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAutoNationACH_Click(sender As Object, e As EventArgs) Handles btnAutoNationACH.Click
        Response.Redirect("~/accounting/autonationach.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnWex_Click(sender As Object, e As EventArgs) Handles btnWex.Click
        Response.Redirect("~/accounting/wex.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub cboWexAcct_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboWexAcct.SelectedIndexChanged
        hfWexAPIID.Value = cboWexAcct.SelectedValue
        FillTotals()
        rgWexTransfer.Rebind()
        rgPaid.Rebind()
        rgPending.Rebind()
        rgVoid.Rebind()
    End Sub

    Private Sub btnAddDeposit_Click(sender As Object, e As EventArgs) Handles btnAddDeposit.Click
        txtTransferAmt.Text = ""
        pnlAdd.Visible = True
        pnlTotal.Visible = False
        hfAdd.Value = "True"
        hfWexTransferID.Value = 0
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        If hfAdd.Value = "True" Then
            pnlTotal.Visible = True
        Else
            pnlTransferList.Visible = True
        End If
        pnlAdd.Visible = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clWT As New clsDBO
        SQL = "select * from wextransfer "
        SQL = SQL + "where wextransferid = " & hfWexTransferID.Value
        clWT.OpenDB(SQL, AppSettings("connstring"))
        If clWT.RowCount > 0 Then
            clWT.GetRow()
        Else
            clWT.NewRow()
        End If
        clWT.Fields("wexapiid") = cboWexAcct.SelectedValue
        If Not rdpTransferDate.SelectedDate Is Nothing Then
            clWT.Fields("transferdate") = rdpTransferDate.SelectedDate
        End If
        If txtTransferAmt.Text.Length > 0 Then
            clWT.Fields("amt") = txtTransferAmt.Text
        End If
        If clWT.RowCount = 0 Then
            clWT.AddRow()
        End If
        clWT.SaveDB()

        If hfAdd.Value = "True" Then
            pnlTotal.Visible = True
        Else
            pnlTransferList.Visible = True
        End If
        pnlAdd.Visible = False
    End Sub

    Private Sub btnViewDeposit_Click(sender As Object, e As EventArgs) Handles btnViewDeposit.Click
        pnlTotal.Visible = False
        pnlTransferList.Visible = True
    End Sub

    Private Sub btnTransferClose_Click(sender As Object, e As EventArgs) Handles btnTransferClose.Click
        pnlTotal.Visible = True
        pnlTransferList.Visible = False
    End Sub

    Private Sub rdpTransferDate_SelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs) Handles rdpTransferDate.SelectedDateChanged
        hfAdd.Value = "False"
        pnlTransferList.Visible = False
        pnlAdd.Visible = True
        hfWexTransferID.Value = rgWexTransfer.SelectedValue
        FillTransferDetail()
    End Sub

    Private Sub FillTransferDetail()
        Dim SQL As String
        Dim clWT As New clsDBO
        SQL = "select * from wextransfer "
        SQL = SQL + "where wextransferid = " & hfWexTransferID.Value
        clWT.OpenDB(SQL, AppSettings("connstring"))
        If clWT.RowCount > 0 Then
            clWT.GetRow()
            rdpTransferDate.SelectedDate = CDate(clWT.Fields("transferdate"))
            txtTransferAmt.Text = Format(CDbl(clWT.Fields("amt")), "#,##.00")
        End If
    End Sub

    Private Sub btnViewPaid_Click(sender As Object, e As EventArgs) Handles btnViewPaid.Click
        pnlPaid.Visible = True
    End Sub

    Private Sub btnPaidClose_Click(sender As Object, e As EventArgs) Handles btnPaidClose.Click
        pnlPaid.Visible = False
    End Sub

    Private Sub btnViewPending_Click(sender As Object, e As EventArgs) Handles btnViewPending.Click
        pnlPending.Visible = True
    End Sub

    Private Sub btnPendingClose_Click(sender As Object, e As EventArgs) Handles btnPendingClose.Click
        pnlPending.Visible = False
    End Sub

    Private Sub btnViewVoid_Click(sender As Object, e As EventArgs) Handles btnViewVoid.Click
        pnlVoid.Visible = True
    End Sub

    Private Sub btnVoidClose_Click(sender As Object, e As EventArgs) Handles btnVoidClose.Click
        pnlVoid.Visible = False
    End Sub

    Private Sub btnAccountAdj_Click(sender As Object, e As EventArgs) Handles btnAccountAdj.Click
        Response.Redirect("~/accounting/AccountAdjustments.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnCommissionCancel_Click(sender As Object, e As EventArgs) Handles btnCommissionCancel.Click
        Response.Redirect("~/accounting/CommissionCancel.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub
End Class