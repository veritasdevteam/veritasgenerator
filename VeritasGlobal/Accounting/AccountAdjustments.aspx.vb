﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class AccountAdjustments
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsAdj.ConnectionString = AppSettings("connstring")
        dsRateType.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            pnlDetail.Visible = False
            pnlSearch.Visible = False
            GetServerInfo()
            rgAdj.Rebind()
        End If


    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = DateTime.Today
        sEndDate = DateAdd("d", 1, DateTime.Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnCommission_Click(sender As Object, e As EventArgs) Handles btnCommission.Click
        Response.Redirect("~/accounting/Commission.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnPaylink_Click(sender As Object, e As EventArgs) Handles btnPaylink.Click
        Response.Redirect("~/accounting/Paylink.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAutoNationACH_Click(sender As Object, e As EventArgs) Handles btnAutoNationACH.Click
        Response.Redirect("~/accounting/autonationach.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnWex_Click(sender As Object, e As EventArgs) Handles btnWex.Click
        Response.Redirect("~/accounting/wex.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccountAdj_Click(sender As Object, e As EventArgs) Handles btnAccountAdj.Click
        Response.Redirect("~/accounting/AccountAjustments.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        pnlList.Visible = False
        pnlDetail.Visible = True
        txtAmt.Text = 0
        rdpAdjDate.Clear()
        txtDesc.Text = ""
        txtRateTypeDesc.Text = ""
        hfAAID.Value = 0
    End Sub

    Private Sub rgAdj_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAdj.SelectedIndexChanged
        pnlList.Visible = False
        pnlDetail.Visible = True
        Dim SQL As String
        Dim clR As New clsDBO
        hfAAID.Value = rgAdj.SelectedValue
        SQL = "select * from accountadjustments "
        SQL = SQL + "where idx = " & rgAdj.SelectedValue
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfRateTypeID.Value = clR.Fields("ratetypeid")
            rdpAdjDate.SelectedDate = clR.Fields("adjdate")
            txtAmt.Text = clR.Fields("amount")
            txtDesc.Text = clR.Fields("description")
            getratedesc
        End If
    End Sub

    Private Sub GetRateDesc()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from ratetype "
        SQL = SQL + "where ratetypeid = " & hfRateTypeID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtRateTypeDesc.Text = clR.Fields("ratetypename")
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        If hfAAID.Value.Length = 0 Then
            hfAAID.Value = 0
        End If
        SQL = "select * from accountadjustments "
        SQL = SQL + "where idx = " & hfAAID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("moddate") = Today
            clR.Fields("modby") = hfUserID.Value
        Else
            clR.NewRow()
            clR.Fields("credate") = Today
            clR.Fields("creby") = hfUserID.Value
        End If
        If Not rdpAdjDate.SelectedDate Is Nothing Then
            clR.Fields("adjdate") = rdpAdjDate.SelectedDate
        End If
        clR.Fields("amount") = txtAmt.Text
        clR.Fields("ratetypeid") = hfRateTypeID.Value
        clR.Fields("description") = txtDesc.Text
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgAdj.Rebind()
    End Sub

    Private Sub btnSearchRateType_Click(sender As Object, e As EventArgs) Handles btnSearchRateType.Click
        pnlDetail.Visible = False
        pnlSearch.Visible = True
        rgRateType.Rebind()
    End Sub

    Private Sub rgRateType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgRateType.SelectedIndexChanged
        pnlSearch.Visible = False
        pnlDetail.Visible = True
        hfRateTypeID.Value = rgRateType.SelectedValue
        GetRateDesc()
    End Sub

    Private Sub btnCommissionCancel_Click(sender As Object, e As EventArgs) Handles btnCommissionCancel.Click
        Response.Redirect("~/accounting/CommissionCancel.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub
End Class