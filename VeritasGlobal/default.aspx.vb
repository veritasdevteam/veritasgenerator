﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class _default
    Inherits System.Web.UI.Page
    Private lUserID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim SQL As String
        tbTodo.Width = pnlHeader.Width
        If Not IsPostBack Then
            GetServerInfo()
            CheckToDo()
            If lUserID = 0 Then
                hfSigninPopup.Value = "Visible"
                Dim Script As String
                Script = "function f(){$find(""" + rwSignIn.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwSignIn", Script, True)
                rwSignIn.VisibleOnPageLoad = True
                LockButtons()
            Else
                Dim clR As New clsDBO
                SQL = "select * from usersecurityinfo usi "
                SQL = SQL + "inner join userteam ut on usi.teamid = ut.teamid "
                SQL = SQL + "where userid = " & lUserID
                clR.OpenDB(SQL, AppSettings("connstring"))
                If clR.RowCount > 0 Then
                    clR.GetRow()
                    If clR.Fields("teamid") = 2 Or clR.Fields("teamid") = 3 Or clR.Fields("teamid") = 4 Or clR.Fields("teamid") = 11 Or clR.Fields("teamid") = 13 Then
                        If clR.Fields("teamlead") = "False" Then
                            ucAgentDashboards.sID = hfID.Value
                            ucAgentDashboards.Update()
                            pvAgentDashboard.Selected = True
                        Else
                            ucManagerDashboard.ID = hfID.Value
                            ucManagerDashboard.Update()
                            pvManagementDashboard.Selected = True
                        End If
                    End If
                    If clR.Fields("teamid") = 6 Then
                        UCExecDash.ID = hfID.Value
                        UCExecDash.UserID = lUserID
                        UCExecDash.Update()
                        pvExecDashboard.Selected = True
                    End If
                End If
                checkclaimpopup
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & lUserID & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")

        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        lUserID = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            lUserID = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
            CheckToDo()

        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Dim SQL As String
        lblError.Text = ""
        If txtEMail.Text = "" Then
            lblError.Text = "No E-Mail was entered."
            ShowError()
            Exit Sub
        End If
        If txtPassword.Text = "" Then
            lblError.Text = "No Password was entered."
            ShowError()
            Exit Sub
        End If
        SQL = "select * from userinfo "
        SQL = SQL + "where email = '" & txtEMail.Text & "' "
        SQL = SQL + "and password = '" & txtPassword.Text & "' "
        SQL = SQL + "and active <> 0"
        Dim clU As New clsDBO
        clU.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clU.RowCount = 0 Then
            lblError.Text = "Cannot locate user name/password."
            ShowError()
            Exit Sub
        End If
        clU.GetRow()
        lUserID = clU.Fields("userid")
        hfID.Value = System.Guid.NewGuid.ToString
        Dim clSI As New clsDBO
        '        SQL = "delete serverinfo where userid = " & lUserID
        'clSI.RunSQL(SQL, sCON)
        SQL = "select * from serverinfo "
        SQL = SQL + "where userid = " & lUserID
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        clSI.NewRow()
        clSI.Fields("userid") = lUserID
        clSI.Fields("systemid") = hfID.Value
        clSI.Fields("signindate") = Now
        clSI.AddRow()
        clSI.SaveDB()
        hfSigninPopup.Value = ""
        Dim script As String = "function f(){$find(""" + rwSignIn.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
        Unlockbuttons()
        CheckToDo()
        checkclaimpopup
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & lUserID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("teamid") = 2 Or clR.Fields("teamid") = 3 Or clR.Fields("teamid") = 4 Or clR.Fields("teamid") = 11 Or clR.Fields("teamid") = 13 Then
                If clR.Fields("teamlead") = "False" Then
                    ucAgentDashboards.sID = hfID.Value
                    ucAgentDashboards.Update()
                    pvAgentDashboard.Selected = True
                Else
                    ucManagerDashboard.ID = hfID.Value
                    ucManagerDashboard.Update()
                    pvManagementDashboard.Selected = True
                End If
            End If
            If clR.Fields("teamid") = 6 Then
                UCExecDash.ID = hfID.Value
                UCExecDash.UserID = lUserID
                UCExecDash.Update()
                pvExecDashboard.Selected = True
            End If
        End If
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & lUserID
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwError", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
        script = "function f(){$find(""" + rwSignIn.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwSignIn", script, True)
        rwSignIn.VisibleOnPageLoad = True

    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub CheckClaimPopup()
        Dim clCA As New VeritasGlobalTools.clsClaimAssign
        If clCA.CheckUserInfo(lUserID) Then
            hfClaimID.Value = clCA.CheckClaimUnassigned
            hfUserID.Value = lUserID
            If hfClaimID.Value > "0" Then
                hfClaimNo.Value = clCA.GetClaimNo(hfClaimID.Value)
                txtClaimNo.Text = hfClaimNo.Value & " is waiting to be accepted for processing."
                If clCA.IsContractAN(hfClaimID.Value) Then
                    If clCA.CheckANUser(hfUserID.Value) Then
                        ShowClaimAssign()
                    End If
                Else
                    If clCA.CheckANUser(hfUserID.Value) Then
                        ShowClaimAssign()
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub ShowClaimAssign()
        rwClaimAssign.Visible = True
        hfClaimAssign.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwClaimAssign.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwClaimAssign", script, True)
    End Sub

    Private Sub HideClaimAssign()
        hfClaimAssign.Value = ""
        Dim script As String = "function f(){$find(""" + rwClaimAssign.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
        rwClaimAssign.Visible = False
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        Dim clCA As New VeritasGlobalTools.clsClaimAssign
        If clCA.CheckClaimUnassigned2(hfClaimID.Value) > 0 Then
            clCA.AddAccepted(hfClaimID.Value, hfUserID.Value)
            clCA.UpdateClaimAssign(hfClaimID.Value, hfUserID.Value)
        End If
        HideClaimAssign()
    End Sub

    Private Sub btnDeny_Click(sender As Object, e As EventArgs) Handles btnDeny.Click
        Dim clCA As New VeritasGlobalTools.clsClaimAssign
        clCA.AddDeny(hfClaimID.Value, hfUserID.Value)
        HideClaimAssign()
    End Sub
End Class