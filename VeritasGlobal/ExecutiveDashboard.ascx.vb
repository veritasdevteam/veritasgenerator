﻿Imports System.Configuration.ConfigurationManager

Public Class ExecutiveDashboard
    Inherits System.Web.UI.UserControl

    Private bExecutive As Boolean
    Public UserID As Long
    Public ID As String

    Public Sub Update()
        hfID.Value = ID
        hfUserID.Value = UserID
        SetDashboard()
        FillDropDownDates()
    End Sub

    Private Sub SetDashboard()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clDate As New VeritasGlobalTools.clsDates
        GetTeamInfo()
        If bExecutive Then
            clDate.SetDates(rblNewClaims.SelectedIndex)
            SQL = "Declare @startDate as date = '" & clDate.EndDate & "' select (case when DATEDIFF(day,@startDate, GETDATE()) < 91 then cast(CreDate as date) else CAST(cast(MONTH(CreDate) as nvarchar)+'/1/'+CAST(YEAR(CreDate) as nvarchar) as date) end) as CreateDate, COUNT(*) as numOfClaims, 0 as goal from claim where CreDate >= @startDate and ((DATEPART(dw, CreDate) + @@DATEFIRST) % 7) NOT IN (0, 1) group by (case when DATEDIFF(day,@startDate, GETDATE()) < 91 then cast(CreDate as date) else CAST(cast(MONTH(CreDate) as nvarchar)+'/1/'+CAST(YEAR(CreDate) as nvarchar) as date) end) order by CreateDate"
            htmlNewClaims.DataSource = clR.GetData(SQL, AppSettings("connstring"))
            htmlNewClaims.DataBind()
            SQL = "select count(*) as counter from Claim where Status = 'Open'"
            clR.OpenDB(SQL, AppSettings("connstring"))
            Try
                clR.GetRow()
                txtClaimOpen.Text = clR.Fields("counter")
            Catch ex As Exception
                txtClaimOpen.Text = "0"
            End Try
            SQL = "select sum(cd.totalamt) as amt, count(distinct claimno) as cnt, " +
                "FORMAT(case when (sum(cd.totalamt) / count(distinct claimno)) is null then  0 else sum(cd.totalamt) / count(distinct claimno) end,'c') as Severity " +
                "from claim cl inner join claimdetail cd on cl.ClaimID = cd.claimid " +
                "where ClaimDetailType != 'Inspect' " +
                "and ClaimDetailStatus in ('Paid') " +
                "and ClaimActivityID = 56 " + clDate.BetweenDates(ddlDates.SelectedItem.ToString(), 0)
            clR.OpenDB(SQL, AppSettings("connstring"))
            Try
                clR.GetRow()
                txtSeverity.Text = clR.Fields("Severity")
            Catch ex As Exception
                txtSeverity.Text = "0"
            End Try
            SQL = "select sum(cd.totalamt) as amt, count(distinct claimno) as cnt, FORMAT(sum(cd.totalamt) / count(distinct claimno),'c') as Severity from claim cl " +
                "inner join claimdetail cd on cl.ClaimID = cd.claimid " +
                "where ClaimDetailType != 'Inspect' " +
                "and ClaimDetailStatus in ('Authorized', 'Approved') " + clDate.BetweenDates(ddlDates.SelectedItem.ToString(), 1)
            clR.OpenDB(SQL, AppSettings("connstring"))
            Try
                clR.GetRow()
                txtClaimsOverAuth.Text = clR.Fields("Severity")
            Catch ex As Exception
                txtClaimsOverAuth.Text = "0"
            End Try
            clDate.SetDates(rblClaimsInfo.SelectedIndex)
            SQL = "Declare @date as date = '" + clDate.EndDate + "' select PaidANRMF.PaidANRMF, PaidANReserve.PaidANReserve,PaidNonAN.PaidNonAN,PaidRMF.PaidRMF,PaidNonRMF.PaidNonRMF,Total.Total from (select FORMAT(sum(PaidAmt),'c') as PaidANRMF, '1' as Joiner from ClaimDetail where RateTypeID = 1101 and ClaimDetailStatus = 'PAID' and DatePaid > @date) PaidANRMF left join (select FORMAT(sum(PaidAmt),'c') as PaidANReserve, '1' as Joiner from ClaimDetail where RateTypeID = 1 and ClaimDetailStatus = 'PAID'and DatePaid > @date and ClaimID in(select ClaimID from claim where contractid in (select contractid from contract where AN = 1))) PaidANReserve on PaidANReserve.Joiner = PaidANRMF.Joiner left join (select FORMAT(sum(PaidAmt),'c') as PaidNonAN, '1' as Joiner from ClaimDetail where RateTypeID != 1101 and ClaimDetailStatus = 'PAID'and DatePaid > @date) PaidNonAN on PaidANRMF.Joiner = PaidNonAN.Joiner left join (select FORMAT(sum(PaidAmt),'c') as PaidRMF, '1' as Joiner from ClaimDetail where RateTypeID != 1 and ClaimDetailStatus = 'PAID' and DatePaid > @date) PaidRMF on PaidANRMF.Joiner = PaidRMF.Joiner left join (select FORMAT(sum(PaidAmt),'c') as PaidNonRMF, '1' as Joiner from ClaimDetail where RateTypeID = 1 and ClaimDetailStatus = 'PAID' and DatePaid > @date) PaidNonRMF on PaidANRMF.Joiner = PaidNonRMF.Joiner  left join (select FORMAT(sum(PaidAmt), 'c') as Total, '1' as Joiner from ClaimDetail where ClaimDetailStatus = 'Paid' and DatePaid > @date) as Total on PaidANRMF.Joiner = Total.Joiner"
            rgClaimsPaid.DataSource = clR.GetData(SQL, AppSettings("connstring"))
            SQL = "Declare @date as date = '" + clDate.EndDate + "' select ANRMF.AuthANRMF,AuthAN.AuthANReserve,AuthNonAN.AuthNonAN,AuthRMF.AuthRMF,AuthNonRMF, total.total from (select FORMAT(sum(AuthAmt),'c') as AuthANRMF, '1' as Joiner from ClaimDetail where RateTypeID = 1101 and ClaimDetailStatus = 'Authorized' and DateAuth > @date) ANRMF left join (select case when sum(AuthAmt) is null then FORMAT(0,'c') else FORMAT(sum(AuthAmt),'c') end as AuthANReserve, '1' as Joiner from ClaimDetail where RateTypeID = 1 and ClaimDetailStatus = 'Authorized'and DatePaid > @date and ClaimID in(select ClaimID from claim where contractid in (select contractid from contract where AN = 1))) AuthAN on ANRMF.Joiner = AuthAN.Joiner left join (select FORMAT(sum(AuthAmt),'c') as AuthNonAN, '1' as Joiner from ClaimDetail where RateTypeID != 1101 and ClaimDetailStatus = 'Authorized'and DateAuth > @date) as AuthNonAN on ANRMF.Joiner = AuthNonAN.Joiner left join (select FORMAT(sum(AuthAmt),'c') as AuthRMF, '1' as Joiner from ClaimDetail where RateTypeID != 1 and ClaimDetailStatus = 'Authorized' and DateAuth > @date) as AuthRMF on ANRMF.Joiner = AuthRMF.Joiner left join (select FORMAT(sum(AuthAmt),'c') as AuthNonRMF, '1' as Joiner from ClaimDetail where RateTypeID = 1 and ClaimDetailStatus = 'Authorized' and DateAuth > @date) as AuthNonRMF on ANRMF.Joiner = AuthNonRMF.Joiner left join (select FORMAT(sum(AuthAmt), 'c') as total, '1' as joiner from ClaimDetail where ClaimDetailStatus = 'Authorized' and DateAuth > @date) Total  on ANRMF.Joiner = Total.joiner "
            rgClaimsAuthorized.DataSource = clR.GetData(SQL, AppSettings("connstring"))
            clDate.SetDates(rblAverageClose.SelectedIndex)
            SQL = "Declare @startDate as date = '" + clDate.EndDate + "' select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(CloseDate as date) else CAST(cast(MONTH(CloseDate) as nvarchar) + '/1/' + CAST(YEAR(CloseDate) as nvarchar) as date) end) as closeDates, sum(DATEDIFF(DAY, CreDate, CloseDate)) / count(*) as average from Claim    where CloseDate >= @startDate and((DATEPART(dw, CloseDate) + @@DATEFIRST) % 7) NOT IN(0, 1)  group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(CloseDate as date) else CAST(cast(MONTH(CloseDate) as nvarchar) + '/1/' + CAST(YEAR(CloseDate) as nvarchar) as date) end) order by closeDates "
            htmlAverageClose.DataSource = clR.GetData(SQL, AppSettings("connstring"))
            clDate.SetDates(rblHeadCount.SelectedIndex)
            SQL = "select count(distinct userid) as numberOfUsers, cast(SignInDate as date) as date, 0 as goal from ServerInfo where SignInDate >= '" + clDate.EndDate + "' and ((DATEPART(dw, SignInDate) + @@DATEFIRST) % 7) NOT IN (0, 1) and UserID in (select UserID from UserSecurityInfo where TeamID in (2,3,4,11,13)) group by cast(SignInDate as date) order by cast(SignInDate as date)"
            htmlHeadCount.DataSource = clR.GetData(SQL, AppSettings("connstring"))
            clDate.SetDates(rblSales.SelectedIndex)
            SQL = "Declare @startDate as date = '" + clDate.EndDate + "' " +
                "select Other.SoldDate as salesDate, 
                case when an.cnt is null then 0 else an.cnt end as ANCnt, 
                case when vero.cnt is null then 0 else vero.cnt end  as VeroCnt, 
                case when other.cnt is null then 0 else other.cnt end as OtherCnt, 
                case when ep.cnt is null then 0 else ep.cnt end as EPCnt, 
                case when an.cnt is null then 0 else an.cnt end+ case when vero.cnt is null then 0 else vero.cnt end +other.cnt + case when ep.cnt is null then 0 else ep.cnt end as TotalCnt, 
                AVG(case when an.cnt is null then 0 else an.cnt end+ case when vero.cnt is null then 0 else vero.cnt end +other.cnt + case when ep.cnt is null then 0 else ep.cnt end) 
                over(order by an.solddate Rows between 29 Preceding and current Row) as average 
                from (select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end) as SoldDate, count(*) as cnt from VeritasMoxy.dbo.moxycontract where not agentname in ('Vero LLC', 'Financial Indemnity Group', 'Michael Clause', 'David Bailes', 'David Collins', 'Paul Coughlin') and not AgentName = 'Auto Nation Agent' and not contractno is null and SoldDate >= @startDate group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end)) Other
                left join(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end) as SoldDate, count(*) as cnt from VeritasMoxy.dbo.moxycontract  where agentname in ('Vero LLC', 'Financial Indemnity Group', 'Michael Clause', 'David Bailes', 'David Collins', 'Paul Coughlin') and not contractno is null and SoldDate >= @startDate group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end)) vero on vero.SoldDate = Other.SoldDate 
                left join (select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end) as SoldDate, count(*) as cnt from VeritasMoxy.dbo.moxycontract where AgentName = 'Auto Nation Agent' and not contractno is null and SoldDate >= @startDate group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end)) an   on other.SoldDate = an.SoldDate 
                left join(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end) as SoldDate, count(*) as cnt from VeritasMoxy.dbo.epcontract where not contractno is null and SoldDate >= @startDate group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end)) EP on EP.SoldDate = Other.SoldDate 
                order by Other.SoldDate desc "
            htmlSales.DataSource = clR.GetData(SQL, AppSettings("connstring"))
            clDate.SetDates(rblPaid.SelectedIndex)
            SQL = "Declare @startDate as date = '" + clDate.EndDate + "' " +
                "select Other.PaidDate as paidDates, 
                case when an.cnt is null then 0 else an.cnt end as ANCnt, 
                case when vero.cnt is null then 0 else vero.cnt end  as VeroCnt, 
                case when other.cnt is null then 0 else other.cnt end as OtherCnt, 
                case when ep.cnt is null then 0 else ep.cnt end as EPCnt,  
                case when an.cnt is null then 0 else an.cnt end + case when vero.cnt is null then 0 else vero.cnt end +other.cnt + case when ep.cnt is null then 0 else ep.cnt end as TotalCnt, 
                AVG(case when an.cnt is null then 0 else an.cnt end+ case when vero.cnt is null then 0 else vero.cnt end +other.cnt + case when ep.cnt is null then 0 else ep.cnt end) over(order by an.PaidDate Rows between 29 Preceding and current Row) as average 
                from(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(PaidDate as date) else CAST(cast(MONTH(PaidDate) as nvarchar) + '/1/' + CAST(YEAR(PaidDate) as nvarchar) as date) end) as PaidDate, count(*) as cnt from VeritasMoxy.dbo.moxycontract where not agentname in ('Vero LLC', 'Financial Indemnity Group', 'Michael Clause', 'David Bailes', 'David Collins', 'Paul Coughlin') and not AgentName = 'Auto Nation Agent' and not contractno is null and PaidDate >= @startDate group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(PaidDate as date) else CAST(cast(MONTH(PaidDate) as nvarchar) + '/1/' + CAST(YEAR(PaidDate) as nvarchar) as date) end)) Other
                left join(select(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(PaidDate as date) else CAST(cast(MONTH(PaidDate) as nvarchar) + '/1/' + CAST(YEAR(PaidDate) as nvarchar) as date) end) as PaidDate, count(*) as cnt from VeritasMoxy.dbo.moxycontract  where agentname in ('Vero LLC', 'Financial Indemnity Group', 'Michael Clause', 'David Bailes', 'David Collins', 'Paul Coughlin') and not contractno is null and PaidDate >= @startDate group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(PaidDate as date) else CAST(cast(MONTH(PaidDate) as nvarchar) + '/1/' + CAST(YEAR(PaidDate) as nvarchar) as date) end)) vero on vero.PaidDate = Other.PaidDate 
                left join(select(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(DatePaid as date) else CAST(cast(MONTH(DatePaid) as nvarchar) + '/1/' + CAST(YEAR(DatePaid) as nvarchar) as date) end) as PaidDate,  count(*) as cnt from contract where AN = 1 and Status = 'PAID' and not programid in (47, 48, 51, 54, 55, 56, 57, 58, 59, 60, 67, 101, 103, 105) and DatePaid >= @startDate group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(DatePaid as date) else CAST(cast(MONTH(DatePaid) as nvarchar) + '/1/' + CAST(YEAR(DatePaid) as nvarchar) as date) end)) an  on other.PaidDate = an.PaidDate 
                left join(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(DatePaid as date) else CAST(cast(MONTH(DatePaid) as nvarchar) + '/1/' + CAST(YEAR(DatePaid) as nvarchar) as date) end) as PaidDate, count(*) as cnt from Contract where ContractNo like '%EP%' and not programid in (47, 48, 51, 54, 55, 56, 57, 58, 59, 60, 67, 101, 103, 105) and Status = 'Paid' and DatePaid >= @startDate group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(DatePaid as date) else CAST(cast(MONTH(DatePaid) as nvarchar) + '/1/' + CAST(YEAR(DatePaid) as nvarchar) as date) end)) EP on EP.PaidDate = Other.PaidDate 
                order by Other.PaidDate desc "
            htmlPaid.DataSource = clR.GetData(SQL, AppSettings("connstring"))
            clDate.SetDates(rblCancel.SelectedIndex)
            SQL = "Declare @startDate as date = '" + clDate.EndDate + "' " +
                "select Other.CancelDate, 
                case when an.cnt is null then 0 else an.cnt end as ANCnt, 
                case when vero.cnt is null then 0 else vero.cnt end as VeroCnt, 
                case when ep.cnt is null then 0 else ep.cnt end as EPCnt, 
                case when Other.cnt is null then 0 else Other.cnt end as OtherCnt, 
                case when an.cnt is null then 0 else an.cnt end + case when vero.cnt is null then 0 else vero.cnt end +other.cnt + case when ep.cnt is null then 0 else ep.cnt end as TotalCnt, 
                AVG(case when an.cnt is null then 0 else an.cnt end+ case when vero.cnt is null then 0 else vero.cnt end +other.cnt + case when ep.cnt is null then 0 else ep.cnt end) over(order by an.CancelDate Rows between 29 Preceding and current Row) as average 
                from (select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end) as CancelDate, COUNT(*) as cnt from ContractCancel cc inner join contract c on c.ContractID = cc.ContractID where not cc.CancelDate is null and not programid in (47, 48, 51, 54, 55, 56, 57, 58, 59, 60, 67, 101, 103, 105)  and cc.CancelDate >= @startDate and not AN = 1and not ContractNo like '%EP%'and not ContractNo in (select ContractNo from VeritasMoxy.dbo.moxycontract vm where agentname in ('Vero LLC', 'Financial Indemnity Group', 'Michael Clause', 'David Bailes', 'David Collins', 'Paul Coughlin') and not ContractNo is null) group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end)) Other
                left join(select(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end) as CancelDate, COUNT(*) as cnt from ContractCancel cc inner join contract c on c.ContractID = cc.ContractID inner join VeritasMoxy.dbo.moxycontract vm on vm.ContractNo = c.ContractNo where agentname in ('Vero LLC', 'Financial Indemnity Group', 'Michael Clause', 'David Bailes', 'David Collins', 'Paul Coughlin') and cc.CancelDate >= @startDate and not cc.CancelDate is null and not programid in (47, 48, 51, 54, 55, 56, 57, 58, 59, 60, 67, 101, 103, 105) group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end)) vero on vero.CancelDate = Other.CancelDate 
                left join(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end) as CancelDate,COUNT(*) as cnt from ContractCancel cc inner join contract c on c.ContractID = cc.ContractID where not cc.CancelDate is null and not programid in (47, 48, 51, 54, 55, 56, 57, 58, 59, 60, 67, 101, 103, 105) and ContractNo in (select contractno from VeritasMoxy.dbo.EPContract)  and cc.CancelDate >= @startDate group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end)) ep on ep.CancelDate = Other.CancelDate 
                left join(select(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end) as CancelDate, COUNT(*) as cnt from ContractCancel cc inner join contract c on c.ContractID = cc.ContractID where AN = 1 and CancelDate >= @startDate and not CancelDate is null and not programid in (47, 48, 51, 54, 55, 56, 57, 58, 59, 60, 67, 101, 103, 105) group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end)) an  on Other.CancelDate = an.CancelDate 
                order by Other.CancelDate"
            htmlCancel.DataSource = clR.GetData(SQL, AppSettings("connstring"))
            clDate.SetDates(rblNewClaims.SelectedIndex)
            Try
                rgClaimsPaid.DataBind()
                rgClaimsAuthorized.DataBind()
                htmlAverageClose.DataBind()
                htmlHeadCount.DataBind()
                htmlSales.DataBind()
                htmlCancel.DataBind()
                htmlPaid.DataBind()
                htmlCancel.DataBind()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub FillDropDownDates()
        Dim sDate As String
        ddlDates.Items.Clear()
        ddlDates.Items.Add("All Time")
        sDate = Today
        Dim iCounter As Int32 = 0
        While iCounter < 24
            ddlDates.Items.Add(CDate(sDate).ToString("MM/yyyy"))
            sDate = CDate(sDate).AddMonths(-1)
            iCounter = iCounter + 1
        End While
    End Sub

    Private Sub GetTeamInfo()
        Dim SQL As String
        Dim clTI As New clsDBO
        bExecutive = False
        SQL = "select TeamID, TeamLead from UserSecurityInfo where UserID = " & hfUserID.Value
        clTI.OpenDB(SQL, AppSettings("connstring"))
        If clTI.RowCount > 0 Then
            clTI.GetRow()
            If clTI.Fields("teamid") = "6" Then
                bExecutive = True
            End If
        End If
    End Sub

    Private Sub btnUpdateDates_Click(sender As Object, e As EventArgs) Handles btnUpdateDates.Click
        SetDashboard()
    End Sub

    Private Sub rblNewClaims_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblNewClaims.SelectedIndexChanged
        SetDashboard()
    End Sub

    Private Sub rblClaimsInfo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblClaimsInfo.SelectedIndexChanged
        SetDashboard()
    End Sub

    Private Sub rblHeadCount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblHeadCount.SelectedIndexChanged
        SetDashboard()
    End Sub

    Private Sub rblPaid_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblPaid.SelectedIndexChanged
        SetDashboard()
    End Sub

    Private Sub rblCancel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblCancel.SelectedIndexChanged
        SetDashboard()
    End Sub

    Private Sub rblAverageClose_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblAverageClose.SelectedIndexChanged
        SetDashboard()
    End Sub

    Private Sub rblSales_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblSales.SelectedIndexChanged
        SetDashboard()
    End Sub

End Class