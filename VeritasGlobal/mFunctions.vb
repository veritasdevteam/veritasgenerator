﻿Imports System.Configuration.ConfigurationManager

Module mFunctions
    Public Function GetServiceCenterState(xClaimID As Long) As String
        GetServiceCenterState = ""
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select sc.State from claim c
            inner join ServiceCenter sc on c.ServiceCenterID = sc.ServiceCenterID
            where claimid = " & xClaimID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetServiceCenterState = clR.Fields("state")
        End If
    End Function

    Public Function GetUserInfo(xUserID As Long) As String
        GetUserInfo = ""
        If xUserID = 0 Then
            Exit Function
        End If
        Dim SQL As String
        Dim clUI As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & xUserID
        clUI.OpenDB(SQL, AppSettings("connstring"))
        If clUI.RowCount > 0 Then
            clUI.GetRow()
            GetUserInfo = clUI.Fields("fname") & " " & clUI.Fields("lname")
        End If

    End Function

    Public Function GetUserEmail(xUserID As Long) As String
        GetUserEmail = ""
        If xUserID = 0 Then
            Exit Function
        End If
        Dim SQL As String
        Dim clUI As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & xUserID
        clUI.OpenDB(SQL, AppSettings("connstring"))
        If clUI.RowCount > 0 Then
            clUI.GetRow()
            GetUserEmail = clUI.Fields("email")
        End If
    End Function

    Public Function GetSubAgentInfo(xID As Long) As String
        GetSubAgentInfo = ""
        If xID = 0 Then
            Exit Function
        End If
        Dim SQL As String
        Dim clSA As New clsDBO
        SQL = "select * from subagents "
        SQL = SQL + "where subagentid = " & xID
        clSA.OpenDB(SQL, AppSettings("connstring"))
        If clSA.RowCount > 0 Then
            clSA.GetRow()
            GetSubAgentInfo = clSA.Fields("subagentno") & " / " & clSA.Fields("subagentname")
        End If
    End Function

    Public Function GetAgentInfo(xID As Long) As String
        GetAgentInfo = ""
        If xID = 0 Then
            Exit Function
        End If
        Dim SQL As String
        Dim clA As New clsDBO
        SQL = "select * from agents "
        SQL = SQL + "where agentid = " & xID
        clA.OpenDB(SQL, AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            GetAgentInfo = clA.Fields("agentno") & " / " & clA.Fields("agentname")
        End If
    End Function

    Public Function GetRateType(xID As Long) As String
        GetRateType = ""
        If xID = 0 Then
            Exit Function
        End If
        Dim SQL As String
        Dim clRT As New clsDBO
        SQL = "select * from ratetype "
        SQL = SQL + "where ratetypeid = " & xID
        clRT.OpenDB(SQL, AppSettings("connstring"))
        If clRT.RowCount > 0 Then
            clRT.GetRow()
            GetRateType = clRT.Fields("ratetypename")
        End If
    End Function

    Public Function GetAllow45(xUserID As Long) As Boolean
        GetAllow45 = False
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & xUserID & " "
        SQL = SQL + "and allow45days <> 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            GetAllow45 = True
        End If
    End Function

    Public Function GetDaysSale(xClaimID As Long) As Long
        GetDaysSale = 0
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from claim cl "
        SQL = SQL + "left join contract c on cl.contractid = c.contractid "
        SQL = SQL + "where claimid = " & xClaimID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetDaysSale = DateDiff(DateInterval.Day, CDate(clR.Fields("saledate")), CDate(clR.Fields("lossdate")))
        End If
    End Function

End Module
