﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class ClaimJobs
    Inherits System.Web.UI.UserControl

    Public Sub Update()
        rgJobs.Rebind()
        FillTotal()
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("claimid")
        dsStates.ConnectionString = AppSettings("connstring")
        dsCDT.ConnectionString = AppSettings("connstring")
        dsClaimDetailStatus.ConnectionString = AppSettings("connstring")
        dsClaimReason.ConnectionString = AppSettings("connstring")
        dsLossCode.ConnectionString = AppSettings("connstring")
        dsWexMethod.ConnectionString = AppSettings("connstring")
        dsClaimPayee.ConnectionString = AppSettings("connstring")
        dsJobs.ConnectionString = AppSettings("connstring")



        If bJobRefresh Then
            rgJobs.Rebind()
            bJobRefresh = False
        End If
        If Not IsPostBack Then
            GetServerInfo()
            CheckPaymentSecurity()
            If CheckLock() Then
                LockButtons()
            Else
                UnlockButtons()
            End If
            pnlDetail.Visible = False
            pnlList.Visible = True
            pnlAddClaimPayee.Visible = False
            pnlLossCode.Visible = False
            pnlSeekPayee.Visible = False
            pnlToBePaidDetail.Visible = False
            trACH.Visible = False
            trCC.Visible = False
            trCheck.Visible = False
            trWex.Visible = False
            FillPayeeTotal()
            FillRateType()
            FillJobsTotal()
            FillTotal()
            ReadOnlyButtons()
            FillJobNo()
            FillJobNo2()
            FillRateType2()
            If hfSlushNote.Value = "Visible" Then
                ShowSlushNote()
            Else
                HideSlushNote()
            End If
            If hfFieldCheck.Value = "Visible" Then
                ShowFieldCheck()
            Else
                HideFieldCheck()
            End If
        End If
    End Sub

    Private Sub FillRateType2()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim i1 As New ListItem
        cboRateType0.Items.Clear()
        i1.Value = 0
        i1.Text = ""
        cboRateType0.Items.Add(i1)
        i1 = New ListItem
        i1.Value = 1
        i1.Text = "Claims Reserve"
        cboRateType0.Items.Add(i1)

        If hfUserID.Value = "3" Then
            SQL = "select * from ratetype "
            SQL = SQL + "where ratecategoryid = 5 "
            SQL = SQL + "order by ratetypename "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                For cnt = 0 To clR.RowCount - 1
                    clR.GetRowNo(cnt)
                    i1 = New ListItem
                    i1.Value = clR.Fields("ratetypeid")
                    i1.Text = clR.Fields("ratetypename")
                    cboRateType0.Items.Add(i1)
                Next
            End If
        Else
            SQL = "select * from contract c "
            SQL = SQL + "inner join claim cl on c.contractid = cl.contractid "
            SQL = SQL + "where claimid = " & hfClaimID.Value
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                clR.GetRow()
                If clR.Fields("programname") = "Veritas" Then
                    i1 = New ListItem
                    i1.Value = 2868
                    i1.Text = "Veritas RMF"
                    cboRateType0.Items.Add(i1)
                Else
                    i1 = New ListItem
                    i1.Value = 2171
                    i1.Text = "Red Shield RMF"
                    cboRateType0.Items.Add(i1)
                End If
                If CBool(clR.Fields("an")) Then
                    i1 = New ListItem
                    i1.Value = 1101
                    i1.Text = "AN RMF"
                    cboRateType0.Items.Add(i1)
                End If
                If clR.Fields("contractno").Contains("EP") Then
                    i1 = New ListItem
                    i1.Value = 3072
                    i1.Text = "EP RMF"
                    cboRateType0.Items.Add(i1)
                End If
                i1 = New ListItem
                i1.Value = 2834
                i1.Text = "Seller RMF"
                cboRateType0.Items.Add(i1)
                i1 = New ListItem
                i1.Value = 3071
                i1.Text = "Agent RMF"
                cboRateType0.Items.Add(i1)
            End If
        End If
    End Sub

    Private Sub FillJobNo2()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim li As New ListItem
        li.Value = "0"
        li.Text = ""
        cboSlushJobNo.Items.Add(li)
        li = New ListItem
        li.Value = "All"
        li.Text = "All"
        cboSlushJobNo.Items.Add(li)
        SQL = "select JobNo from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        SQL = SQL + "group by Jobno "
        SQL = SQL + "order by JobNo "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                li = New ListItem
                li.Value = clR.Fields("jobno")
                li.Text = clR.Fields("jobno")
                cboSlushJobNo.Items.Add(li)
            Next
        End If
    End Sub

    Private Sub FillJobNo()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim li As New ListItem
        li.Value = "0"
        li.Text = ""
        cboJobNo.Items.Add(li)
        SQL = "select JobNo from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        SQL = SQL + "group by Jobno "
        SQL = SQL + "order by JobNo "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                li = New ListItem
                li.Value = clR.Fields("jobno")
                li.Text = clR.Fields("jobno")
                cboJobNo.Items.Add(li)
            Next
        End If
    End Sub

    Private Sub FillJobsTotal()
        Dim clCD As New clsDBO
        Dim SQL As String
        Dim li As New ListItem
        li.Value = 0
        li.Text = "All"
        cboJobsTotal.Items.Add(li)
        SQL = "select jobno from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "group by jobno "
        SQL = SQL + "order by jobno "
        clCD.OpenDB(SQL, AppSettings("connstring"))
        If clCD.RowCount > 0 Then
            For cnt = 0 To clCD.RowCount - 1
                clCD.GetRowNo(cnt)
                li = New ListItem
                li.Text = clCD.Fields("jobno")
                li.Value = cnt + 1
                cboJobsTotal.Items.Add(li)
            Next
        End If
        cboJobsTotal.SelectedValue = 0
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                LockButtons()
            End If
            If CBool(clR.Fields("AllowSlush")) Then
                tcManageSlush.Visible = True
            Else
                tcManageSlush.Visible = False
            End If
            tcManagerAuth.Visible = True
            If CBool(clR.Fields("AllowAZDenied")) Then
                hfAllowAZDenied.Value = "True"
            Else
                hfAllowAZDenied.Value = "False"
            End If
            If CBool(clR.Fields("allowtotalpayment")) Then
                txtTotalAmt.ReadOnly = False
                txtPaidAmt.ReadOnly = False
            End If
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = DateTime.Today
        sEndDate = DateAdd("d", 1, DateTime.Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Function CheckLock() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckLock = False
        SQL = "Select claimid from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "And lockuserid <> " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            CheckLock = False
        Else
            CheckLock = True
        End If
    End Function

    Private Sub CheckPaymentSecurity()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and claimpayment <> 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            btnMakePayment.Visible = True
        Else
            btnMakePayment.Visible = False
        End If
    End Sub

    Private Sub LockButtons()
        btnAddClaimPayee.Enabled = False
        btnAddDetail.Enabled = False
        btnSaveClaimDetail.Enabled = False
        btnSCSave.Enabled = False
        btnSeekLossCode.Enabled = False
        btnSeekPayee.Enabled = False
        btnApproveIt.Enabled = False
        btnAuthIt.Enabled = False
        btnDenyIt.Enabled = False
    End Sub

    Private Sub UnlockButtons()
        btnAddClaimPayee.Enabled = True
        btnAddDetail.Enabled = True
        btnCancelClaimDetail.Enabled = True
        btnSaveClaimDetail.Enabled = True
        btnSCCancel.Enabled = True
        btnSCSave.Enabled = True
        btnSeekLossCode.Enabled = True
        btnSeekPayee.Enabled = True
    End Sub

    Private Sub btnAddDetail_Click(sender As Object, e As EventArgs) Handles btnAddDetail.Click
        pnlList.Visible = False
        pnlDetail.Visible = True
        cboClaimDetailType.ClearSelection()
        GetNextJobNo()
        cboClaimDetailStatus.ClearSelection()
        txtLossCode.Text = ""
        txtLossCodeDesc.Text = ""
        txtPayeeNo.Text = ""
        txtPayeeName.Text = ""
        txtReqAmt.Text = 0
        txtAuthAmt.Text = 0
        txtTaxAmt.Text = 0
        txtPaidAmt.Text = 0
        cboRateType0.SelectedValue = 1
        rdpAuthorizedDate.SelectedDate = Nothing
        rdpDateApprove.SelectedDate = Nothing
        rdpDatePaid.SelectedDate = Nothing
        hfClaimDetailID.Value = 0
    End Sub

    Private Sub GetNextJobNo()
        Dim SQL As String
        Dim clR As New clsDBO
        If hfJobNo.Value.Length = 0 Then
            SQL = "select max(jobno) as mJob from claimdetail "
            SQL = SQL + "where claimid = " & hfClaimID.Value & " "
            SQL = SQL + "and jobno like 'j%' "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount = 0 Then
                hfJobNo.Value = "J01"
            Else
                clR.GetRow()
                If clR.Fields("mjob").Length = 0 Then
                    hfJobNo.Value = "J01"

                Else
                    hfJobNo.Value = "J" & Format(CLng(clR.Fields("mjob").Substring(clR.Fields("mjob").Length - 2, 2)) + 1, "00")
                End If
            End If
        End If

        SQL = "select max(jobno) as mJob from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and jobno like 'j%' "
        SQL = SQL + "and jobno <> '" & hfJobNo.Value & "' "
        SQL = SQL + "and losscode = '" & txtLossCode.Text.Trim & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("mjob").Length > 0 Then
                hfJobNo.Value = clR.Fields("mJob")
            End If
        End If
        txtJobNo.Text = hfJobNo.Value
    End Sub

    Private Sub FillRateType()
        Dim SQL As String
        Dim clR As New clsDBO
        cboSlushRateType.Items.Clear()
        Dim i1 As New ListItem
        i1.Value = 0
        i1.Text = ""
        cboSlushRateType.Items.Add(i1)

        i1 = New ListItem
        i1.Value = 1
        i1.Text = "Claims Reserve"
        cboSlushRateType.Items.Add(i1)

        SQL = "select * from contract c "
        SQL = SQL + "inner join claim cl on c.contractid = cl.contractid "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("programname") = "Veritas" Then
                i1 = New ListItem
                i1.Value = 2868
                i1.Text = "Veritas RMF"
                cboSlushRateType.Items.Add(i1)
            Else
                i1 = New ListItem
                i1.Value = 2171
                i1.Text = "Red Shield RMF"
                cboSlushRateType.Items.Add(i1)
            End If
            If clR.Fields("contractno").Contains("AN") Then
                i1 = New ListItem
                i1.Value = 1101
                i1.Text = "AN RMF"
                cboSlushRateType.Items.Add(i1)
            End If
            If clR.Fields("contractno").Contains("EP") Then
                i1 = New ListItem
                i1.Value = 3072
                i1.Text = "EP RMF"
                cboSlushRateType.Items.Add(i1)
            End If
            i1 = New ListItem
            i1.Value = 2834
            i1.Text = "Seller RMF"
            cboSlushRateType.Items.Add(i1)
            i1 = New ListItem
            i1.Value = 3071
            i1.Text = "Agent RMF"
            cboSlushRateType.Items.Add(i1)
        End If
    End Sub

    Private Sub rgJobs_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgJobs.SelectedIndexChanged
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("allowslush")) Then
                cboRateType0.Enabled = True
            Else
                cboRateType0.Enabled = False
            End If
            If clR.Fields("AutoNationOnly") = True Then
                Exit Sub
            End If
            If clR.Fields("VeroOnly") = True Then
                Exit Sub
            End If
        End If

        pnlList.Visible = False
        pnlDetail.Visible = True
        cboClaimDetailType.ClearSelection()
        txtJobNo.Text = ""
        cboClaimDetailStatus.ClearSelection()
        txtLossCode.Text = ""
        txtLossCodeDesc.Text = ""
        txtPayeeNo.Text = ""
        txtPayeeName.Text = ""
        txtReqAmt.Text = 0
        txtAuthAmt.Text = 0
        txtTaxAmt.Text = 0
        txtPaidAmt.Text = 0
        rdpAuthorizedDate.SelectedDate = Nothing
        rdpDateApprove.SelectedDate = Nothing
        rdpDatePaid.SelectedDate = Nothing
        cboRateType0.ClearSelection()
        hfClaimDetailID.Value = 0
        'FillRateType()
        hfClaimDetailID.Value = rgJobs.SelectedValue
        SQL = "select * from claimdetail cd "
        SQL = SQL + "where claimdetailid = " & rgJobs.SelectedValue
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            cboClaimDetailType.SelectedValue = clR.Fields("claimdetailtype")
            txtJobNo.Text = clR.Fields("jobno")
            cboClaimDetailStatus.SelectedValue = clR.Fields("claimdetailstatus")
            txtLossCode.Text = clR.Fields("losscode")
            GetLossCodeDesc()
            hfClaimPayeeID.Value = clR.Fields("claimpayeeid")
            GetPayee()

            txtReqAmt.Text = clR.Fields("reqamt")
            txtAuthAmt.Text = clR.Fields("authamt")
            txtTaxAmt.Text = clR.Fields("taxamt")
            txtPaidAmt.Text = clR.Fields("paidamt")
            txtReqQty.Text = clR.Fields("reqqty")
            txtReqCost.Text = clR.Fields("reqcost")
            txtAuthQty.Text = clR.Fields("authqty")
            txtAuthCost.Text = clR.Fields("authcost")
            txtPartNo.Text = clR.Fields("partno")
            txtClaimDesc.Text = clR.Fields("ClaimDesc")
            txtTotalAmt.Text = clR.Fields("totalamt")
            cboReason.SelectedValue = clR.Fields("claimreasonid")
            If clR.Fields("taxper").Length > 0 Then
                If clR.Fields("taxper") < 1 Then
                    txtTaxRate.Text = clR.Fields("taxper") * 100
                Else
                    txtTaxRate.Text = clR.Fields("taxper")
                End If
            End If
            If clR.Fields("dateauth").Length > 0 Then
                rdpAuthorizedDate.SelectedDate = CDate(clR.Fields("dateauth"))
            Else
                rdpAuthorizedDate.Clear()
            End If
            If clR.Fields("dateapprove").Length > 0 Then
                rdpDateApprove.SelectedDate = CDate(clR.Fields("dateapprove"))
            Else
                rdpDateApprove.Clear()
            End If
            If clR.Fields("datepaid").Length > 0 Then
                rdpDatePaid.SelectedDate = clR.Fields("datepaid")
            Else
                rdpDatePaid.Clear()
            End If
            If clR.Fields("ratetypeid").Length > 0 Then
                Try
                    cboRateType0.SelectedValue = clR.Fields("ratetypeid")
                Catch ex As Exception
                    Dim i1 As New ListItem
                    If clR.Fields("ratetypeid") = "1101" Then
                        i1 = New ListItem
                        i1.Value = 1101
                        i1.Text = "AN RMF"
                        cboRateType0.Items.Add(i1)
                    End If
                    If clR.Fields("ratetypeid") = 2868 Then
                        i1 = New ListItem
                        i1.Value = 2868
                        i1.Text = "Veritas RMF"
                        cboRateType0.Items.Add(i1)
                    End If
                    If clR.Fields("ratetypeid") = 2171 Then
                        i1 = New ListItem
                        i1.Value = 2171
                        i1.Text = "Red Shield RMF"
                        cboRateType0.Items.Add(i1)
                    End If
                    If clR.Fields("ratetypeid") = 2995 Then
                        i1 = New ListItem
                        i1.Value = 2995
                        i1.Text = "Auto Nation RMF"
                        cboRateType0.Items.Add(i1)
                    End If
                    If clR.Fields("ratetypeid") = 3072 Then
                        i1 = New ListItem
                        i1.Value = 3072
                        i1.Text = "EP RMF"
                        cboRateType0.Items.Add(i1)
                    End If
                    cboRateType0.SelectedValue = clR.Fields("ratetypeid")
                End Try
            End If
            If txtReqQty.Text.Length = 0 Then
                txtReqQty.Text = 0
            End If
            If txtReqCost.Text.Length = 0 Then
                txtReqCost.Text = 0
            End If
            If txtAuthCost.Text.Length = 0 Then
                txtAuthCost.Text = 0
            End If
            If txtAuthQty.Text.Length = 0 Then
                txtAuthQty.Text = 0
            End If
            If txtTaxRate.Text.Length = 0 Then
                txtTaxRate.Text = 0
            End If
            If txtReqAmt.Text.Length = 0 Then
                txtReqAmt.Text = 0
            End If
            If txtTotalAmt.Text.Length = 0 Then
                txtTotalAmt.Text = 0
            End If
            LockAuthText()
            CheckSecurity()
            If GetDaysSale(clR.Fields("claimid")) <= 45 Then
                If Not GetAllow45(hfUserID.Value) Then
                    cboClaimDetailStatus.Enabled = False
                End If
            End If
        End If
    End Sub

    Private Sub CheckSecurity()
        Dim SQL As String
        Dim clUSI As New clsDBO
        cboClaimDetailStatus.Enabled = True
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clUSI.OpenDB(SQL, AppSettings("connstring"))
        If clUSI.RowCount > 0 Then
            clUSI.GetRow()
            If cboClaimDetailStatus.SelectedValue = "Denied" Then
                If CBool(clUSI.Fields("AllowUndenyClaim")) Then
                    cboClaimDetailStatus.Enabled = True
                Else
                    cboClaimDetailStatus.Enabled = False
                End If
            End If
        End If
    End Sub

    Private Sub LockAuthText()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("AllCoverage")) Then
                Exit Sub
            End If
        End If

        If GetSaleDate() < CDate("1/1/2019") Then
            Exit Sub
        End If


        SQL = "select * from claimlosscode clc "
        SQL = SQL + "inner join contractcoverage cc "
        SQL = SQL + "on cc.CoverageCode = clc.LossCodePrefix "
        SQL = SQL + "inner join contract c "
        SQL = SQL + "on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID "
        SQL = SQL + "inner join claim cl "
        SQL = SQL + "on cl.ContractID = c.ContractID "
        SQL = SQL + "where cl.claimid = " & hfClaimID.Value & " "
        If txtLossCode.Text.Length = 0 Then
            Exit Sub
        End If
        SQL = SQL + "and cc.coveragecode = '" & txtLossCode.Text.Substring(0, 4) & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            SQL = "update claimdetail "
            SQL = SQL + "set losscodelock = 1 "
            SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
            clR.RunSQL(SQL, AppSettings("connstring"))
            txtAuthCost.Enabled = False
            txtAuthQty.Enabled = False
            txtTaxRate.Enabled = False
            cboClaimDetailStatus.Enabled = False
            txtAuthAmt.Enabled = False
            txtPaidAmt.Enabled = False
            txtLossCode.BackColor = Drawing.Color.Red
        Else
            SQL = "update claimdetail "
            SQL = SQL + "set losscodelock = 0 "
            SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
            clR.RunSQL(SQL, AppSettings("connstring"))
            txtAuthCost.Enabled = True
            txtAuthQty.Enabled = True
            txtTaxRate.Enabled = True
            cboClaimDetailStatus.Enabled = True
            txtAuthAmt.Enabled = True
            txtLossCode.BackColor = Drawing.Color.White
        End If
    End Sub

    Private Function GetSaleDate() As Date
        Dim clR As New clsDBO
        Dim SQL As String
        GetSaleDate = "1/1/1950"
        SQL = "select c.saledate from contract c "
        SQL = SQL + "inner join claim cl on cl.contractid = c.contractid "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetSaleDate = clR.Fields("saledate")
        End If
    End Function

    Private Sub GetPayee()
        If hfClaimPayeeID.Value = "" Then
            hfClaimPayeeID.Value = 0
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimpayee "
        SQL = SQL + "where claimpayeeid = " & hfClaimPayeeID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtPayeeName.Text = clR.Fields("payeename")
            txtPayeeNo.Text = clR.Fields("payeeno")
        End If
    End Sub

    Private Sub GetLossCodeDesc()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimlosscode "
        SQL = SQL + "where losscode = '" & txtLossCode.Text & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtLossCodeDesc.Text = clR.Fields("losscodedesc")
        End If
    End Sub

    Private Sub btnSeekLossCode_Click(sender As Object, e As EventArgs) Handles btnSeekLossCode.Click
        pnlDetail.Visible = False
        pnlLossCode.Visible = True
        rgLossCode.Rebind()
        ColorLossCode()
    End Sub

    Private Sub ColorLossCode()
        Dim SQL As String
        Dim clR As New clsDBO

        If GetSaleDate() < CDate("1/1/2019") Then
            Exit Sub
        End If

        rgLossCode.Columns(3).Visible = True

        For cnt2 = 0 To rgLossCode.Items.Count - 1
            If rgLossCode.Items(cnt2).Item("ACP").Text = "1" Then
                rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.Red
            End If
        Next

        rgLossCode.Columns(3).Visible = False

        'SQL = "select coveragecode from contractcoverage cc "
        'SQL = SQL + "inner join contract c on c.programid = cc.programid and c.plantypeid = cc.plantypeid "
        'SQL = SQL + "inner join claim cl on cl.contractid = c.contractid "
        'SQL = SQL + "where cl.claimid = " & hfClaimID.Value
        'clR.OpenDB(SQL, AppSettings("connstring"))
        'If clR.RowCount > 0 Then
        '    For cnt = 0 To clR.RowCount - 1
        '        clR.GetRowNo(cnt)
        '        For cnt2 = 0 To rgLossCode.Items.Count - 1
        '            If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = clR.Fields("coveragecode") Then
        '                rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
        '            End If
        '            If hfUserID.Value = "1" Or hfUserID.Value = "1205" Or hfUserID.Value = "1205" Then
        '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "RR00" Then
        '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
        '                End If
        '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "PA00" Then
        '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
        '                End If
        '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "ZLEG" Then
        '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
        '                End If
        '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "ZSET" Then
        '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
        '                End If
        '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "ZATO" Then
        '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
        '                End If
        '            End If
        '        Next
        '    Next
        'End If
    End Sub
    Private Sub btnSeekPayee_Click(sender As Object, e As EventArgs) Handles btnSeekPayee.Click
        pnlDetail.Visible = False
        pnlSeekPayee.Visible = True
    End Sub

    Private Sub btnCancelClaimDetail_Click(sender As Object, e As EventArgs) Handles btnCancelClaimDetail.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgJobs.Rebind()
    End Sub

    Private Sub btnSaveClaimDetail_Click(sender As Object, e As EventArgs) Handles btnSaveClaimDetail.Click
        Dim SQL As String
        Dim clR As New clsDBO
        pnlList.Visible = False
        pnlDetail.Visible = True
        'FillRateType()
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfStatus.Value = clR.Fields("claimdetailstatus")
            hfOldTotalAmt.Value = clR.Fields("totalamt")
        Else
            clR.NewRow()
            hfStatus.Value = "New"
            hfOldTotalAmt.Value = "0"
        End If
        If cboClaimDetailStatus.SelectedValue = "Authorized" Or cboClaimDetailStatus.SelectedValue = "Approved" Then
            If clR.Fields("claimdetailstatus") <> "Authorized" Then
                If cboClaimDetailStatus.SelectedValue <> clR.Fields("claimdetailstatus") Then
                    If CheckMissingInfo() Then
                        Exit Sub
                    End If
                End If
            End If
        End If
        If cboClaimDetailStatus.Text = "Authorized" And Not CheckInspect() Then
            lblAuthorizedError.Text = "Inspection has not been completed."
            pnlDetail.Visible = False
            pnlList.Visible = True
            Exit Sub
        End If
        If txtAuthAmt.Text.Length = 0 Then
            txtFieldChk.Text = "Authorized Amt is missing"
            ShowFieldCheck()
            Exit Sub
        End If
        If Math.Round(CDbl(txtAuthAmt.Text) + CDbl(txtTaxAmt.Text), 2) <> CDbl(txtTotalAmt.Text) Then
            txtFieldChk.Text = "The Authorized Amt plus Tax Amt does not equal Total Amt."
            ShowFieldCheck()
            Exit Sub
        End If
        If cboClaimDetailStatus.Text = "Paid" Then
            If txtTotalAmt.Text <> txtPaidAmt.Text Then
                txtFieldChk.Text = "The Paid Amt does not equal Total Amt."
                ShowFieldCheck()
                Exit Sub
            End If
        End If
        clR.Fields("claimid") = hfClaimID.Value
        clR.Fields("claimdesc") = txtClaimDesc.Text
        clR.Fields("claimdetailtype") = cboClaimDetailType.SelectedValue
        If txtJobNo.Text.Length > 0 Then
            clR.Fields("jobno") = txtJobNo.Text
        End If
        clR.Fields("losscode") = txtLossCode.Text
        clR.Fields("reqamt") = txtReqAmt.Text
        clR.Fields("claimpayeeid") = hfClaimPayeeID.Value
        clR.Fields("authcost") = txtAuthCost.Text
        If txtTaxRate.Text.Length > 0 Then
            clR.Fields("taxper") = CDbl(txtTaxRate.Text)
        End If
        If txtTaxAmt.Text.Length = 0 Then
            clR.Fields("taxamt") = 0
        Else
            If clR.Fields("authqty").Length = 0 Then
                clR.Fields("authqty") = 0
            End If
            clR.Fields("taxamt") = txtTaxAmt.Text
        End If
        clR.Fields("totalamt") = txtTotalAmt.Text
        clR.Fields("Authamt") = txtAuthAmt.Text
        clR.Fields("paidamt") = txtPaidAmt.Text
        clR.Fields("claimreasonid") = cboReason.SelectedValue
        If Not rdpAuthorizedDate.SelectedDate Is Nothing Then
            clR.Fields("dateauth") = rdpAuthorizedDate.SelectedDate
        Else
            clR.Fields("dateauth") = ""
        End If
        If Not rdpDateApprove.SelectedDate Is Nothing Then
            clR.Fields("dateapprove") = rdpDateApprove.SelectedDate
        Else
            clR.Fields("dateapprove") = ""
        End If
        If Not rdpDatePaid.SelectedDate Is Nothing Then
            clR.Fields("datepaid") = rdpDatePaid.SelectedDate
        Else
            clR.Fields("datepaid") = ""
        End If
        If clR.Fields("ratetypeid") = "1" Then
            If cboRateType0.SelectedValue <> "1" Then
                clR.Fields("slushbyid") = hfUserID.Value
                clR.Fields("slushdate") = Today
            End If
        End If
        clR.Fields("ratetypeid") = cboRateType0.SelectedValue
        clR.Fields("reqqty") = txtReqQty.Text
        clR.Fields("reqcost") = txtReqCost.Text
        clR.Fields("authqty") = txtAuthQty.Text
        clR.Fields("authcost") = txtAuthCost.Text
        clR.Fields("moddate") = DateTime.Today
        clR.Fields("modby") = hfUserID.Value
        lblApprovedError.Text = ""
        If cboClaimDetailStatus.SelectedValue = "Denied" Then
            If clR.Fields("claimdetailstatus").Trim <> "Denied" Then
                If hfAllowAZDenied.Value = "False" Then
                    If GetServiceCenterState(hfClaimID.Value) = "AZ" Then
                        lblAuthorizedError.Text = "You can not deny an AZ claim. Please see manager."
                        lblAuthorizedError.Visible = True
                        GoTo MoveHere
                    End If
                End If
            End If
            clR.Fields("claimdetailstatus") = cboClaimDetailStatus.SelectedValue
            CheckClose()
            GoTo MoveToSave
        End If
        clR.Fields("claimdetailstatus") = cboClaimDetailStatus.SelectedValue

        If cboClaimDetailStatus.SelectedValue = "Cancelled" Then
            CheckClose()
            GoTo MoveToSave
        End If
        If cboClaimDetailStatus.SelectedValue = "Requested" Then
            CheckClose()
            GoTo MoveToSave
        End If
        If clR.Fields("claimdetailstatus") <> "Paid" Then
            If cboClaimDetailStatus.SelectedValue = "Approved" Then
                If txtAuthAmt.Text.Length = 0 Then
                    txtAuthAmt.Text = 0
                End If
                If clR.Fields("claimdetailid").Length > 0 Then
                    If Not CheckLimitApproved(txtAuthAmt.Text, clR.Fields("claimdetailid")) Then
                        lblApprovedError.Text = "Send to Manager for Approval"
                        GoTo MoveHere
                    End If
                Else
                    If Not CheckLimitApproved(txtAuthAmt.Text, 0) Then
                        lblApprovedError.Text = "Send to Manager for Approval"
                        GoTo MoveHere
                    End If
                End If
            End If
            If cboClaimDetailStatus.SelectedValue = "Authorized" Then
                If txtAuthAmt.Text.Length = 0 Then
                    txtAuthAmt.Text = 0
                End If
                If Not CheckLimitAuthorized(txtAuthAmt.Text, clR.Fields("claimdetailid")) Then
                    lblApprovedError.Text = "Send to Manager for Approval"
                    clR.Fields("claimdetailstatus") = "Requested"
                End If
            End If

        End If
MoveToSave:

        If hfTicket.Value = "True" Then
            hfTicket.Value = "False"
            PlaceTicket()
        End If
        If clR.RowCount = 0 Then
            clR.Fields("credate") = DateTime.Today
            clR.Fields("creby") = hfUserID.Value
            clR.AddRow()
        End If
        clR.SaveDB()
        AddChange()
        Dim clCD As New VeritasGlobalTools.clsClaimDetail
        clCD.ProcessClaim(hfClaimID.Value)
        If clCD.Refresh Then
            Response.Redirect("~/claim/claim.aspx?sid=" & hfID.Value & "&ClaimID=" & hfClaimID.Value)
        End If
MoveHere:
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgJobs.Rebind()
        FillTotal()
        SQL = "update claim "
        SQL = SQL + "set moddate = '" & DateTime.Today & "', "
        SQL = SQL + "modby = " & hfUserID.Value & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))

    End Sub

    Private Sub AddChange()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from claimdetail cd "
        SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If hfStatus.Value = "Requested" Then
                If clR.Fields("claimdetailstatus") = "Authorized" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "values ('" & Today & "', 0, " & clR.Fields("totalamt") & ", " & clR.Fields("totalamt")
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
                If clR.Fields("claimdetailstatus") = "Approved" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "values ('" & Today & "', 0, " & clR.Fields("totalamt") & ", " & clR.Fields("totalamt")
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
                If clR.Fields("claimdetailstatus") = "Paid" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "values ('" & Today & "', 0, " & clR.Fields("totalamt") & ", " & clR.Fields("totalamt")
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
            End If
            If hfStatus.Value = "Authorized" Then
                If hfOldTotalAmt.Value = "" Then
                    hfOldTotalAmt.Value = 0
                End If
                If CDbl(hfOldTotalAmt.Value) <> CDbl(clR.Fields("totalamt")) Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "values ('" & Today & "', " & hfOldTotalAmt.Value & ", "
                    SQL = SQL + clR.Fields("totalamt") & ", " & CDbl(clR.Fields("totalamt")) - CDbl(hfOldTotalAmt.Value)
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
                If clR.Fields("claimdetailstatus") = "Requested" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "value ('" & Today & "', " & hfOldTotalAmt.Value & ", "
                    SQL = SQL + "0, " & (CDbl(hfOldTotalAmt.Value) * -1)
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
                If clR.Fields("claimdetailstatus") = "Denied" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "value ('" & Today & "', " & hfOldTotalAmt.Value & ", "
                    SQL = SQL + "0, " & (CDbl(hfOldTotalAmt.Value) * -1)
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
            End If
            If hfStatus.Value = "Approved" Then
                If hfOldTotalAmt.Value = "" Then
                    hfOldTotalAmt.Value = 0
                End If
                If CDbl(hfOldTotalAmt.Value) <> CDbl(clR.Fields("totalamt")) Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "values ('" & Today & "', " & hfOldTotalAmt.Value & ", "
                    SQL = SQL + clR.Fields("totalamt") & ", " & CDbl(clR.Fields("totalamt")) - CDbl(hfOldTotalAmt.Value)
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
                If clR.Fields("claimdetailstatus") = "Requested" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "value ('" & Today & "', " & hfOldTotalAmt.Value & ", "
                    SQL = SQL + "0, " & (CDbl(hfOldTotalAmt.Value) * -1)
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
                If clR.Fields("claimdetailstatus") = "Denied" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "value ('" & Today & "', " & hfOldTotalAmt.Value & ", "
                    SQL = SQL + "0, " & (CDbl(hfOldTotalAmt.Value) * -1)
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
            End If
            If hfStatus.Value = "Transmitted" Then
                If hfOldTotalAmt.Value = "" Then
                    hfOldTotalAmt.Value = 0
                End If
                If CDbl(hfOldTotalAmt.Value) <> CDbl(clR.Fields("totalamt")) Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "values ('" & Today & "', " & hfOldTotalAmt.Value & ", "
                    SQL = SQL + clR.Fields("totalamt") & ", " & CDbl(clR.Fields("totalamt")) - CDbl(hfOldTotalAmt.Value)
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
                If clR.Fields("claimdetailstatus") = "Requested" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "value ('" & Today & "', " & hfOldTotalAmt.Value & ", "
                    SQL = SQL + "0, " & (CDbl(hfOldTotalAmt.Value) * -1)
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
                If clR.Fields("claimdetailstatus") = "Denied" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "value ('" & Today & "', " & hfOldTotalAmt.Value & ", "
                    SQL = SQL + "0, " & (CDbl(hfOldTotalAmt.Value) * -1)
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
            End If
            If hfStatus.Value = "Paid" Then
                If hfOldTotalAmt.Value = "" Then
                    hfOldTotalAmt.Value = 0
                End If
                If CDbl(hfOldTotalAmt.Value) <> CDbl(clR.Fields("totalamt")) Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "values ('" & Today & "', " & hfOldTotalAmt.Value & ", "
                    SQL = SQL + clR.Fields("totalamt") & ", " & CDbl(clR.Fields("totalamt")) - CDbl(hfOldTotalAmt.Value)
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
                If clR.Fields("claimdetailstatus") = "Requested" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "value ('" & Today & "', " & hfOldTotalAmt.Value & ", "
                    SQL = SQL + "0, " & (CDbl(hfOldTotalAmt.Value) * -1)
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
                If clR.Fields("claimdetailstatus") = "Denied" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "value ('" & Today & "', " & hfOldTotalAmt.Value & ", "
                    SQL = SQL + "0, " & (CDbl(hfOldTotalAmt.Value) * -1)
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
            End If
            If hfStatus.Value = "New" Then
                If clR.Fields("claimdetailstatus") = "Authorized" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "values ('" & Today & "', 0, " & clR.Fields("totalamt") & ", " & clR.Fields("totalamt")
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
                If clR.Fields("claimdetailstatus") = "Approved" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "values ('" & Today & "', 0, " & clR.Fields("totalamt") & ", " & clR.Fields("totalamt")
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
                If clR.Fields("claimdetailstatus") = "Paid" Then
                    SQL = "insert into claimdetailchange "
                    SQL = SQL + "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) "
                    SQL = SQL + "values ('" & Today & "', 0, " & clR.Fields("totalamt") & ", " & clR.Fields("totalamt")
                    SQL = SQL + ", " & hfUserID.Value & ", " & hfClaimDetailID.Value & ")"
                    clR.RunSQL(SQL, AppSettings("connstring"))
                End If
            End If
        End If
    End Sub

    Private Function CheckMissingInfo() As Boolean
        CheckMissingInfo = False
        If CheckBypassMissingInfo() Then
            Exit Function
        End If
        Dim sMessage As String
        Dim SQL As String
        Dim clR As New clsDBO
        Dim sServiceCenter As String = "0"
        Dim l As Long
        sMessage = "Check your funding for claim and you are missing the following fields." & vbCrLf
        l = sMessage.Length
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("lossmile").Length = 0 Then
                sMessage = sMessage + "Loss Mile" & vbCrLf
            Else
                If clR.Fields("lossmile") = "0" Then
                    sMessage = sMessage + "Loss Mile" & vbCrLf
                End If
            End If
            If clR.Fields("servicecenterid").Length = 0 Then
                sMessage = sMessage + "Service Center" & vbCrLf
            Else
                If clR.Fields("servicecenterid") = "0" Then
                    sMessage = sMessage + "Service Center" & vbCrLf
                End If
            End If
            If clR.Fields("claimactivityid").Length = 0 Then
                sMessage = sMessage + "Claim Activity" & vbCrLf
            Else
                If clR.Fields("claimactivityid") = "0" Then
                    sMessage = sMessage + "Claim Activity" & vbCrLf
                End If
            End If
            If clR.Fields("sccontactinfo").Length = 0 Then
                sMessage = sMessage + "Service Center Contact Info" & vbCrLf
            End If
            sServiceCenter = clR.Fields("servicecenterid")
        End If
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnotetypeid = 1 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            sMessage = sMessage + "Complaint" & vbCrLf
        End If
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnotetypeid = 2 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            sMessage = sMessage + "Corrective Action" & vbCrLf
        End If
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnotetypeid = 3 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            sMessage = sMessage + "Cause" & vbCrLf
        End If
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and not claimnotetypeid in (1,2,3,8) "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            sMessage = sMessage + "Notes" & vbCrLf
        End If
        If Not CheckAN() Then
            SQL = "select * from claimnote "
            SQL = SQL + "where claimid = " & hfClaimID.Value & " "
            SQL = SQL + "and claimnotetypeid = 8 "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount = 0 Then
                sMessage = sMessage + "Customer Statement" & vbCrLf
            End If
        End If

        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimdetailtype in ('Part', 'Labor') "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                If clR.Fields("ReqQty").Length = 0 Then
                    sMessage = sMessage + clR.Fields("jobno") + " Request Qty" & vbCrLf
                Else
                    If clR.Fields("ReqCost") = "0" Then
                        sMessage = sMessage + clR.Fields("jobno") + " Request Cost" & vbCrLf
                    End If
                End If
                If clR.Fields("AuthQty").Length = 0 Then
                    sMessage = sMessage + clR.Fields("jobno") + " Authorized Qty" & vbCrLf
                Else
                    If clR.Fields("AuthQty") = "0" Then
                        sMessage = sMessage + clR.Fields("jobno") + " Authorized Qty" & vbCrLf
                    End If
                End If
                If clR.Fields("AuthCost") = 0 Then
                    sMessage = sMessage + clR.Fields("jobno") + " Authorized Cost" & vbCrLf
                End If
                If clR.Fields("losscode").Length = 0 Then
                    sMessage = sMessage + clR.Fields("jobno") + " Loss Code" & vbCrLf
                End If
            Next
        End If
        If sMessage.Length > 72 Then
            CheckMissingInfo = True
            txtFieldChk.Text = sMessage
            ShowFieldCheck()
        Else
            txtFieldChk.Text = "Check your funding for claim!"
        End If
    End Function

    Private Function CheckBypassMissingInfo() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckBypassMissingInfo = False
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and BypassMissingInfo <> 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckBypassMissingInfo = True
        End If

    End Function

    Private Function CheckAN() As Boolean
        CheckAN = False
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from claim cl "
        SQL = SQL + "inner join contract c on c.contractid = cl.contractid "
        SQL = SQL + "inner join dealer d on d.dealerid = c.dealerid "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and dealerno like '2%' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckAN = True
        End If
    End Function

    Private Sub CheckClose()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select claimdetailid from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimdetailstatus = 'Requested' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            CloseClaim()
            If cboClaimDetailStatus.SelectedValue = "Authorized" Then
                UpdateAuthorizedDate()
            End If
            If cboClaimDetailStatus.SelectedValue = "Approved" Then
                UpdateApproveDate()
            End If
        End If
    End Sub

    Private Sub UpdateApproveDate()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update claim "
        SQL = SQL + "set approvedate = '" & DateTime.Today & "', "
        SQL = SQL + "approveby = " & hfUserID.Value & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub UpdateAuthorizedDate()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update claim "
        SQL = SQL + "set authdate = '" & DateTime.Today & "', "
        SQL = SQL + "authby = " & hfUserID.Value & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub CloseClaim()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update claim "
        SQL = SQL + "set closedate = '" & DateTime.Today & "', "
        SQL = SQL + "closeby = " & hfUserID.Value & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and closedate is null "
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Function CheckLimitApproved(xAmt As Double, xClaimDetailID As Long) As Boolean
        CheckLimitApproved = False
        Dim SQL As String
        Dim clR As New clsDBO
        Dim dLimit As Double
        SQL = "select claimapprove from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            dLimit = clR.Fields("claimapprove")
        Else
            dLimit = 0
        End If
        SQL = "select sum(authamt) as Amt from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (claimdetailstatus = 'approved' "
        SQL = SQL + "or claimdetailstatus = 'Authorized') "
        SQL = SQL + "and claimdetailid <> " & xClaimDetailID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("amt").Length > 0 Then
                If dLimit > CDbl(clR.Fields("amt")) + xAmt Then
                    CheckLimitApproved = True
                End If
            Else
                If dLimit > xAmt Then
                    CheckLimitApproved = True
                End If
            End If
        End If
    End Function

    Private Function CheckLimitAuthorized(xAmt As Double, xClaimDetailID As Long) As Boolean
        CheckLimitAuthorized = False
        Dim SQL As String
        Dim clR As New clsDBO
        Dim dLimit As Double
        SQL = "select claimauth from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            dLimit = clR.Fields("claimauth")
        Else
            dLimit = 0
        End If
        SQL = "select sum(authamt) as Amt from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (claimdetailstatus = 'approved' "
        SQL = SQL + "or claimdetailstatus = 'Authorized') "
        SQL = SQL + "and claimdetailid <> " & xClaimDetailID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("amt").Length > 0 Then
                If dLimit > CDbl(clR.Fields("amt")) + xAmt Then
                    CheckLimitAuthorized = True
                End If
            Else
                If dLimit > xAmt Then
                    CheckLimitAuthorized = True
                End If
            End If
        End If
    End Function

    Private Sub rgLossCode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgLossCode.SelectedIndexChanged
        txtLossCode.Text = rgLossCode.SelectedValue
        GetLossCodeDesc()
        GetNextJobNo()
        pnlLossCode.Visible = False
        pnlDetail.Visible = True
        LockAuthText()
    End Sub

    Private Sub btnAddClaimPayee_Click(sender As Object, e As EventArgs) Handles btnAddClaimPayee.Click
        pnlSeekPayee.Visible = False
        pnlAddClaimPayee.Visible = True
        txtServiceCenterName.Text = ""
        txtZip.Text = ""
        txtAddr1.Text = ""
        txtAddr2.Text = ""
        cboState.ClearSelection()
        txtPhone.Text = ""
    End Sub

    Private Sub rgClaimPayee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimPayee.SelectedIndexChanged
        hfClaimPayeeID.Value = rgClaimPayee.SelectedValue
        GetPayee()
        pnlDetail.Visible = True
        pnlSeekPayee.Visible = False
    End Sub

    Private Sub btnSCCancel_Click(sender As Object, e As EventArgs) Handles btnSCCancel.Click
        pnlSeekPayee.Visible = True
        pnlAddClaimPayee.Visible = False
    End Sub

    Private Sub btnSCSave_Click(sender As Object, e As EventArgs) Handles btnSCSave.Click
        AddServiceCenter()
        AddClaimPayee()
        FillClaimPayee()
        pnlSeekPayee.Visible = True
        pnlAddClaimPayee.Visible = False

    End Sub

    Private Sub FillClaimPayee()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select claimpayeeid, payeeno, payeename, city, state from claimpayee "
        rgClaimPayee.DataSource = clR.GetData(SQL, AppSettings("connstring"))
    End Sub

    Private Sub AddClaimPayee()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimpayee "
        SQL = SQL + "where claimpayeeid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.NewRow()
        clR.Fields("payeeno") = hfServiceCenterNo.Value
        clR.Fields("payeename") = txtServiceCenterName.Text
        clR.Fields("addr1") = txtAddr1.Text
        clR.Fields("addr2") = txtAddr2.Text
        clR.Fields("city") = txtCity.Text
        clR.Fields("state") = cboState.SelectedValue
        clR.Fields("zip") = txtZip.Text
        clR.Fields("phone") = txtPhone.Text
        clR.AddRow()
        clR.SaveDB()
    End Sub

    Private Sub AddServiceCenter()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.NewRow()
        GetNextServiceCenterNo()
        clR.Fields("servicecenterno") = hfServiceCenterNo.Value
        clR.Fields("servicecentername") = txtServiceCenterName.Text
        clR.Fields("addr1") = txtAddr1.Text
        clR.Fields("addr2") = txtAddr2.Text
        clR.Fields("city") = txtCity.Text
        clR.Fields("state") = cboState.SelectedValue
        clR.Fields("zip") = txtZip.Text
        clR.Fields("phone") = txtPhone.Text
        clR.AddRow()
        clR.SaveDB()
    End Sub

    Private Sub GetNextServiceCenterNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select max(servicecenterno) as mSCN from servicecenter "
        SQL = SQL + "where servicecenterno like 'RF%' "
        clR.OpenDB(SQL, AppSettings("connstring"))

        clR.GetRow()
        hfServiceCenterNo.Value = "RF" & Format(CLng(Right(clR.Fields("mscn"), 6)) + 1, "0000000")
    End Sub

    Private Function CheckInspect() As Boolean
        CheckInspect = False
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select inspect from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("inspect") = "True" Then
                CheckInspect = True
            End If
        End If
    End Function

    Private Sub FillTotal()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select 'Requested' as ClaimProcess, (select sum(reqamt) as Part from claimdetail where ClaimDetailType = 'Part' "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and claimid = " & hfClaimID.Value & ") as Part, "
        SQL = SQL + "(select sum(reqamt) as Labor from claimdetail where ClaimDetailType = 'Labor' "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + " and claimid = " & hfClaimID.Value & ") as Labor, "
        SQL = SQL + "(select sum(reqamt) as Other from claimdetail where (ClaimDetailType = 'Inspect' or ClaimDetailType = 'Other') "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + " and claimid = " & hfClaimID.Value & ") as Other, "
        SQL = SQL + "(select sum(reqamt) as SubTotal from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + " and claimdetailtype <> 'Deduct') as SubTotal, "
        SQL = SQL + "0 as taxAmt, "
        SQL = SQL + "(select sum(reqamt) as 'Deduct' from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.Text & "' "
        End If
        SQL = SQL + "and claimdetailtype = 'Deductible') AS Deduct, "
        SQL = SQL + "(select sum(reqamt) as Total from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + ")  as Total, 1 as orderby "
        SQL = SQL + "union "
        SQL = SQL + "select 'Adjusted' as ClaimProcess, (select sum(authamt) as Part from claimdetail where ClaimDetailType = 'Part' "
        SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied' "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and claimid = " & hfClaimID.Value & ") as Part, "
        SQL = SQL + "(select sum(authamt) as Labor from claimdetail where ClaimDetailType = 'Labor' and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied' "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and claimid = " & hfClaimID.Value & ") as Labor, "
        SQL = SQL + "(select sum(authamt) as Other from claimdetail where (ClaimDetailType = 'Inspect' or ClaimDetailType = 'Other') "
        SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied'"
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and claimid = " & hfClaimID.Value & ") as Other, "
        SQL = SQL + "(select sum(authamt) as SubTotal from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied' "
        SQL = SQL + "and claimdetailtype <> 'Deduct') as SubTotal, "
        SQL = SQL + "(select sum(taxamt) as TaxAmt from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied') as TaxAmt, "
        SQL = SQL + "(select sum(authamt) as 'Deduct' from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied'"
        SQL = SQL + "and claimdetailtype = 'Deductible') AS Deduct, "
        SQL = SQL + "(select sum(authamt) + sum(taxamt) as Total from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied')  as Total, 2 as orderby  "
        SQL = SQL + "union "
        SQL = SQL + "select 'To Pay' as ClaimProcess, (select sum(authamt) as Part from claimdetail where ClaimDetailType = 'Part' "
        SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid') "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and claimid = " & hfClaimID.Value & ") as Part, "
        SQL = SQL + "(select sum(authamt) as Labor from claimdetail where ClaimDetailType = 'Labor' and (ClaimDetailStatus = 'Approved' "
        SQL = SQL + " or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid') "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and claimid = " & hfClaimID.Value & ") as Labor, "
        SQL = SQL + "(select sum(authamt) as Other from claimdetail where (ClaimDetailType = 'Inspect' or ClaimDetailType = 'Other') "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid') and claimid = " & hfClaimID.Value & ") as Other, "
        SQL = SQL + "(select sum(authamt) as SubTotal from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' "
        SQL = SQL + "or ClaimDetailStatus = 'Paid') and claimdetailtype <> 'Deduct') as SubTotal, "
        SQL = SQL + "(select sum(taxamt) as TaxAmt from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' "
        SQL = SQL + "or ClaimDetailStatus = 'Paid')) as TaxAmt, "
        SQL = SQL + "(select sum(authamt) as 'Deduct' from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid') "
        SQL = SQL + "and claimdetailtype = 'Deductible') AS Deduct, (select sum(authamt) + sum(taxamt) as Total from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid'))  as Total, 3 as orderby "
        SQL = SQL + "union "
        SQL = SQL + "select 'Paid' as ClaimProcess, (select sum(authamt) as Part from claimdetail where ClaimDetailType = 'Part' "
        SQL = SQL + "and ClaimDetailStatus = 'Paid' "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and claimid = " & hfClaimID.Value & ") as Part, "
        SQL = SQL + "(select sum(authamt) as Labor from claimdetail where ClaimDetailType = 'Labor' and (ClaimDetailStatus = 'Paid') "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and claimid = " & hfClaimID.Value & ") as Labor, (select sum(authamt) as Other from claimdetail where (ClaimDetailType = 'Inspect' "
        SQL = SQL + "or ClaimDetailType = 'Other') and (ClaimDetailStatus = 'Paid') "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and claimid = " & hfClaimID.Value & ") as Other, "
        SQL = SQL + "(select sum(authamt) as SubTotal from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.Text & "' "
        End If
        SQL = SQL + "and (ClaimDetailStatus = 'Paid') "
        SQL = SQL + "and claimdetailtype <> 'Deduct') as SubTotal, (select sum(taxamt) as TaxAmt from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and (ClaimDetailStatus = 'Paid')) as TaxAmt, "
        SQL = SQL + "(select sum(authamt) as 'Deduct' from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and (ClaimDetailStatus = 'Paid') "
        SQL = SQL + "and claimdetailtype = 'Deductible') AS Deduct, (select sum(authamt) + sum(taxamt) as Total from claimdetail where claimid = " & hfClaimID.Value & " "
        If cboPayeeTotal.SelectedValue > 0 Then
            SQL = SQL + "and claimpayeeid = " & cboPayeeTotal.SelectedValue & " "
        End If
        If cboJobsTotal.SelectedValue > 0 Then
            SQL = SQL + "and jobno = '" & cboJobsTotal.SelectedItem.Text & "' "
        End If
        SQL = SQL + "and (ClaimDetailStatus = 'Paid'))  as Total, 4 as orderby "
        SQL = SQL + "order by orderby "
        rgClaimTotal.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgClaimTotal.DataBind()
    End Sub

    Private Sub FillPayeeTotal()
        Dim li As ListItem
        Dim SQL As String
        Dim clR As New clsDBO
        li = New ListItem
        li.Value = 0
        li.Text = "All"
        cboPayeeTotal.Items.Add(li)
        SQL = "select cp.claimpayeeid, payeeno, payeename from claimdetail cd "
        SQL = SQL + "inner join claimpayee cp on cp.claimpayeeid = cd.claimpayeeid "
        SQL = SQL + "where cd.claimid = " & hfClaimID.Value & " "
        SQL = SQL + "group by cp.claimpayeeid, payeeno, payeename "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                li = New ListItem
                li.Value = clR.Fields("claimpayeeid")
                li.Text = clR.Fields("payeeno") & "-" & clR.Fields("payeename")
                cboPayeeTotal.Items.Add(li)
            Next
        End If
    End Sub

    Private Sub cboPayeeTotal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPayeeTotal.SelectedIndexChanged
        FillTotal()
    End Sub

    Private Sub FillPaymentMethod()
        cboPaymentMethod.Items.Clear()
        cboPaymentMethod.Items.Add("")
        cboPaymentMethod.Items.Add("ACH")
        cboPaymentMethod.Items.Add("Check")
        cboPaymentMethod.Items.Add("Credit Card")
        cboPaymentMethod.Items.Add("Wex")
    End Sub

    Private Sub btnMakePayment_Click(sender As Object, e As EventArgs) Handles btnMakePayment.Click
        pnlToBePaidDetail.Visible = True
        FillToBePaidDetail()
    End Sub

    Private Sub FillToBePaidDetail()
        GetPayeeInfo()
        txtPaidAmt.Text = txtPayAmt.Text
        FillPaymentMethod()
        txtConfirmNo.Text = ""
        txtCheckNo.Text = ""
    End Sub

    Private Sub GetPayeeInfo()

        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimpayee "
        SQL = SQL + "where claimpayeeid = " & hfClaimPayeeID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtClaimPayeeNo.Text = clR.Fields("payeeno")
            txtClaimPayeeName.Text = clR.Fields("payeename")
        End If
    End Sub

    Private Sub btnProcessACH_Click(sender As Object, e As EventArgs) Handles btnProcessACH.Click
        ProcessPayment()
        pnlToBePaidDetail.Visible = False
        trACH.Visible = False
        rgJobs.Rebind()
    End Sub

    Private Sub ProcessPayment()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clCPL As New clsDBO
        AddClaimPayment()
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value & " "
        SQL = SQL + "and claimpayeeid = " & hfClaimPayeeID.Value & " "
        SQL = SQL + "and claimdetailstatus = 'approved' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from claimpaymentlink "
                SQL = SQL + "where claimdetailid = " & clR.Fields("claimdetailid") & " "
                SQL = SQL + "and claimpaymentid = " & hfClaimPaymentID.Value & " "
                clCPL.OpenDB(SQL, AppSettings("connstring"))
                If clCPL.RowCount = 0 Then
                    clCPL.NewRow()
                    clCPL.Fields("claimdetailid") = clR.Fields("claimdetailid")
                    clCPL.Fields("claimpaymentid") = hfClaimPaymentID.Value
                    clCPL.AddRow()
                    clCPL.SaveDB()
                End If
                SQL = "update claimdetail "
                If cboPaymentMethod.Text <> "Wex" Then
                    SQL = SQL + "set claimdetailstatus = 'Paid', "
                    SQL = SQL + "datepaid = '" & DateTime.Today & "', "
                    SQL = SQL + "modby = " & hfUserID.Value & ", "
                    SQL = SQL + "moddate = '" & DateTime.Today & "' "
                Else
                    SQL = SQL + "set claimdetailstatus = 'Transmitted To Wex', "
                    SQL = SQL + "modby = " & hfUserID.Value & ", "
                    SQL = SQL + "moddate = '" & DateTime.Today & "' "
                End If
                SQL = SQL + "where claimdetailid = " & clR.Fields("claimdetailid")
                clCPL.RunSQL(SQL, AppSettings("connstring"))
            Next
        End If
    End Sub

    Private Sub AddClaimPayment()
        Dim SQL As String
        Dim clCP As New clsDBO
        SQL = "select * from claimpayment "
        SQL = SQL + "where claimpaymentid = 0 "
        clCP.OpenDB(SQL, AppSettings("connstring"))
        If clCP.RowCount = 0 Then
            clCP.NewRow()
            clCP.Fields("claimpayeeid") = hfClaimPayeeID.Value
            clCP.Fields("paymentamt") = txtPayAmt.Text
            If cboPaymentMethod.Text = "ACH" Then
                clCP.Fields("datepaid") = DateTime.Today
                clCP.Fields("achinfo") = txtConfirmNo.Text
                clCP.Fields("status") = "Paid"
            End If
            If cboPaymentMethod.Text = "Check" Then
                clCP.Fields("datepaid") = DateTime.Today
                clCP.Fields("checkno") = txtCheckNo.Text
                clCP.Fields("status") = "Paid"
            End If
            If cboPaymentMethod.Text = "Credit Card" Then
                clCP.Fields("datepaid") = DateTime.Today
                clCP.Fields("checkno") = txtCCNo.Text
                clCP.Fields("status") = "Paid"
            End If
            If cboPaymentMethod.Text = "Wex" Then
                clCP.Fields("datetransmitted") = DateTime.Today
                clCP.Fields("status") = "Transmitted To Wex"
                clCP.Fields("ccno") = hfWexCCno.Value
                clCP.Fields("wexcode") = hfWexCode.Value
                clCP.Fields("wexdeliverymethod") = cboWexMethod.SelectedText
                clCP.Fields("wexdeliveryaddress") = txtWexAddress.Text
                clCP.Fields("companyno") = hfCompanyNo.Value
            End If
            clCP.AddRow()
            clCP.SaveDB()
        End If

        SQL = "select max(claimpaymentid) as mCPI from claimpayment "
        SQL = SQL + "where claimpayeeid = " & hfClaimPayeeID.Value
        clCP.OpenDB(SQL, AppSettings("connstring"))
        If clCP.RowCount > 0 Then
            clCP.GetRow()
            hfClaimPaymentID.Value = clCP.Fields("mcpi")
        End If
    End Sub

    Private Sub btnProcessCC_Click(sender As Object, e As EventArgs) Handles btnProcessCC.Click
        ProcessPayment()
        pnlToBePaidDetail.Visible = False
        trCheck.Visible = False
        rgJobs.Rebind()
    End Sub

    Private Sub cboPaymentMethod_TextChanged(sender As Object, e As EventArgs) Handles cboPaymentMethod.TextChanged
        trACH.Visible = False
        trWex.Visible = False
        trCheck.Visible = False
        trCC.Visible = False
        If cboPaymentMethod.Text = "ACH" Then
            trACH.Visible = True
        End If
        If cboPaymentMethod.Text = "Check" Then
            trCheck.Visible = True
        End If
        If cboPaymentMethod.Text = "Credit Card" Then
            trCC.Visible = True
        End If
        If cboPaymentMethod.Text = "Wex" Then
            trWex.Visible = True
        End If
    End Sub

    Private Sub btnProcessWex_Click(sender As Object, e As EventArgs) Handles btnProcessWex.Click
        ProcessWexDLL()
        pnlToBePaidDetail.Visible = False
        rgJobs.Rebind()
        trWex.Visible = False
    End Sub

    Private Sub btnProcessCheck_Click(sender As Object, e As EventArgs) Handles btnProcessCheck.Click
        ProcessPayment()
        pnlToBePaidDetail.Visible = False
        trCheck.Visible = False
        rgJobs.Rebind()
    End Sub

    Private Sub ProcessWexDLL()
        Dim clCWP As New clsDBO
        Dim clR As New clsDBO

        GetClaimNo()
        If AppSettings("connstring") = "server=142.11.233.98;database=veritastest;User Id=sa;Password=NCC1701E" Then
            Exit Sub
        Else
            GetCompanyNo()
            Dim SQL As String
            Dim clWA As New clsDBO
            SQL = "select * from wexapi "
            SQL = SQL + "where companyno = '" & hfCompanyNo.Value & "' "
            clWA.OpenDB(SQL, AppSettings("connstring"))
            If clWA.RowCount > 0 Then
                clWA.GetRow()
                GetClaimNo()
                SQL = "select * from claimwexpayment "
                SQL = SQL + "where claimwexpaymentid = 0 "
                clCWP.OpenDB(SQL, AppSettings("connstring"))
                If clCWP.RowCount = 0 Then
                    clCWP.NewRow()
                    clCWP.Fields("claimno") = hfClaimNo.Value
                    clCWP.Fields("claimid") = hfClaimID.Value
                    clCWP.Fields("rono") = hfRONo.Value
                    clCWP.Fields("orggroup") = clWA.Fields("OrgGroup")
                    clCWP.Fields("username") = clWA.Fields("UserName")
                    clCWP.Fields("password") = clWA.Fields("Password")
                    clCWP.Fields("currency") = clWA.Fields("currency")
                    clCWP.Fields("wexaddress") = txtWexAddress.Text
                    clCWP.Fields("wexmethod") = cboWexMethod.SelectedValue
                    clCWP.Fields("bankno") = clWA.Fields("bankno")
                    clCWP.Fields("companyno") = hfCompanyNo.Value
                    clCWP.Fields("paymentamt") = txtPayAmt.Text
                    clCWP.Fields("contractno") = hfContractNo.Value
                    clCWP.Fields("customername") = hfCustomerName.Value
                    clCWP.Fields("Claimpayeename") = txtClaimPayeeName.Text
                    clCWP.Fields("payeecontact") = hfPayeeContact.Value
                    clCWP.Fields("vin") = hfVIN.Value
                    clCWP.AddRow()
                    clCWP.SaveDB()
                    SQL = "select max(claimwexpaymentid) as MCWP from claimwexpayment "
                    SQL = SQL + "where claimid = " & hfClaimID.Value
                    clCWP.OpenDB(SQL, AppSettings("connstring"))
                    If clCWP.RowCount > 0 Then
                        clCWP.GetRow()
                        If Not AppSettings("connstring").ToLower.Contains("test") Then
                            Dim pr As New Process
                            pr.StartInfo.FileName = "C:\ProcessProgram\WexPayment\WexPayment.exe"
                            pr.StartInfo.Arguments = clCWP.Fields("mcwp")
                            pr.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
                            pr.Start()
                            pr.WaitForExit()
                            pr.Close()
                            SQL = "select * from claimwexpayment "
                            SQL = SQL + "where claimwexpaymentid = " & clCWP.Fields("mcwp")
                            clR.OpenDB(SQL, AppSettings("connstring"))
                            If clR.RowCount > 0 Then
                                clR.GetRow()
                                If clR.Fields("ReasonCode").ToLower = "success" Then
                                    hfWexCCno.Value = clR.Fields("wexccno").Substring(0, 4)
                                    hfWexCode.Value = clR.Fields("wexcode")
                                    ProcessPayment()
                                    pnlToBePaidDetail.Visible = False
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub GetClaimNo()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfClaimNo.Value = clR.Fields("claimno")
            hfRONo.Value = clR.Fields("RONumber")
            hfContractID.Value = clR.Fields("contractid")
            OpenContract()
            hfPayeeContact.Value = clR.Fields("sccontactinfo")
        End If
    End Sub

    Private Sub GetCompanyNo()
        Dim clCL As New clsDBO
        Dim SQL As String
        SQL = "select * from claim cl "
        SQL = SQL + "inner join contract c on c.contractid = cl.contractid "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clCL.OpenDB(SQL, AppSettings("connstring"))
        If clCL.RowCount > 0 Then
            clCL.GetRow()
            If clCL.Fields("clipid") = 1 Or clCL.Fields("clipid") = 2 Then
                hfCompanyNo.Value = "0000978"
            End If
            If clCL.Fields("clipid") = 3 Or clCL.Fields("clipid") = 4 Then
                If clCL.Fields("contractno").Substring(0, 3) = "CHJ" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "DRV" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RAC" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RAD" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RAN" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RDI" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "REP" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RSA" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RSD" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RSW" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "VEL" Then
                    hfCompanyNo.Value = "0000978"
                Else
                    hfCompanyNo.Value = "0000976"
                End If
            End If
            If clCL.Fields("clipid") = 5 Or clCL.Fields("clipid") = 6 Then
                hfCompanyNo.Value = "0000976"
            End If
            If clCL.Fields("clipid") = 7 Or clCL.Fields("clipid") = 8 Then
                hfCompanyNo.Value = "0000976"
            End If
            If clCL.Fields("clipid") = 9 Or clCL.Fields("clipid") = 10 Then
                hfCompanyNo.Value = "0000977"
            End If
            If clCL.Fields("clipid") = 11 Then
                hfCompanyNo.Value = "0000978"
            End If
        End If
    End Sub

    Private Sub OpenContract()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfContractNo.Value = clR.Fields("contractno")
            hfCustomerName.Value = clR.Fields("fname") & " " & clR.Fields("lname")
            hfVIN.Value = clR.Fields("vin")
        End If
    End Sub

    Private Sub txtReqQty_TextChanged(sender As Object, e As EventArgs) Handles txtReqQty.TextChanged
        Dim dQty As Double
        Dim dCost As Double
        If txtReqQty.Text.Length > 0 Then
            dQty = txtReqQty.Text
        Else
            dQty = 0
        End If
        If txtReqCost.Text.Length > 0 Then
            dCost = txtReqCost.Text
        Else
            dCost = 0
        End If
        txtReqAmt.Text = dQty * dCost
        txtReqCost.Focus()
    End Sub

    Private Sub txtReqCost_TextChanged(sender As Object, e As EventArgs) Handles txtReqCost.TextChanged
        Dim dQty As Double
        Dim dCost As Double
        If txtReqQty.Text.Length > 0 Then
            dQty = txtReqQty.Text
        Else
            dQty = 0
        End If
        If txtReqCost.Text.Length > 0 Then
            dCost = txtReqCost.Text
        Else
            dCost = 0
        End If
        txtReqAmt.Text = dQty * dCost
        txtReqAmt.Focus()
    End Sub

    Private Sub txtAuthQty_TextChanged(sender As Object, e As EventArgs) Handles txtAuthQty.TextChanged
        Dim dQty As Double
        Dim dCost As Double
        Dim dTax As Double
        Dim dTaxRate As Double
        If txtAuthQty.Text.Length > 0 Then
            dQty = txtAuthQty.Text
        Else
            dQty = 0
        End If
        If txtAuthCost.Text.Length > 0 Then
            dCost = txtAuthCost.Text
        Else
            dCost = 0
        End If
        If txtTaxRate.Text.Length > 0 Then
            If txtTaxRate.Text < 1 Then
                dTaxRate = txtTaxRate.Text
            Else
                dTaxRate = txtTaxRate.Text / 100
            End If
        Else
            dTaxRate = 0
        End If
        dTax = (dQty * dCost) * dTaxRate
        txtTaxAmt.Text = dTax
        txtAuthAmt.Text = (dQty * dCost)
        txtTotalAmt.Text = (dQty * dCost) + dTax
        txtAuthCost.Focus()
    End Sub

    Private Sub txtAuthCost_TextChanged(sender As Object, e As EventArgs) Handles txtAuthCost.TextChanged
        Dim dQty As Double
        Dim dCost As Double
        Dim dTax As Double
        Dim dTaxRate As Double
        If txtAuthQty.Text.Length > 0 Then
            dQty = txtAuthQty.Text
        Else
            dQty = 0
        End If
        If txtAuthCost.Text.Length > 0 Then
            dCost = txtAuthCost.Text
        Else
            dCost = 0
        End If
        If txtTaxRate.Text.Length > 0 Then
            dTaxRate = CDbl(txtTaxRate.Text) / 100
        Else
            dTaxRate = 0
        End If
        dTax = (dQty * dCost) * dTaxRate
        dTax = Math.Round((dTax), 2)
        txtTaxAmt.Text = dTax
        txtAuthAmt.Text = (dQty * dCost)
        txtTotalAmt.Text = (dQty * dCost) + dTax
        txtTaxRate.Focus()
    End Sub

    Private Sub txtTaxRate_TextChanged(sender As Object, e As EventArgs) Handles txtTaxRate.TextChanged
        Dim dQty As Double
        Dim dCost As Double
        Dim dTax As Double
        Dim dTaxRate As Double
        If txtAuthQty.Text.Length > 0 Then
            dQty = txtAuthQty.Text
        Else
            dQty = 0
        End If
        If txtAuthCost.Text.Length > 0 Then
            dCost = txtAuthCost.Text
        Else
            dCost = 0
        End If
        If txtTaxRate.Text.Length > 0 Then
            dTaxRate = CDbl(txtTaxRate.Text) / 100
        Else
            dTaxRate = 0
        End If
        dTax = (dQty * dCost) * dTaxRate
        dTax = (Decimal.Round(dTax * 100) / 100)
        txtTaxAmt.Text = dTax
        txtAuthAmt.Text = (dQty * dCost)
        txtTotalAmt.Text = (dCost * dQty) + dTax
        txtTotalAmt.Focus()
    End Sub

    Private Sub cboClaimDetailStatus_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboClaimDetailStatus.SelectedIndexChanged
        If cboClaimDetailStatus.SelectedValue = "Approved" Then
            If rdpDateApprove.SelectedDate Is Nothing Then
                rdpDateApprove.SelectedDate = DateTime.Today
                CalcAuth()
            End If
            If rdpAuthorizedDate.SelectedDate Is Nothing Then
                rdpAuthorizedDate.SelectedDate = DateTime.Today
            End If
        End If
        If cboClaimDetailStatus.SelectedValue = "Authorized" Then
            If Not rdpDateApprove.SelectedDate Is Nothing Then
                rdpDateApprove.Clear()
            End If
            If rdpAuthorizedDate.SelectedDate Is Nothing Then
                rdpAuthorizedDate.SelectedDate = DateTime.Today
            End If
        End If
        If cboClaimDetailStatus.SelectedValue = "Cancelled" Then
            rdpDateApprove.Clear()
            rdpAuthorizedDate.Clear()
        End If
        If cboClaimDetailStatus.SelectedValue = "Denied" Then
            rdpDateApprove.Clear()
            rdpAuthorizedDate.Clear()
            If txtJobNo.Text.Substring(0, 1) = "J" Then
                hfTicket.Value = "True"
            End If
        End If
        If cboClaimDetailStatus.SelectedValue = "Paid" Then
            rdpDatePaid.SelectedDate = DateTime.Today
            txtPaidAmt.Text = txtTotalAmt.Text
        End If
        CheckClose()
    End Sub

    Private Sub PlaceTicket()
        Dim SQL As String
        Dim clT As New clsDBO
        GetContractID()
        SQL = "select * from veritasclaimticket.dbo.ticket "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimdetailid = " & hfClaimDetailID.Value & " "
        clT.OpenDB(SQL, AppSettings("connstring"))
        If clT.RowCount = 0 Then
            If Not CheckContract() Then
                Exit Sub
            End If
            clT.NewRow()
            clT.Fields("ticketno") = CalcTicketNo()
            clT.Fields("contractid") = hfContractID.Value
            clT.Fields("claimid") = hfClaimID.Value
            clT.Fields("claimdetailid") = hfClaimDetailID.Value
            clT.Fields("credate") = Today
            clT.Fields("creby") = CalcUserName()
            clT.Fields("contractno") = GetContractNo()
            clT.Fields("claimno") = GetClaimNo2()
            clT.AddRow()
            clT.SaveDB()
        Else
            clT.GetRow()
            clT.Fields("statusid") = 1
            clT.SaveDB()
        End If
        'SQL = "select * from veritasclaimticket.dbo.ticket "
        'SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        'clT.OpenDB(SQL, AppSettings("connstring"))
        'If clT.RowCount = 1 Then
        '    clT.GetRow()
        '    Dim clSTE As New VeritasGlobalTools.clsSendTASAEMail
        '    clSTE.ClaimDetailID = hfClaimDetailID.Value
        '    clSTE.ClaimID = hfClaimID.Value
        '    clSTE.UserID = hfUserID.Value
        '    clSTE.TicketNo = clT.Fields("ticketno")
        '    clSTE.SendEMail()
        'End If

    End Sub

    Private Function GetClaimNo2() As String
        Dim SQL As String
        Dim clR As New clsDBO
        GetClaimNo2 = ""
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetClaimNo2 = clR.Fields("claimno")
        End If
    End Function

    Private Function GetContractNo() As String
        Dim SQL As String
        Dim clR As New clsDBO
        GetContractNo = ""
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetContractNo = clR.Fields("contractno")
        End If
    End Function

    Private Function CalcUserName() As String
        CalcUserName = ""
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            CalcUserName = clR.Fields("username")
        End If
    End Function

    Private Sub GetContractID()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfContractID.Value = clR.Fields("contractid")
        End If
    End Sub

    Private Function CalcTicketNo() As String
        Dim SQL As String
        Dim clR As New clsDBO
        Dim lTicketNo As Long
        CalcTicketNo = ""
        SQL = "select * from veritasclaimticket.dbo.ticket "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            CalcTicketNo = clR.Fields("ticketno")
            Exit Function
        End If
        SQL = "select * from veritasclaimticket.dbo.ticket "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            CalcTicketNo = "T000000"
            Exit Function
        End If
        SQL = "select max(ticketno) as TicketNo from veritasclaimticket.dbo.ticket "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            lTicketNo = CLng(clR.Fields("ticketno").Replace("T", ""))
            lTicketNo = lTicketNo + 1
            CalcTicketNo = "T" & Format(lTicketNo, "000000")
        End If
    End Function

    Private Function CheckContract() As Boolean
        CheckContract = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid in (select contractid from contract 
            where dealerid in (select dealerid from VeritasClaimTicket.dbo.Dealer)) "
        SQL = SQL + "and contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckContract = True
        End If
    End Function

    Private Sub CalcAuth()
        Dim dQty As Double
        Dim dCost As Double
        Dim dTax As Double
        Dim dTaxRate As Double
        If txtAuthQty.Text.Length > 0 Then
            dQty = txtAuthQty.Text
        Else
            dQty = 0
        End If
        If txtAuthCost.Text.Length > 0 Then
            dCost = txtAuthCost.Text
        Else
            dCost = 0
        End If
        If txtTaxRate.Text.Length > 0 Then
            dTaxRate = CDbl(txtTaxRate.Text) / 100
        Else
            dTaxRate = 0
        End If
        dTax = (dQty * dCost) * dTaxRate
        dTax = (Decimal.Round(dTax * 100) / 100)
        txtTaxAmt.Text = dTax
        txtAuthAmt.Text = (dQty * dCost)
        txtTotalAmt.Text = (dCost * dQty) + dTax
    End Sub

    Private Sub cboJobsTotal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboJobsTotal.SelectedIndexChanged
        FillTotal()
    End Sub

    Private Sub cboClaimDetailType_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboClaimDetailType.SelectedIndexChanged
        If cboClaimDetailType.SelectedItem.Text = "Deductible" Then
            txtJobNo.Text = "A01"
        End If
        If cboClaimDetailType.SelectedItem.Text = "Other" Then
            txtJobNo.Text = "A04"
        End If
        If cboClaimDetailType.SelectedItem.Text = "Inspect" Then
            txtJobNo.Text = "A02"
        End If

    End Sub

    Private Sub btnAuthIt_Click(sender As Object, e As EventArgs) Handles btnAuthIt.Click
        Dim clR As New clsDBO
        Dim clCD As New clsDBO
        Dim SQL As String
        Dim dQty As Double
        Dim dCost As Double
        Dim dTax As Double
        Dim dTaxRate As Double
        Dim bCopyReq As Boolean
        If cboJobNo.SelectedValue = "0" Then
            Exit Sub
        End If
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and jobno = '" & cboJobNo.SelectedValue & "' "
        SQL = SQL + "and claimdetailstatus = 'Requested' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from claimdetail "
                SQL = SQL + "where claimdetailid = " & clR.Fields("claimdetailid")
                clCD.OpenDB(SQL, AppSettings("connstring"))
                If clCD.RowCount > 0 Then
                    clCD.GetRow()
                    hfClaimDetailID.Value = clCD.Fields("claimdetailid")
                    hfStatus.Value = "Requested"
                    hfOldTotalAmt.Value = 0
                    bCopyReq = False
                    If clCD.Fields("authqty").Length > 0 Then
                        If clCD.Fields("authqty") = 0 Then
                            clCD.Fields("authqty") = clCD.Fields("reqqty")
                            bCopyReq = True
                        End If
                    Else
                        clCD.Fields("authqty") = clCD.Fields("reqqty")
                        bCopyReq = True
                    End If
                    If clCD.Fields("authcost").Length > 0 Then
                        If clCD.Fields("authcost") = 0 Then
                            clCD.Fields("authcost") = clCD.Fields("reqcost")
                            bCopyReq = True
                        End If
                    Else
                        clCD.Fields("authcost") = clCD.Fields("reqcost")
                        bCopyReq = True
                    End If

                    If clCD.Fields("authqty").Length > 0 Then
                        dQty = CDbl(clCD.Fields("authqty"))
                    Else
                        dQty = 0
                    End If
                    If clCD.Fields("authcost").Length > 0 Then
                        dCost = CDbl(clCD.Fields("authcost"))
                    Else
                        dCost = 0
                    End If
                    If clCD.Fields("taxper").Length > 0 Then
                        dTaxRate = CDbl(clCD.Fields("taxper")) / 100
                        If clCD.Fields("taxper") = "0" Then
                            dTaxRate = GetTaxRate(clCD.Fields("claimdetailtype"))
                            clCD.Fields("taxper") = dTaxRate
                        End If
                    Else
                        dTaxRate = GetTaxRate(clCD.Fields("claimdetailtype"))
                        clCD.Fields("taxper") = dTaxRate
                    End If
                    dTax = (dQty * dCost) * dTaxRate
                    dTax = (Decimal.Round(dTax * 100) / 100)
                    clCD.Fields("taxamt") = dTax
                    clCD.Fields("authamt") = (dQty * dCost)
                    clCD.Fields("totalamt") = (dCost * dQty) + dTax
                    If Not CheckLimitAuthorized(clCD.Fields("totalamt"), clCD.Fields("claimdetailid")) Then
                        lblAuthorizedError.Text = "Send to Manager for Authorization"
                        Exit Sub
                    End If
                    clCD.Fields("claimdetailstatus") = "Authorized"
                    clCD.Fields("authby") = hfUserID.Value
                    clCD.Fields("moddate") = Today
                    clCD.Fields("modby") = hfUserID.Value
                    clCD.Fields("dateauth") = DateTime.Today
                    clCD.SaveDB()
                    AddChange()
                End If
            Next
        End If
        SQL = "select claimdetailid from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimdetailstatus = 'Requested' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            CloseClaim()
            UpdateAuthorizedDate()
        End If
        rgJobs.Rebind()
        Dim clCD2 As New VeritasGlobalTools.clsClaimDetail
        clCD2.ProcessClaim(hfClaimID.Value)
        If clCD2.Refresh Then
            Response.Redirect("~/claim/claim.aspx?sid=" & hfID.Value & "&ClaimID=" & hfClaimID.Value)
        End If

    End Sub

    Private Sub btnApproveIt_Click(sender As Object, e As EventArgs) Handles btnApproveIt.Click
        Dim clR As New clsDBO
        Dim clCD As New clsDBO
        Dim SQL As String
        Dim dQty As Double
        Dim dCost As Double
        Dim dTax As Double
        Dim dTaxRate As Double
        Dim bCopyReq As Boolean
        If cboJobNo.SelectedValue = "0" Then
            Exit Sub
        End If
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and jobno = '" & cboJobNo.SelectedValue & "' "
        SQL = SQL + "and claimdetailstatus = 'Requested' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from claimdetail "
                SQL = SQL + "where claimdetailid = " & clR.Fields("claimdetailid")
                clCD.OpenDB(SQL, AppSettings("connstring"))
                If clCD.RowCount > 0 Then
                    clCD.GetRow()
                    hfStatus.Value = "Requested"
                    hfClaimDetailID.Value = clCD.Fields("claimdetailid")
                    hfOldTotalAmt.Value = 0
                    bCopyReq = False
                    If clCD.Fields("authqty").Length > 0 Then
                        If clCD.Fields("authqty") = 0 Then
                            clCD.Fields("authqty") = clCD.Fields("reqqty")
                            bCopyReq = True
                        End If
                    Else
                        clCD.Fields("authqty") = clCD.Fields("reqty")
                        bCopyReq = True
                    End If
                    If clCD.Fields("authcost").Length > 0 Then
                        If clCD.Fields("authcost") = 0 Then
                            clCD.Fields("authcost") = clCD.Fields("reqcost")
                            bCopyReq = True
                        End If
                    Else
                        clCD.Fields("authcost") = clCD.Fields("reqcost")
                        bCopyReq = True
                    End If

                    If clCD.Fields("authqty").Length > 0 Then
                        dQty = CDbl(clCD.Fields("authqty"))
                    Else
                        dQty = 0
                    End If
                    If clCD.Fields("authcost").Length > 0 Then
                        dCost = CDbl(clCD.Fields("authcost"))
                    Else
                        dCost = 0
                    End If
                    If clCD.Fields("taxper").Length > 0 Then
                        dTaxRate = CDbl(clCD.Fields("taxper")) / 100
                        If clCD.Fields("taxper") = "0" Then
                            dTaxRate = GetTaxRate(clCD.Fields("claimdetailtype"))
                            clCD.Fields("taxper") = dTaxRate
                        End If
                    Else
                        dTaxRate = GetTaxRate(clCD.Fields("claimdetailtype"))
                        clCD.Fields("taxrate") = dTaxRate
                    End If
                    dTax = (dQty * dCost) * dTaxRate
                    dTax = (Decimal.Round(dTax * 100) / 100)
                    clCD.Fields("taxamt") = dTax
                    clCD.Fields("authamt") = (dQty * dCost)
                    clCD.Fields("totalamt") = (dCost * dQty) + dTax
                    If Not CheckLimitApproved(clCD.Fields("totalamt"), clR.Fields("claimdetailid")) Then
                        lblAuthorizedError.Text = "Send to Manager for Authorization"
                        Exit Sub
                    End If
                    clCD.Fields("claimdetailstatus") = "Approved"
                    clCD.Fields("authby") = hfUserID.Value
                    clCD.Fields("dateauth") = DateTime.Today
                    clCD.Fields("approveby") = hfUserID.Value
                    clCD.Fields("dateapprove") = DateTime.Today
                    clCD.Fields("modby") = hfUserID.Value
                    clCD.Fields("moddate") = DateTime.Today
                    clCD.SaveDB()
                End If
            Next
        End If
        SQL = "select claimdetailid from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimdetailstatus = 'Requested' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            CloseClaim()
            UpdateApproveDate()
        End If
        rgJobs.Rebind()
        Dim clCD2 As New VeritasGlobalTools.clsClaimDetail
        clCD2.ProcessClaim(hfClaimID.Value)
        If clCD2.Refresh Then
            Response.Redirect("~/claim/claim.aspx?sid=" & hfID.Value & "&ClaimID=" & hfClaimID.Value)
        End If

    End Sub

    Private Function GetTaxRate(xClaimDetailType As String) As Double
        Dim SQL As String
        Dim clR As New clsDBO
        GetTaxRate = 0
        If xClaimDetailType = "Deductible" Then
            Exit Function
        End If
        If xClaimDetailType = "Inspect" Then
            Exit Function
        End If
        If xClaimDetailType = "Other" Then
            Exit Function
        End If
        If xClaimDetailType = "" Then
            Exit Function
        End If
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If xClaimDetailType = "Part" Then
                If clR.Fields("parttax").Length > 0 Then
                    GetTaxRate = CDbl(clR.Fields("parttax"))
                End If
            End If
            If xClaimDetailType = "Labor" Then
                If clR.Fields("labortax").Length > 0 Then
                    GetTaxRate = CDbl(clR.Fields("parttax"))
                End If
            End If
        End If

    End Function

    Private Sub btnDenyIt_Click(sender As Object, e As EventArgs) Handles btnDenyIt.Click
        Dim clR As New clsDBO
        Dim clCD As New clsDBO
        Dim SQL As String
        If cboJobNo.SelectedValue = "0" Then
            Exit Sub
        End If
        If hfAllowAZDenied.Value = "False" Then
            If GetServiceCenterState(hfClaimID.Value) = "AZ" Then
                lblAuthorizedError.Text = "You can not deny an AZ claim. Please see manager."
                lblAuthorizedError.Visible = True
                Exit Sub
            End If
        End If
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and jobno = '" & cboJobNo.SelectedValue & "' "
        SQL = SQL + "and claimdetailstatus = 'Requested' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from claimdetail "
                SQL = SQL + "where claimdetailid = " & clR.Fields("claimdetailid")
                clCD.OpenDB(SQL, AppSettings("connstring"))
                If clCD.RowCount > 0 Then
                    clCD.GetRow()
                    clCD.Fields("claimdetailstatus") = "Denied"
                    clCD.Fields("modby") = hfUserID.Value
                    clCD.Fields("moddate") = DateTime.Today
                    clCD.SaveDB()
                End If
            Next
        End If
        SQL = "select claimdetailid from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimdetailstatus = 'Requested' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            CloseClaim()
        End If
        rgJobs.Rebind()
        Dim clCD2 As New VeritasGlobalTools.clsClaimDetail
        clCD2.ProcessClaim(hfClaimID.Value)
        If clCD2.Refresh Then
            Response.Redirect("~/claim/claim.aspx?sid=" & hfID.Value & "&ClaimID=" & hfClaimID.Value)
        End If
    End Sub

    Private Sub ShowSlushNote()
        hfSlushNote.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwSlushNote.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub HideSlushNote()
        hfSlushNote.Value = ""
        Dim script As String = "function f(){$find(""" + rwSlushNote.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub ShowFieldCheck()
        hfFieldCheck.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwFieldCheck.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency2", script, True)
    End Sub

    Private Sub HideFieldCheck()
        hfFieldCheck.Value = ""
        Dim script As String = "function f(){$find(""" + rwFieldCheck.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub


    Private Sub btnUpdateSlush_Click(sender As Object, e As EventArgs) Handles btnUpdateSlush.Click
        If cboSlushJobNo.SelectedValue = "0" Then
            Exit Sub
        End If
        If cboSlushRateType.SelectedValue = 0 Then
            Exit Sub
        End If
        hfSlushNote.Value = "Visible"
        ShowSlushNote()
    End Sub

    Private Sub UpdateSlushClaimDetail()
        Dim clR As New clsDBO
        Dim SQL As String
        If cboSlushJobNo.SelectedValue = "0" Then
            Exit Sub
        End If
        If cboSlushJobNo.SelectedValue = "All" Then
            SQL = "update claimdetail "
            SQL = SQL + "set ratetypeid = " & cboSlushRateType.SelectedValue & ", "
            SQL = SQL + "moddate = '" & Today & "', "
            SQL = SQL + "modby = " & hfUserID.Value & ", "
            If cboSlushRateType.SelectedValue > 1 Then
                SQL = SQL + "slushbyid = " & hfUserID.Value & ", "
                SQL = SQL + "slushdate = '" & Today & "' "
            Else
                SQL = SQL + "slushbyid = null, "
                SQL = SQL + "slushdate = null "
            End If
            SQL = SQL + "where claimid = " & hfClaimID.Value
            clR.RunSQL(SQL, AppSettings("connstring"))
        End If
        SQL = "update claimdetail "
        SQL = SQL + "set ratetypeid = " & cboSlushRateType.SelectedValue & ", "
        SQL = SQL + "moddate = '" & Today & "', "
        SQL = SQL + "modby = " & hfUserID.Value & ", "
        If cboSlushRateType.SelectedValue > 1 Then
            SQL = SQL + "slushbyid = " & hfUserID.Value & ", "
            SQL = SQL + "slushdate = '" & Today & "' "
        Else
            SQL = SQL + "slushbyid = null, "
            SQL = SQL + "slushdate = null "
        End If
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and jobno = '" & cboJobNo.SelectedValue & "' "
        clR.RunSQL(SQL, AppSettings("connstring"))

    End Sub

    Private Sub btnCloseSlushNote_Click(sender As Object, e As EventArgs) Handles btnCloseSlushNote.Click
        hfSlushNote.Value = ""
        HideSlushNote()
    End Sub

    Private Sub btnSaveSlushNote_Click(sender As Object, e As EventArgs) Handles btnSaveSlushNote.Click
        lblAuthorizedError.Text = ""
        If txtSlushNote.Text.Trim.Length < 25 Then
            lblAuthorizedError.Text = "You need to put in a proper Note. Nothing was saved."
            HideSlushNote()
            Exit Sub
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimnote "
        SQL = SQL + "where claimnoteid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimid") = hfClaimID.Value
            clR.Fields("claimnotetypeid") = 5
            clR.Fields("note") = txtSlushNote.Text
            clR.Fields("credate") = Today
            clR.Fields("creby") = hfUserID.Value
            clR.Fields("NoteText") = txtSlushNote.Text
            clR.AddRow()
            clR.SaveDB()
        End If
        UpdateSlushClaimDetail()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        HideFieldCheck()
    End Sub
End Class