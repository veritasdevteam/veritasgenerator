﻿Imports System.Configuration.ConfigurationManager

Public Class ServiceCenters
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsStates.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            If hfUserID.Value = "1" Then
                btnLossCode.Visible = True
            Else
                btnLossCode.Visible = False
            End If
            ReadOnlyButtons()
            lblSCError.Visible = False
            btnRFClaimSubmit.Enabled = False
            CheckRFClaimSubmit()
            checksecurity
        End If
    End Sub

    Private Sub CheckSecurity()
        Dim SQL As String
        Dim clUSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clUSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clUSI.RowCount > 0 Then
            clUSI.GetRow()
            If CBool(clUSI.Fields("AllowClaimAudit")) Then
                btnClaimAudit.Enabled = True
            Else
                btnClaimAudit.Enabled = False
            End If
        End If
    End Sub

    Private Sub CheckRFClaimSubmit()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and teamlead <> 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            SQL = "select * from veritasclaims.dbo.claim "
            SQL = SQL + "where not sendclaim is null "
            SQL = SQL + "and processclaimdate is null "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                btnRFClaimSubmit.Enabled = True
            End If
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                Response.Redirect("~/claim/claimssearch.aspx?sid=" & hfID.Value)
            End If
            If CBool(clR.Fields("teamlead")) Then
                btnClaimTeamOpen.Enabled = True
            Else
                btnClaimTeamOpen.Enabled = False
            End If
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnUnlockClaim.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("unlockclaim") = True Then
                btnUnlockClaim.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub rgServiceCenter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgServiceCenter.SelectedIndexChanged
        hfServiceCenterID.Value = rgServiceCenter.SelectedValue
        pnlSC.Visible = True
        pnlGrid.Visible = False
        ClearData()
        FillData()
    End Sub

    Private Sub ClearData()
        txtServiceCenterNo.Text = ""
        txtServiceCenterName.Text = ""
        txtDealerNo.Text = ""
        txtAddr1.Text = ""
        txtAddr2.Text = ""
        txtCity.Text = ""
        txtZip.Text = ""
        cboState.SelectedValue = ""
        txtPhone.Text = ""
        txtEMail.Text = ""
        txtFax.Text = ""
        txtContact.Text = ""
    End Sub

    Private Sub FillData()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterid = " & hfServiceCenterID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtServiceCenterNo.Text = clR.Fields("servicecenterno")
            txtServiceCenterName.Text = clR.Fields("servicecentername")
            txtDealerNo.Text = clR.Fields("dealerno")
            txtAddr1.Text = clR.Fields("addr1")
            txtAddr2.Text = clR.Fields("addr2")
            txtCity.Text = clR.Fields("city")
            cboState.SelectedValue = clR.Fields("state")
            txtZip.Text = clR.Fields("zip")
            txtPhone.Text = clR.Fields("phone")
            txtContact.Text = clR.Fields("contact")
            txtEMail.Text = clR.Fields("email")
            txtFax.Text = clR.Fields("fax")
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        hfServiceCenterID.Value = 0
        pnlSC.Visible = True
        pnlGrid.Visible = False
        ClearData()
        GetNextServiceCenterNo()
    End Sub

    Private Sub GetNextServiceCenterNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select max(servicecenterno) as mSCN from servicecenter "
        SQL = SQL + "where servicecenterno like 'RF0%' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.GetRow()
        txtServiceCenterNo.Text = "RF" & Format(CLng(Right(clR.Fields("mscn"), 6)) + 1, "0000000")
    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlSC.Visible = False
        pnlGrid.Visible = True
        rgServiceCenter.Rebind()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim clR As New clsDBO
        Dim clCP As New clsDBO
        Dim SQL As String
        lblSCError.Visible = False
        If txtAddr1.Text.Length = 0 Then
            lblSCError.Text = "No Address 1 given."
            lblSCError.Visible = True
            Exit Sub
        End If
        If txtServiceCenterName.Text.Length = 0 Then
            lblSCError.Text = "No Service Center Name given."
            lblSCError.Visible = True
            Exit Sub
        End If
        If txtServiceCenterNo.Text.Length = 0 Then
            lblSCError.Text = "No Service Center Number given."
            lblSCError.Visible = True
            Exit Sub
        End If
        If txtCity.Text.Length = 0 Then
            lblSCError.Text = "No City given."
            lblSCError.Visible = True
            Exit Sub
        End If
        If cboState.Text.Length = 0 Then
            lblSCError.Text = "No State given."
            lblSCError.Visible = True
            Exit Sub
        End If
        If txtZip.Text.Length = 0 Then
            lblSCError.Text = "No Zip Code give."
            lblSCError.Visible = True
            Exit Sub
        End If
        If txtPhone.Text.Length = 0 Then
            lblSCError.Text = "No Phone Number given."
            lblSCError.Visible = True
            Exit Sub
        End If
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterid = " & hfServiceCenterID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
        End If
        clR.Fields("servicecenterno") = txtServiceCenterNo.Text
        clR.Fields("servicecentername") = txtServiceCenterName.Text
        clR.Fields("dealerno") = txtDealerNo.Text
        clR.Fields("addr1") = txtAddr1.Text
        clR.Fields("addr2") = txtAddr2.Text
        clR.Fields("city") = txtCity.Text
        clR.Fields("state") = cboState.SelectedValue
        clR.Fields("zip") = txtZip.Text
        clR.Fields("phone") = txtPhone.Text
        clR.Fields("contact") = txtContact.Text
        clR.Fields("email") = txtEMail.Text
        clR.Fields("fax") = txtFax.Text
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
        pnlSC.Visible = False
        pnlGrid.Visible = True
        rgServiceCenter.Rebind()
    End Sub

    Private Sub btnAutonationACH_Click(sender As Object, e As EventArgs) Handles btnAutonationACH.Click
        Response.Redirect("~/claim/autonationach")
    End Sub

    Private Sub btnClaimSearch_Click(sender As Object, e As EventArgs) Handles btnClaimSearch.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnUnlockClaim_Click(sender As Object, e As EventArgs) Handles btnUnlockClaim.Click
        Response.Redirect("~/claim/unlockclaim.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLossCode_Click(sender As Object, e As EventArgs) Handles btnLossCode.Click
        Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsOpen_Click(sender As Object, e As EventArgs) Handles btnClaimsOpen.Click
        Response.Redirect("~/claim/claimsopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimTeamOpen_Click(sender As Object, e As EventArgs) Handles btnClaimTeamOpen.Click
        Response.Redirect("~/claim/claimteamopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnANAdd_Click(sender As Object, e As EventArgs) Handles btnANAdd.Click
        hfServiceCenterID.Value = 0
        pnlSC.Visible = True
        pnlGrid.Visible = False
        ClearData()
        GetNextANServiceCenterNo()
    End Sub

    Private Sub GetNextANServiceCenterNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select max(servicecenterno) as mSCN from servicecenter "
        SQL = SQL + "where servicecenterno like 'AN0%' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.GetRow()
        If clR.Fields("mscn").Length > 0 Then
            txtServiceCenterNo.Text = "AN" & Format(CLng(Right(clR.Fields("mscn"), 6)) + 1, "0000000")
        Else
            txtServiceCenterNo.Text = "AN0000000"
        End If
    End Sub

    Private Sub btnPrimeAdd_Click(sender As Object, e As EventArgs) Handles btnPrimeAdd.Click
        hfServiceCenterID.Value = 0
        pnlSC.Visible = True
        pnlGrid.Visible = False
        ClearData()
        GetNextPrimeServiceCenterNo()
    End Sub

    Private Sub GetNextPrimeServiceCenterNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select max(servicecenterno) as mSCN from servicecenter "
        SQL = SQL + "where servicecenterno like 'PR0%' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.GetRow()
        If clR.Fields("mscn").Length > 0 Then
            txtServiceCenterNo.Text = "PR" & Format(CLng(Right(clR.Fields("mscn"), 6)) + 1, "0000000")
        Else
            txtServiceCenterNo.Text = "PR0000000"
        End If
    End Sub

    Private Sub btnRFClaimSubmit_Click(sender As Object, e As EventArgs) Handles btnRFClaimSubmit.Click
        Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimAudit_Click(sender As Object, e As EventArgs) Handles btnClaimAudit.Click
        Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketMessage_Click(sender As Object, e As EventArgs) Handles btnTicketMessage.Click
        Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketResponse_Click(sender As Object, e As EventArgs) Handles btnTicketResponse.Click
        Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub
End Class