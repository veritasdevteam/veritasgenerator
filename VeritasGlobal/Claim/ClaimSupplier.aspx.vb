﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class ClaimSupplier
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("claimid")
        dsClaimDetail.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
        End If
        'pnlAfterMarket.Visible = False
        'pnlOEM.Visible = False
        FillPurchase()
        'CalcTotal()
    End Sub

    Private Sub FillPurchase()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdetailinvoice "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            Exit Sub
        Else
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                If CBool(clR.Fields("oem")) Then
                    pnlOEM.Visible = True
                    filloem
                Else
                    pnlAfterMarket.Visible = True
                    fillaftermarket
                End If
            Next
        End If
    End Sub

    Private Sub FillOEM()

    End Sub

    Private Sub FillAfterMarket()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from claimdetailinvoice "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and oem = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("orderid").Length > 0 Then
                rgAMToBePurchase.Columns(9).Visible = False
                btnAMCompletePurchase.Visible = False
            End If
            txtAMOrderID.Text = clR.Fields("orderid")
            txtAMOrderBy.Text = GetUserInfo(clR.Fields("orderby"))
            txtAMOrderDate.Text = Format(CDate(clR.Fields("orderdate")), "M/d/yyyy")
            SQL = "select ClaimDetailPurchaseID, ClaimDetailID,OrderID,Companyname,PartNo,PartDesc,YourCost,TotalOrderCost from claimdetailpurchase cdp "
            SQL = SQL + "inner join claimcompany cc on cdp.claimcompanyid = cc.claimcompanyid "
            SQL = SQL + "where cdp.claimdetailinvoiceid = " & clR.Fields("claimdetailinvoiceid")
            rgAMToBePurchase.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        End If


    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            Unlockbuttons()
        End If
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            If clSI.Fields("readonly") Then
                rgClaimDetailQuote.Enabled = False
            End If
        End If
    End Sub

    Private Sub rgClaimDetail_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimDetail.SelectedIndexChanged
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select cdq.ClaimDetailQuoteID, cdq.ClaimDetailID, cc.CompanyName, cdq.PartNo, cdq.ListPrice, cdq.YourCost, cdq.oem  
                    from ClaimDetailQuote cdq inner join ClaimCompany cc on cc.ClaimCompanyID = cdq.ClaimCompanyID
                    and cdq.claimdetailid = " & rgClaimDetail.SelectedValue
        rgClaimDetailQuote.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgClaimDetailQuote.Rebind()
    End Sub

    Private Sub rgClaimDetailQuote_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgClaimDetailQuote.ItemCommand
        Dim da As GridDataItem
        If e.CommandName = "Purchase" Then
            da = e.Item
            hfClaimDetailQuoteID.Value = da.GetDataKeyValue("ClaimDetailQuoteID")
            AddPurchase()
            FillPurchase()
            CalcAMTotal()

        End If
    End Sub

    Private Sub CalcAMTotal()
        Dim SQL As String
        Dim clR As New clsDBO
        txtAMShippingAmt.Text = "0.00"
        txtAMSubTotal.Text = "0.00"
        SQL = "select sum(yourcost) as amt, sum(shipping) as ship, oem from claimdetailpurchase "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "group by oem"
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                If CBool(clR.Fields("oem")) = False Then
                    If clR.Fields("amt").Length > 0 Then
                        txtAMSubTotal.Text = Format(CDbl(clR.Fields("amt")), "#0.00")
                    End If
                    If clR.Fields("ship").Length > 0 Then
                        txtAMShippingAmt.Text = Format(CDbl(clR.Fields("ship")), "#0.00")
                    End If
                Else
                    If clR.Fields("amt").Length > 0 Then
                        txtOEMSubtotal.Text = Format(CDbl(clR.Fields("amt")), "#0.00")
                    End If
                    If clR.Fields("ship").Length > 0 Then
                        txtOEMShippingAmt.Text = Format(CDbl(clR.Fields("ship")), "#0.00")
                    End If
                End If
            Next
        End If
        SQL = "select * from claimdetailpurchase "
        SQL = SQL + "where claimcompanyid = 1 "
        SQL = SQL + "and claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                If CBool(clR.Fields("oem")) = True Then
                    txtOEMShippingAmt.Text = Format(CDbl(txtOEMShippingAmt.Text) + 42.5, "#0.00")
                    txtOEMTotal.Text = Format(CDbl(txtOEMSubtotal.Text) + CDbl(txtOEMShippingAmt.Text), "#0.00")
                Else
                    txtAMShippingAmt.Text = Format(CDbl(txtAMShippingAmt.Text) + 42.5, "#0.00")
                    txtAMTotal.Text = Format(CDbl(txtAMSubTotal.Text) + CDbl(txtAMShippingAmt.Text), "#0.00")
                End If
            Next
        End If

    End Sub

    Private Sub AddPurchase()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clCDP As New clsDBO
        SQL = "select * from claimdetailquote "
        SQL = SQL + "where claimdetailquoteid = " & hfClaimDetailQuoteID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            SQL = "select * from claimdetailpurchase "
            SQL = SQL + "where claimdetailquoteid = " & clR.Fields("claimdetailquoteid")
            clCDP.OpenDB(SQL, AppSettings("connstring"))
            If clCDP.RowCount = 0 Then
                clCDP.NewRow()
                clCDP.Fields("claimid") = hfClaimID.Value
                clCDP.Fields("claimdetailid") = clR.Fields("claimdetailid")
                clCDP.Fields("claimdetailquoteid") = hfClaimDetailQuoteID.Value
                clCDP.Fields("claimcompanyid") = clR.Fields("claimcompanyid")
                clCDP.Fields("partno") = clR.Fields("partno")
                clCDP.Fields("partdesc") = clR.Fields("partdesc")
                clCDP.Fields("qty") = clR.Fields("qty")
                clCDP.Fields("listprice") = clR.Fields("listprice")
                clCDP.Fields("yourcost") = clR.Fields("yourcost")
                clCDP.Fields("OEM") = clR.Fields("oem")
                clCDP.Fields("partcost") = clR.Fields("partcost")
                clCDP.Fields("shipping") = clR.Fields("shipping")
                clCDP.Fields("orderby") = hfUserID.Value
                clCDP.Fields("orderdate") = Today
                clCDP.AddRow()
                clCDP.SaveDB()
            End If
        End If

    End Sub

    Private Sub btnCompletePurchase_Click(sender As Object, e As EventArgs) Handles btnAMCompletePurchase.Click
        ProcessPurchases()
    End Sub

    Private Sub ProcessPurchases()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdetailpurchase cdp "
        SQL = SQL + "where cdp.claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and orderid is null "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                Dim p As New ProcessStartInfo
                p.FileName = "C:\ProcessProgram\CKPartPurchase\CKPartPurchase.exe"
                p.Arguments = " " & clR.Fields("claimdetailid") & " " & hfUserID.Value
                p.WindowStyle = ProcessWindowStyle.Hidden
                Dim pr As New Process
                pr.StartInfo = p
                pr.Start()
                pr.WaitForExit()
            Next
        End If
        'CalcTotal()
    End Sub

    Private Sub rgAMPurchase_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgAMToBePurchase.ItemCommand
        Dim da As GridDataItem
        Dim SQL As String
        Dim clR As New clsDBO
        If e.CommandName = "Delete" Then
            da = e.Item
            SQL = "delete claimdetailpurchase "
            SQL = SQL + "where claimdetailpurchaseid = " & da.GetDataKeyValue("ClaimDetailPurchaseID")
            clR.RunSQL(SQL, AppSettings("connstring"))
            FillAfterMarket()
        End If
    End Sub

    Private Sub rgOEMPurchase_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgOEMPurchase.ItemCommand
        Dim da As GridDataItem
        Dim SQL As String
        Dim clR As New clsDBO
        If e.CommandName = "Delete" Then
            da = e.Item
            SQL = "delete claimdetailpurchase "
            SQL = SQL + "where claimdetailpurchaseid = " & da.GetDataKeyValue("ClaimDetailPurchaseID")
            clR.RunSQL(SQL, AppSettings("connstring"))
            FillOEM()
        End If
    End Sub

    Private Sub btnOEMCompletePurchase_Click(sender As Object, e As EventArgs) Handles btnOEMCompletePurchase.Click
        ProcessPurchases()
    End Sub
End Class