﻿Imports System.Configuration.ConfigurationManager

Public Class RFClaimSubmit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsClaim.ConnectionString = AppSettings("connstring")
        dsAssignedTo.ConnectionString = AppSettings("connstring")
        dsJobs.ConnectionString = AppSettings("connstring")

        tbTodo.Width = pnlHeader.Width
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            If hfUserID.Value = "1" Then
                btnLossCode.Visible = True
            Else
                btnLossCode.Visible = False
            End If
            btnRFClaimSubmit.Enabled = False
            CheckRFClaimSubmit()
            hfClaimID.Value = 0
            pnlDetail.Visible = False
            pnlAssignedToChange.Visible = False
        End If
    End Sub

    Private Sub CheckRFClaimSubmit()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and teamlead <> 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            SQL = "select * from veritasclaims.dbo.claim "
            SQL = SQL + "where not sendclaim is null "
            SQL = SQL + "and processclaimdate is null "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                btnRFClaimSubmit.Enabled = True
            End If
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnUnlockClaim.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("unlockclaim") = True Then
                btnUnlockClaim.Enabled = True
            End If
            If clSI.Fields("claimpayment") = False Then
                btnInspectionWex.Enabled = False
                btnCarfaxPayment.Enabled = False
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnServiceCenters_Click(sender As Object, e As EventArgs) Handles btnServiceCenters.Click
        Response.Redirect("~/claim/servicecenters.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAutonationACH_Click(sender As Object, e As EventArgs) Handles btnAutonationACH.Click
        Response.Redirect("~/claim/autonationach.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimSearch_Click(sender As Object, e As EventArgs) Handles btnClaimSearch.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnUnlockClaim_Click(sender As Object, e As EventArgs) Handles btnUnlockClaim.Click
        Response.Redirect("~/claim/unlockclaim.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLossCode_Click(sender As Object, e As EventArgs) Handles btnLossCode.Click
        Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsOpen_Click(sender As Object, e As EventArgs) Handles btnClaimsOpen.Click
        Response.Redirect("~/claim/claimsopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimTeamOpen_Click(sender As Object, e As EventArgs) Handles btnClaimTeamOpen.Click
        Response.Redirect("~/claim/claimteamopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRFClaimSubmit_Click(sender As Object, e As EventArgs) Handles btnRFClaimSubmit.Click
        Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub rgClaim_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaim.SelectedIndexChanged
        hfClaimID.Value = rgClaim.SelectedValue
        FillClaimDetail()

    End Sub

    Private Sub FillClaimDetail()
        hfContractID.Value = 0
        hfServiceCenterID.Value = 0
        txtContractNo.Focus()
        pnlList.Visible = False
        pnlDetail.Visible = True
        OpenClaim()
        OpenContract()
        rgJobs.Rebind()
    End Sub

    Private Sub OpenClaim()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from veritasclaims.dbo.claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfContractID.Value = clR.Fields("contractid")
            hfServiceCenterID.Value = clR.Fields("servicecenterid")
        End If
    End Sub

    Private Sub OpenContract()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from contract c "
        SQL = SQL + "left join InsCarrier ic on c.InsCarrierID = ic.InsCarrierID "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtContractNo.Text = clR.Fields("contractno")
            txtStatus.Text = clR.Fields("status")
            txtSaleDate.Text = clR.Fields("saledate")
            txtPaidDate.Text = clR.Fields("datepaid")
            txtFName.Text = clR.Fields("fname")
            txtLName.Text = clR.Fields("lname")
            txtInspCarrier.Text = clR.Fields("inscarriername")
            hfPlanTypeID.Value = clR.Fields("plantypeid")
            hfProgram.Value = clR.Fields("programid")
            GetProgram()
            GetPlanType()
            txtTerm.Text = clR.Fields("termmonth") & "/" & Format(CLng(clR.Fields("termmile")), "#,##0")
            If clR.Fields("effdate").Length > 0 Then
                txtEffDate.Text = Format(CDate(clR.Fields("effdate")), "M/d/yyyy")
            End If

            If clR.Fields("expdate").Length > 0 Then
                txtExpDate.Text = Format(CDate(clR.Fields("expdate")), "M/d/yyyy")
            End If
            txtEffMile.Text = Format(CLng(clR.Fields("effmile")), "#,##0")
            If clR.Fields("expmile").Length > 0 Then
                txtExpMile.Text = Format(CLng(clR.Fields("expmile")), "#,##0")
            End If
            If clR.Fields("decpage").Length = 0 Then

                tcDec.Visible = False
                tcDesc2.Visible = False
            Else
                tcDec.Visible = True
                tcDesc2.Visible = True
                hlDec.NavigateUrl = clR.Fields("decpage")
                hlDec.Text = clR.Fields("decpage")
            End If
            If clR.Fields("tcpage").Length = 0 Then
                tcTC.Visible = False
                tcTC2.Visible = False
            Else
                tcTC.Visible = True
                tcTC2.Visible = True
                hlTC.NavigateUrl = clR.Fields("tcpage")
                hlTC.Text = clR.Fields("tcpage")
            End If

        End If
    End Sub

    Private Sub GetPlanType()
        Dim SQL As String
        Dim clPT As New clsDBO
        SQL = "select * from plantype "
        SQL = SQL + "where plantypeid = " & hfPlanTypeID.Value
        clPT.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clPT.RowCount > 0 Then
            clPT.GetRow()
            txtPlan.Text = clPT.Fields("plantype")
        End If
    End Sub

    Private Sub GetProgram()
        Dim SQL As String
        Dim clP As New clsDBO
        SQL = "select * from program "
        SQL = SQL + "where programid = " & hfProgram.Value
        clP.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clP.RowCount > 0 Then
            clP.GetRow()
            txtProgram.Text = clP.Fields("programname")
        End If
    End Sub

    Private Sub btnSendClaim_Click(sender As Object, e As EventArgs) Handles btnSendClaim.Click
        Dim clIC As New VeritasGlobalTools.clsImportClaim
        If hfAssignedTo.Value = "" Then
            hfAssignedTo.Value = hfUserID.Value
        End If
        If hfAssignedTo.Value = "0" Then
            hfAssignedTo.Value = hfUserID.Value
        End If
        clIC.VeritasClaimID = hfClaimID.Value
        clIC.AssignedToID = hfAssignedTo.Value
        clIC.UserID = hfUserID.Value

        clIC.ProcessClaim()

        pnlDetail.Visible = False
        pnlList.Visible = True
        rgClaim.Rebind()
    End Sub

    Private Sub btnChangeAssignedTo_Click(sender As Object, e As EventArgs) Handles btnChangeAssignedTo.Click
        pnlAssignedToChange.Visible = True
        pnlDetail.Visible = False
    End Sub

    Private Sub rgAssignedTo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAssignedTo.SelectedIndexChanged
        hfAssignedTo.Value = 0
        hfAssignedTo.Value = rgAssignedTo.SelectedValue
        GetAssignedTo()
    End Sub

    Private Sub GetAssignedTo()
        txtAssignedTo.Text = ""
        Dim SQL As String
        Dim clR As New clsDBO
        If hfAssignedTo.Value = "" Then
            hfAssignedTo.Value = hfUserID.Value
        End If
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & hfAssignedTo.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtAssignedTo.Text = clR.Fields("fname") & " " & clR.Fields("lname")
        End If
        pnlDetail.Visible = True
        pnlAssignedToChange.Visible = False
    End Sub

    Private Sub btnTicketMessage_Click(sender As Object, e As EventArgs) Handles btnTicketMessage.Click
        Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketResponse_Click(sender As Object, e As EventArgs) Handles btnTicketResponse.Click
        Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub rgClaim_DataBound(sender As Object, e As EventArgs) Handles rgClaim.DataBound
        For cnt = 0 To rgClaim.Items.Count - 1
            If rgClaim.Items(cnt).Item("ServiceCenterName").Text.ToLower.Contains("- an") Then
                rgClaim.Items(cnt).Item("ServiceCenterName").BackColor = Drawing.Color.LightBlue
            End If
        Next

    End Sub
End Class