﻿Imports System.Configuration.ConfigurationManager

Public Class ClaimVINMoxy1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfContractID.Value = Request.QueryString("contractid")
        If Not IsPostBack Then
            FillVehicleInfo()
        End If
    End Sub

    Private Sub FillVehicleInfo()
        Dim SQL As String
        Dim clC As New clsDBO
        Dim clCS As New clsDBO
        SQL = "select c.contractid, vin, class, year,make,model,trim,awd,turbo,diesel,hybrid,commercial,hydraulic,airbladder,liftkit,luxury,seals,snowplow,largerliftkit,rideshare, salvagetitle from contract c "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            lblVIN.Text = clC.Fields("vin")
            lblClass.Text = clC.Fields("class")
            lblYear.Text = clC.Fields("year")
            lblMake.Text = clC.Fields("make")
            lblModel.Text = clC.Fields("model")
            lblTrim.Text = clC.Fields("Trim")
            chkSalvageTitle.Checked = clC.Fields("SalvageTitle")
            SQL = "select * from contractsurcharge "
            SQL = SQL + "where contractid = " & clC.Fields("contractid")
            clCS.OpenDB(SQL, AppSettings("connstring"))
            If clCS.RowCount > 0 Then
                clCS.GetRow()
                chkAWD.Checked = clCS.Fields("AWD")
                chkTurbo.Checked = clCS.Fields("Turbo")
                chkDiesel.Checked = clCS.Fields("Diesel")
                chkHybrid.Checked = clCS.Fields("Hybrid")
                chkCommercial.Checked = clCS.Fields("Commercial")
                chkHydraulic.Checked = clCS.Fields("Hydraulic")
                chkAirBladder.Checked = clCS.Fields("AirBladder")
                chkLiftKit.Checked = clCS.Fields("LiftKit")
                chkLuxury.Checked = clCS.Fields("Luxury")
                chkSeals.Checked = clCS.Fields("Seals")
                chkSnowPlow.Checked = clCS.Fields("SnowPlow")
                chkLargerLiftKit.Checked = clCS.Fields("LargerLiftKit")
                chkRideShare.Checked = clCS.Fields("rideshare")
                chkLuxuryNC.Checked = clCS.Fields("LuxuryNC")
                chkChrome.Checked = clCS.Fields("chrome")
                chkCosmetic.Checked = clCS.Fields("cosmetic")
            End If
        End If
    End Sub
End Class