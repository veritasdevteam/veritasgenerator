﻿Imports System.Web.Hosting
Imports Telerik.Windows.Documents.Spreadsheet.Expressions.Functions
Imports System.Configuration.ConfigurationManager

Public Class claimsearch
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        SqlDataSource1.ConnectionString = AppSettings("connstring")
        dsStates.ConnectionString = AppSettings("connstring")
        dsStatus.ConnectionString = AppSettings("connstring")
        dsUserInfo.ConnectionString = AppSettings("connstring")

        If Not IsPostBack Then
            hfWebClaim.Value = ""
            GetServerInfo()
            CheckClaimPopup()
            trUserInfo.Visible = False
            trUserInfoClose.Visible = False
            CheckToDo()
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            trClaimResult.Visible = False
            trServiceCenter.Visible = False
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            If hfUserID.Value = "1" Then
                btnLossCode.Visible = True
            Else
                btnLossCode.Visible = False
            End If
            ReadOnlyButtons()
            btnRFClaimSubmit.Enabled = False
            CheckRFClaimSubmit()
            PostMessage()
            CheckWebClaim()
        End If
    End Sub

    Private Sub CheckWebClaim()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select claimid, claimno from claim "
        SQL = SQL + "where webclaim <> 0 "
        SQL = SQL + "and webview = 0 "
        SQL = SQL + "and assignedto = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            lblWebClaim.Text = "You have a Web Claim assigned to you. Claim No: " & clR.Fields("claimno")
            hfWebClaimID.Value = clR.Fields("claimid")
            ShowWebClaim()
        End If
    End Sub

    Private Sub PostMessage()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clUI As New clsDBO
        lblMessage.Text = ""
        SQL = "select * from userinfo ui "
        SQL = SQL + "inner join UserSecurityInfo usi on ui.UserID = usi.UserID "
        SQL = SQL + "where ui.userid = " & hfUserID.Value & " "
        SQL = SQL + "and teamid = 2 "
        clUI.OpenDB(SQL, AppSettings("connstring"))
        If clUI.RowCount > 0 Then
            clUI.GetRow()
            If clUI.Fields("teamid") = 2 Then
                SQL = "select * from veritasclaimticket.dbo.ticket "
                SQL = SQL + "where statusid = 3 "
                clR.OpenDB(SQL, AppSettings("connstring"))
                If clR.RowCount > 0 Then
                    lblMessage.Text = "You have received a message for a Ticket."
                End If
                SQL = "select * from veritasclaimticket.dbo.ticket "
                SQL = SQL + "where statusid = 2 "
                clR.OpenDB(SQL, AppSettings("connstring"))
                If clR.RowCount > 0 Then
                    lblMessage.Text = "You have received a response from ticket."
                End If
            End If
        End If
    End Sub

    Private Sub CheckRFClaimSubmit()
        Dim SQL As String
        Dim clR As New clsDBO
        lblMessage.Text = ""
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and teamlead <> 0 "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            SQL = "select * from veritasclaims.dbo.claim "
            SQL = SQL + "where not sendclaim is null "
            SQL = SQL + "and processclaimdate is null "
            clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clR.RowCount > 0 Then
                btnRFClaimSubmit.Enabled = True
                lblMessage.Text = "There is a claim submitted by a Repairshop Facility."
            End If
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnServiceCenters.Enabled = False
                btnClaimsOpen.Enabled = False
                btnAutonationACH.Enabled = False
            End If
            If CBool(clR.Fields("AllowClaimAudit")) Then
                btnClaimAudit.Enabled = True
            Else
                btnClaimAudit.Enabled = False
            End If
            If CBool(clR.Fields("teamlead")) Then
                btnClaimTeamOpen.Enabled = True
            Else
                btnClaimTeamOpen.Enabled = False
            End If
            hfVeroOnly.Value = CBool(clR.Fields("veroonly"))
            hfAgentID.Value = clR.Fields("agentid")
            hfSubAgentID.Value = clR.Fields("subagentid")
            If CheckANOnly() Then
                chkAutoNation.Visible = False
            End If
            If hfVeroOnly.Value = "True" Then
                chkAutoNation.Visible = False
            End If
            If hfAgentID.Value.Length > 0 Then
                If hfAgentID.Value <> "0" Then
                    chkAutoNation.Visible = False
                End If
            End If
            If hfSubAgentID.Value.Length > 0 Then
                If hfSubAgentID.Value <> "0" Then
                    chkAutoNation.Visible = False
                End If
            End If

        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Date.Today
        sEndDate = DateAdd("d", 1, Date.Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnUnlockClaim.Enabled = False
        btnTicketMessage.Enabled = False
        btnTicketResponse.Enabled = False

    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfInsCarrierID.Value = clSI.Fields("inscarrierid")
            btnUsers.Enabled = True
            If CBool(clSI.Fields("accounting")) = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("unlockclaim") = True Then
                btnUnlockClaim.Enabled = True
            End If
            If clSI.Fields("claimpayment") = False Then
                btnInspectionWex.Enabled = False
                btnCarfaxPayment.Enabled = False
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub ShowWebClaim()
        hfWebClaim.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwWebClaim.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwWebClaim", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnServiceCenters_Click(sender As Object, e As EventArgs) Handles btnServiceCenters.Click
        Response.Redirect("~/claim/servicecenters.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAutonationACH_Click(sender As Object, e As EventArgs) Handles btnAutonationACH.Click
        Response.Redirect("~/claim/autonationach.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimSearch_Click(sender As Object, e As EventArgs) Handles btnClaimSearch.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select claimid, c.contractid, contractno, claimno, cl.status, servicecenterno, servicecentername, sc.city, sc.state, vin, make, model, lossdate  from claim cl "
        SQL = SQL + "left join contract c on cl.contractid = c.contractid "
        SQL = SQL + "left join servicecenter sc on cl.servicecenterid = sc.servicecenterid "
        SQL = SQL + "left join dealer d on c.dealerid = d.dealerid "
        SQL = SQL + "left join agents a on a.agentid = d.agentsid "
        SQL = SQL + "where claimid > 0 "
        If txtContractNo.Text.Trim.Length > 0 Then
            SQL = SQL + "and contractno like '%" & txtContractNo.Text.Trim & "%' "
        End If
        If txtRONumber.Text.Trim.Length > 0 Then
            SQL = SQL + "and ronumber like '%" & txtRONumber.Text.Trim & "%' "
        End If
        If txtClaimNo.Text.Trim.Length > 0 Then
            SQL = SQL + "and claimno like '%" & txtClaimNo.Text.Trim & "%' "
        End If
        If txtFName.Text.Trim.Length > 0 Then
            SQL = SQL + "and fname like '%" & txtFName.Text.Trim & "%' "
        End If
        If txtLName.Text.Trim.Length > 0 Then
            SQL = SQL + "and lname like '%" & txtLName.Text.Trim & "%' "
        End If
        If txtServiceCenterNo.Text.Trim.Length > 0 Then
            SQL = SQL + "and cl.servicecenterid = " & hfServiceCenterID.Value
        End If
        If txtCity.Text.Trim.Length > 0 Then
            SQL = SQL + "and sc.city like '%" & txtCity.Text.Trim & "%' "
        End If
        If cboState.SelectedValue.Length > 0 Then
            SQL = SQL + "and sc.state = '" & cboState.Text & "%' "
        End If
        If txtVIN.Text.Trim.Length > 0 Then
            SQL = SQL + "and vin like '%" & txtVIN.Text.Trim & "%' "
        End If
        If cboStatus.Text.Length > 0 Then
            SQL = SQL + "and cl.status = '" & cboStatus.SelectedValue & "' "
        End If
        If hfAssignedTo.Value.Length > 0 Then
            SQL = SQL + "and assignedto = " & hfAssignedTo.Value & " "
        End If
        If chkAutoNation.Checked Then
            SQL = SQL + "and sc.dealerno like '2%' "
        End If
        If hfInsCarrierID.Value > 0 Then
            SQL = SQL + "and c.inscarrierid = " & hfInsCarrierID.Value & " "
        End If
        If CheckANOnly() Then
            SQL = SQL + "and (sc.dealerno like '2%' "
            SQL = SQL + "or d.dealerno like '2%') "
        End If
        If hfVeroOnly.Value = "True" Then
            SQL = SQL + "and a.agentid = 54 "
        End If
        If hfAgentID.Value.Length > 0 Then
            If hfAgentID.Value <> "0" Then
                SQL = SQL + "and d.agentsid = " & hfAgentID.Value & " "
            End If
        End If
        If hfSubAgentID.Value.Length > 0 Then
            If hfSubAgentID.Value <> "0" Then
                SQL = SQL + "and d.subagentid = " & hfSubAgentID.Value & " "
            End If
        End If
        SQL = SQL + "Union "
        SQL = SQL + "select claimgapid, c.contractid, contractno, claimno, s.ClaimGAPStatusdesc as status, '' as servicecenterno,  '' as servicecentername, c.city, c.state, vin, make, model, cl.ClaimDate as lossdate "
        SQL = SQL + "from claimgap cl "
        SQL = SQL + "left join contract c on cl.contractid = c.contractid "
        SQL = SQL + "left join dealer d on c.dealerid = d.dealerid "
        SQL = SQL + "left join agents a on a.agentid = d.agentsid "
        SQL = SQL + "left join ClaimGapStatus s on cl.Status = s.ClaimGAPStatusID "
        SQL = SQL + "where cl.claimgapid > 0 "
        If txtContractNo.Text.Trim.Length > 0 Then
            SQL = SQL + "and contractno like '%" & txtContractNo.Text.Trim & "%' "
        End If
        If txtClaimNo.Text.Trim.Length > 0 Then
            SQL = SQL + "and claimno like '%" & txtClaimNo.Text.Trim & "%' "
        End If
        If txtFName.Text.Trim.Length > 0 Then
            SQL = SQL + "and c.fname like '%" & txtFName.Text.Trim & "%' "
        End If
        If txtLName.Text.Trim.Length > 0 Then
            SQL = SQL + "and c.lname like '%" & txtLName.Text.Trim & "%' "
        End If
        If txtCity.Text.Trim.Length > 0 Then
            SQL = SQL + "and c.city like '%" & txtCity.Text.Trim & "%' "
        End If
        If cboState.SelectedValue.Length > 0 Then
            SQL = SQL + "and c.state = '" & cboState.Text & "%' "
        End If
        If txtVIN.Text.Trim.Length > 0 Then
            SQL = SQL + "and vin like '%" & txtVIN.Text.Trim & "%' "
        End If
        If cboStatus.Text.Length > 0 Then
            SQL = SQL + "and s.claimgapstatusdesc = '" & cboStatus.SelectedValue & "' "
        End If
        If hfAssignedTo.Value.Length > 0 Then
            SQL = SQL + "and assignedto = " & hfAssignedTo.Value & " "
        End If
        If chkAutoNation.Checked Then
            SQL = SQL + "and c.dealerno like '2%' "
        End If
        If hfInsCarrierID.Value > 0 Then
            SQL = SQL + "and c.inscarrierid = " & hfInsCarrierID.Value & " "
        End If
        If CheckANOnly() Then
            SQL = SQL + "or d.dealerno like '2%' "
        End If
        If hfVeroOnly.Value = "True" Then
            SQL = SQL + "and a.agentid = 54 "
        End If
        If hfAgentID.Value.Length > 0 Then
            If hfAgentID.Value <> "0" Then
                SQL = SQL + "and d.agentsid = " & hfAgentID.Value & " "
            End If
        End If
        If hfSubAgentID.Value.Length > 0 Then
            If hfSubAgentID.Value <> "0" Then
                SQL = SQL + "and d.subagentid = " & hfSubAgentID.Value & " "
            End If
        End If
        rgClaim.DataSource = clC.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring")).Tables(0)
        rgClaim.Rebind()
        trClaimResult.Visible = True
    End Sub

    Private Function CheckANOnly() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckANOnly = False
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and autonationonly <> 0 "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckANOnly = True
        End If
    End Function


    Private Sub btnSCSeek_Click(sender As Object, e As EventArgs) Handles btnSCSeek.Click
        trServiceCenter.Visible = True
        rgServiceCenter.DataSourceID = "SQLDataSource1"
        rgServiceCenter.Rebind()
    End Sub

    Private Sub rgServiceCenter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgServiceCenter.SelectedIndexChanged
        txtServiceCenterNo.Text = rgServiceCenter.SelectedValue
        getservicecenterid
        trServiceCenter.Visible = False
    End Sub

    Private Sub GetServiceCenterID()
        hfServiceCenterID.Value = 0
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterno = '" & txtServiceCenterNo.Text & "' "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfServiceCenterID.Value = clR.Fields("servicecenterid")
        End If
    End Sub

    Private Sub rgClaim_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaim.SelectedIndexChanged

        Dim SQL As String
        Dim clR As New clsDBO
        Try
            SQL = "select PlanTypeID, ProgramID from contract c "
            SQL = SQL + "where contractid = " & rgClaim.SelectedValues("ContractID")
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                clR.GetRow()
                If clR.Fields("programid") <> 103 Then
                    Response.Redirect("~/claim/claim.aspx?sid=" & hfID.Value & "&ClaimID=" & rgClaim.SelectedValues("ClaimID"))
                Else
                    Response.Redirect("~/claim/claimgap.aspx?sid=" & hfID.Value & "&ClaimID=" & rgClaim.SelectedValues("ClaimID"))
                End If
            End If
        Catch ex As Exception
            'Response.Redirect("~/claim/claim.aspx?sid=" & hfID.Value & "&ClaimID=" & rgClaim.SelectedValues("ClaimID"))
        End Try
    End Sub

    Private Sub btnUnlockClaim_Click(sender As Object, e As EventArgs) Handles btnUnlockClaim.Click
        Response.Redirect("~/claim/unlockclaim.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnInspectionWex_Click(sender As Object, e As EventArgs) Handles btnInspectionWex.Click
        Response.Redirect("~/claim/ClaimWexInspection.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLossCode_Click(sender As Object, e As EventArgs) Handles btnLossCode.Click
        Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSeekUser_Click(sender As Object, e As EventArgs) Handles btnSeekUser.Click
        trClaimResult.Visible = False
        trUserInfo.Visible = True
        trUserInfoClose.Visible = True
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        trClaimResult.Visible = True
        trUserInfo.Visible = False
        trUserInfoClose.Visible = False
        hfAssignedTo.Value = ""
        txtAssignedTo.Text = ""
    End Sub

    Private Sub rgUserInfo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgUserInfo.SelectedIndexChanged
        trClaimResult.Visible = True
        trUserInfo.Visible = False
        trUserInfoClose.Visible = False
        hfAssignedTo.Value = rgUserInfo.SelectedValue
        GetUserInfo()
    End Sub

    Private Sub GetUserInfo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & hfAssignedTo.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtAssignedTo.Text = clR.Fields("fname") & " " & clR.Fields("lname")
        End If
    End Sub

    Private Sub btnClaimsOpen_Click(sender As Object, e As EventArgs) Handles btnClaimsOpen.Click
        Response.Redirect("~/claim/claimsopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimTeamOpen_Click(sender As Object, e As EventArgs) Handles btnClaimTeamOpen.Click
        Response.Redirect("~/claim/claimteamopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRFClaimSubmit_Click(sender As Object, e As EventArgs) Handles btnRFClaimSubmit.Click
        Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimAudit_Click(sender As Object, e As EventArgs) Handles btnClaimAudit.Click
        Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketMessage_Click(sender As Object, e As EventArgs) Handles btnTicketMessage.Click
        Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketResponse_Click(sender As Object, e As EventArgs) Handles btnTicketResponse.Click
        Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnWebClaimOK_Click(sender As Object, e As EventArgs) Handles btnWebClaimOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwWebClaim.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
        addwebnotify
    End Sub

    Private Sub AddWebNotify()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "update claim "
        SQL = SQL + "set webnotify = webnotify + 1 "
        SQL = SQL + "where claimid = " & hfWebClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub CheckClaimPopup()
        Dim clCA As New VeritasGlobalTools.clsClaimAssign
        If clCA.CheckUserInfo(hfUserID.Value) Then
            hfClaimID.Value = clCA.CheckClaimUnassigned
            If hfClaimID.Value > "0" Then
                hfClaimNo.Value = clCA.GetClaimNo(hfClaimID.Value)
                txtClaimNoPop.Text = hfClaimNo.Value & " is waiting to be accepted for processing."
                If clCA.IsContractAN(hfClaimID.Value) Then
                    If clCA.CheckANUser(hfUserID.Value) Then
                        ShowClaimAssign()
                    End If
                Else
                    If clCA.CheckANUser(hfUserID.Value) Then
                        ShowClaimAssign()
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub ShowClaimAssign()
        rwClaimAssign.Visible = True
        hfClaimAssign.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwClaimAssign.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwClaimAssign", script, True)
    End Sub

    Private Sub HideClaimAssign()
        hfClaimAssign.Value = ""
        Dim script As String = "function f(){$find(""" + rwClaimAssign.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
        rwClaimAssign.Visible = False
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        Dim clCA As New VeritasGlobalTools.clsClaimAssign
        If clCA.CheckClaimUnassigned2(hfClaimID.Value) > 0 Then
            clCA.AddAccepted(hfClaimID.Value, hfUserID.Value)
            clCA.UpdateClaimAssign(hfClaimID.Value, hfUserID.Value)
        End If
        HideClaimAssign()
    End Sub

    Private Sub btnDeny_Click(sender As Object, e As EventArgs) Handles btnDeny.Click
        Dim clCA As New VeritasGlobalTools.clsClaimAssign
        clCA.AddDeny(hfClaimID.Value, hfUserID.Value)
        HideClaimAssign()
    End Sub
End Class