﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ClaimGAPDocumentStatus

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''chkRISC control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkRISC As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkPaymentHistory control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkPaymentHistory As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkTotalLossInsValuation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkTotalLossInsValuation As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkTotalLossInsSettlementLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkTotalLossInsSettlementLetter As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkPoliceFireReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkPoliceFireReport As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkProofInsPayments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkProofInsPayments As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkGAPClaimNotice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkGAPClaimNotice As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''btnGenerateClaimNotice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGenerateClaimNotice As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblGenerateClaimNotice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblGenerateClaimNotice As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chk60DayLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chk60DayLetter As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''btnGenerate60DayLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGenerate60DayLetter As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblGenerate60DayLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblGenerate60DayLetter As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkDenialLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkDenialLetter As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''btnDenialLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDenialLetter As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblDenialLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDenialLetter As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkMissedOptions control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkMissedOptions As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''btnMissedOptions control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnMissedOptions As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblMissedOptions control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMissedOptions As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkGAPNoGAPDueLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkGAPNoGAPDueLetter As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''btnGAPNoGAPDueLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGAPNoGAPDueLetter As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblGapNoGapDueLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblGapNoGapDueLetter As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkPaymentLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkPaymentLetter As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''btnPaymentLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPaymentLetter As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblPaymentLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPaymentLetter As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkStatusLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkStatusLetter As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''btnStatusLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnStatusLetter As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblStatusLetter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStatusLetter As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnUpdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpdate As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hfClaimID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfClaimID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfUserID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfUserID As Global.System.Web.UI.WebControls.HiddenField
End Class
