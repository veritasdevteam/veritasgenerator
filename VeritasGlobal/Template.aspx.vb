﻿Public Class Template
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetServerInfo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring") = "server=142.11.233.98;database=veritas;User Id=sa;Password=NCC1701E" Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            End If
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnUserSettings.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("usersettings") = True Then
                btnUserSettings.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub
End Class