﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class AgentDashboard1
    Inherits System.Web.UI.UserControl

    Public sID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    Public Sub Update()
        hfID.Value = sID
        GetServerInfo()
        SetDashboard()
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = DateTime.Today
        sEndDate = DateAdd("d", 1, DateTime.Today)
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub SetDashboard()
        Dim SQL As String
        Dim clR As New clsDBO
        Try
            trBlank.Visible = True
            trDashboard.Visible = False
            trOpen.Visible = False
            SQL = "select * from usersecurityinfo "
            SQL = SQL + "where userid = " & hfUserID.Value
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 0 Then
                clR.GetRow()
                trBlank.Visible = False
                trDashboard.Visible = True
                trOpen.Visible = True
                SQL = "select UserID, CallDate,'Inbound Calls' as title ,InboundCt as count, 
                (case when (select count(goal) from UserGoals where GoalsID = 1 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 1 and UserID = ia.UserID) end )as goal 
                from VeritasPhone.dbo.AgentDailyCallSummary ia where UserID = " & hfUserID.Value & " and CallDate <= '" & DateAdd(DateInterval.Day, -1, Today) & "'
                and CallDate > '" & DateAdd(DateInterval.Day, -8, Today) & "' and ((DATEPART(dw, CallDate) + @@DATEFIRST) % 7) NOT IN (0) "
                htmlInbound.DataSource = clR.GetData(SQL, AppSettings("connstring"))
                htmlInbound.ChartTitle.Text = "Inbound Stats for " & DateAdd(DateInterval.Day, -1, Today)
                SQL = "select UserID, CallDate,'Outbound Calls' as title ,OutboundCt as count, 
                (case when (select count(goal) from UserGoals where GoalsID = 2 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 2 and UserID = ia.UserID) end )as goal 
                from VeritasPhone.dbo.AgentDailyCallSummary ia where UserID = " & hfUserID.Value & " and CallDate <= '" & DateAdd(DateInterval.Day, -1, Today) & "'
                and CallDate > '" & DateAdd(DateInterval.Day, -8, Today) & "' and ((DATEPART(dw, CallDate) + @@DATEFIRST) % 7) NOT IN (0) "
                htmlOutBound.DataSource = clR.GetData(SQL, AppSettings("connstring"))
                htmlOutBound.ChartTitle.Text = "Outbound for " & DateAdd(DateInterval.Day, -1, Today)
                SQL = "select userID, date as CallDate, 'DnD Total' as title, dndInstanceCount as count,
                (case when (select count(goal) from UserGoals where GoalsID = 3 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 3 and UserID = ia.UserID) end )as goal
                from VeritasPhone.dbo.AgentDnD ia where UserID = " & hfUserID.Value & " and Date <= '" & DateAdd(DateInterval.Day, -1, Today) & "'
                and Date > '" & DateAdd(DateInterval.Day, -8, Today) & "' and ((DATEPART(dw, Date) + @@DATEFIRST) % 7) NOT IN (0) "
                htmlDnD.DataSource = clR.GetData(SQL, AppSettings("connstring"))
                htmlDnD.ChartTitle.Text = "DnD for " & DateAdd(DateInterval.Day, -1, Today)
                SQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc from Claim 
                cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID 
                where Status = 'open' and AssignedTo = " & hfUserID.Value & " order by ClaimAge desc"
                rgClaimOpen.DataSource = clR.GetData(SQL, AppSettings("connstring"))
                rgClaimOpen.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub rgClaimOpen_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimOpen.SelectedIndexChanged
        Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + Convert.ToString(rgClaimOpen.SelectedValue))
    End Sub

    Private Sub rgClaimOpen_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles rgClaimOpen.NeedDataSource
        SetDashboard()
    End Sub
End Class