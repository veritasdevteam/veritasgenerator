﻿Imports Telerik.Web.UI

Public Class DealerCommission
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsGuard.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsHighline.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsExotic.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsEssentials.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsWrap.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsProgram.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsRateType.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsRateCategory.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsAgent.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsSubAgent.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        If Not IsPostBack Then
            chkNoChargeBack.Visible = False
            trNCB.Visible = False
            hfUserID.Value = hfUserID.Value
            hfDealerID.Value = Request.QueryString("dealerid")
            pnlCommission.Visible = True
            pnlCommissionDetail.Visible = False
            pnlSearchAgent.Visible = False
            pnlSearchRateType.Visible = False
            pnlSearchSubAgent.Visible = False
            pnlAddRateType.Visible = False
            pnlConfirmDelete.Visible = False
        End If
    End Sub

    Private Sub btnAddCommission_Click(sender As Object, e As EventArgs) Handles btnAddCommission.Click
        pnlCommission.Visible = False
        pnlCommissionDetail.Visible = True
        ClearCommissionDetail()
    End Sub

    Private Sub ClearCommissionDetail()
        txtCommDesc.Text = ""
        txtRateType.Text = ""
        cboProgram.ClearSelection()
        rdpStartDate.Clear()
        txtAgent.Text = ""
        txtAmount.Text = ""
        cboClass.Items.Clear()
        rdpEndDate.Clear()
        hfCommissionHeaderID.Value = "0"
        hfAgentID.Value = "0"
        hfSubAgentID.Value = "0"
    End Sub

    Private Sub rgDealerCommission_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgDealerCommission.SelectedIndexChanged
        Dim SQL As String
        Dim clDC As New clsDBO
        SQL = "select * from commissionheader "
        SQL = SQL + "where commissionheaderid = " & rgDealerCommission.SelectedValue
        clDC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clDC.RowCount > 0 Then
            clDC.GetRow()
            hfCommissionHeaderID.Value = clDC.Fields("commissionheaderid")
            txtCommDesc.Text = clDC.Fields("commissiondesc")
            hfRateTypeID.Value = clDC.Fields("ratetypeid")
            txtRateType.Text = GetRateType(clDC.Fields("ratetypeid"))
            cboProgram.SelectedValue = clDC.Fields("programid")
            CalcClass()
            FillClass(clDC.Fields("class"))
            rdpStartDate.SelectedDate = clDC.Fields("startdate")
            txtAgent.Text = GetAgentInfo(clDC.Fields("agentid"))
            txtAmount.Text = clDC.Fields("amt")
            hfAgentID.Value = clDC.Fields("agentid")
            hfSubAgentID.Value = clDC.Fields("subagentid")
            cboClass.Text = clDC.Fields("class")
            If IsDate(clDC.Fields("enddate")) Then
                rdpEndDate.SelectedDate = clDC.Fields("enddate")
            End If
            lblCreBy.Text = GetUserInfo(clDC.Fields("creby"))
            lblCreDate.Text = clDC.Fields("credate")
            lblModBy.Text = GetUserInfo(clDC.Fields("modby"))
            lblModDate.Text = clDC.Fields("moddate")
            chkNoChargeBack.Checked = clDC.Fields("ncb")
            txtNCB.Text = clDC.Fields("NCBAmt")
            pnlCommission.Visible = False
            pnlCommissionDetail.Visible = True
        End If
    End Sub

    Private Sub btnCancelCommission_Click(sender As Object, e As EventArgs) Handles btnCancelCommission.Click
        pnlCommissionDetail.Visible = False
        pnlCommission.Visible = True
    End Sub

    Private Sub btnSaveCommission_Click(sender As Object, e As EventArgs) Handles btnSaveCommission.Click
        Dim bAdd As Boolean
        Dim SQL As String
        Dim clCH As New clsDBO
        Dim bChange As Boolean
        SQL = "select * from commissionheader "
        SQL = SQL + "where commissionheaderid = " & hfCommissionHeaderID.Value
        clCH.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCH.RowCount = 0 Then
            clCH.NewRow()
        Else
            clCH.GetRow()
        End If
        clCH.Fields("commissiondesc") = txtCommDesc.Text
        If clCH.Fields("ratetypeid") <> hfRateTypeID.Value Then
            bChange = True
        End If
        clCH.Fields("ratetypeid") = hfRateTypeID.Value
        If clCH.Fields("programid") <> cboProgram.SelectedValue Then
            bChange = True
        End If
        If clCH.Fields("programid") <> cboProgram.SelectedValue Then
            bChange = True
        End If
        clCH.Fields("programid") = cboProgram.SelectedValue
        clCH.Fields("dealerid") = hfDealerID.Value
        If clCH.Fields("startdate").Length > 0 Then
            If clCH.Fields("startdate") <> rdpStartDate.SelectedDate Then
                bChange = True
            End If
        End If
        clCH.Fields("startdate") = rdpStartDate.SelectedDate
        If clCH.Fields("agentid") <> hfAgentID.Value Then
            bChange = True
        End If
        clCH.Fields("agentid") = hfAgentID.Value
        If clCH.Fields("class") <> cboClass.Text Then
            bChange = True
        End If
        clCH.Fields("class") = cboClass.Text
        If clCH.Fields("amt") <> txtAmount.Text Then
            bChange = True
        End If
        clCH.Fields("amt") = txtAmount.Text
        clCH.Fields("ncb") = chkNoChargeBack.Checked
        If txtNCB.Text.Length > 0 Then
            clCH.Fields("ncbamt") = txtNCB.Text
        End If
        If IsDate(rdpEndDate.SelectedDate) Then
            If clCH.Fields("enddate").Length > 0 Then
                If clCH.Fields("enddate") <> rdpEndDate.SelectedDate Then
                    bChange = True
                End If
            Else
                bChange = True
            End If
            clCH.Fields("enddate") = rdpEndDate.SelectedDate
        End If

        If clCH.RowCount = 0 Then
            clCH.Fields("creby") = hfUserID.Value
            clCH.Fields("credate") = Today
            clCH.AddRow()
            bAdd = True
        Else
            clCH.Fields("moddate") = Today
            clCH.Fields("modby") = hfUserID.Value
        End If
        clCH.SaveDB()
        If bAdd Then
            getcommissionheaderid
        End If
        If bAdd Then
            AddCommissions()
        Else
            If bChange Then
                DeleteCommissionDetail()
                AddCommissions()
            End If
        End If
        rgDealerCommission.Rebind()
        rgEssentils.Rebind()
        rgExotic.Rebind()
        rgGuard.Rebind()
        rgHighLine.Rebind()
        pnlCommissionDetail.Visible = False
        pnlCommission.Visible = True

    End Sub

    Private Sub DeleteCommissionDetail()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "delete commissions "
        SQL = SQL + "where commissionheaderid = " & hfCommissionHeaderID.Value
        clR.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
    End Sub

    Private Sub AddCommissions()
        Dim sClass As String()
        Dim clPPT As New clsDBO
        Dim SQL As String
        Dim clAC As New clsDBO
        Dim cnt As Long
        Dim cnt2 As Long
        If cboClass.Text = "All items checked" Then
            SQL = "select * from programtemplate "
            SQL = SQL + "where programid = " & cboProgram.SelectedValue & " "
            clPPT.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clPPT.RowCount > 0 Then
                For cnt = 0 To clPPT.RowCount - 1
                    clPPT.GetRowNo(cnt)
                    SQL = "select * from commissions "
                    SQL = SQL + "where dealerid = " & hfDealerID.Value & " "
                    SQL = SQL + "and commissionheaderid = " & hfCommissionHeaderID.Value & " "
                    SQL = SQL + "and ratetypeid = " & hfRateTypeID.Value & " "
                    If hfAgentID.Value > 0 Then
                        SQL = SQL + "and agentid = " & hfAgentID.Value & " "
                    End If
                    If chkNoChargeBack.Checked Then
                        AddNoChargeBack(clPPT.Fields("class"))
                    End If
                    SQL = SQL + "and programid = " & cboProgram.SelectedValue & " "
                    SQL = SQL + "and class = '" & clPPT.Fields("class") & "' "
                    SQL = SQL + "and startdate = '" & rdpStartDate.SelectedDate & "' "
                    clAC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                    If clAC.RowCount = 0 Then
                        clAC.NewRow()
                    Else
                        clAC.GetRow()
                    End If
                    clAC.Fields("dealerid") = hfDealerID.Value
                    clAC.Fields("ratetypeid") = hfRateTypeID.Value
                    clAC.Fields("agentid") = hfAgentID.Value
                    clAC.Fields("commissionheaderid") = hfCommissionHeaderID.Value
                    clAC.Fields("programid") = cboProgram.SelectedValue
                    clAC.Fields("class") = clPPT.Fields("class")
                    clAC.Fields("amt") = txtAmount.Text
                    clAC.Fields("startdate") = rdpStartDate.SelectedDate
                    If Not rdpEndDate.SelectedDate Is Nothing Then
                        clAC.Fields("enddate") = rdpEndDate.SelectedDate
                    End If
                    If clAC.RowCount = 0 Then
                        clAC.AddRow()
                    End If
                    clAC.SaveDB()

                Next
            End If
        Else
            sClass = cboClass.Text.Split(",")
            For cnt = 0 To sClass.Count - 1
                SQL = "select * from programtemplate "
                SQL = SQL + "where programid = " & cboProgram.SelectedValue & " "
                SQL = SQL + "and class = '" & sClass(cnt).Trim & "' "
                clPPT.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                If clPPT.RowCount > 0 Then
                    For cnt2 = 0 To clPPT.RowCount - 1
                        clPPT.GetRowNo(cnt2)
                        SQL = "select * from commissions "
                        SQL = SQL + "where dealerid = " & hfDealerID.Value & " "
                        SQL = SQL + "and commissionheaderid = " & hfCommissionHeaderID.Value & " "
                        SQL = SQL + "and ratetypeid = " & hfRateTypeID.Value & " "
                        If hfRateTypeID.Value = "13" Then
                            SQL = SQL + "and agentid = " & hfAgentID.Value & " "
                        End If
                        If hfRateTypeID.Value = 14 Then
                            SQL = SQL + "and subagentid = " & hfSubAgentID.Value & " "
                        End If
                        SQL = SQL + "and programid = " & cboProgram.SelectedValue & " "
                        SQL = SQL + "and class = '" & clPPT.Fields("class") & "' "
                        SQL = SQL + "and startdate = '" & rdpStartDate.SelectedDate & "' "
                        clAC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                        If clAC.RowCount = 0 Then
                            clAC.NewRow()
                        Else
                            clAC.GetRow()
                        End If
                        clAC.Fields("ratetypeid") = hfRateTypeID.Value
                        clAC.Fields("dealerid") = hfDealerID.Value
                        If hfRateTypeID.Value = "13" Then
                            clAC.Fields("agentid") = hfAgentID.Value
                            If chkNoChargeBack.Checked Then
                                AddNoChargeBack(clPPT.Fields("class"))
                            End If
                        End If
                        If hfSubAgentID.Value = "14" Then
                            clAC.Fields("subagentid") = hfSubAgentID.Value
                        End If
                        clAC.Fields("commissionheaderid") = hfCommissionHeaderID.Value
                        clAC.Fields("programid") = cboProgram.SelectedValue
                        clAC.Fields("class") = clPPT.Fields("class").Trim
                        clAC.Fields("amt") = txtAmount.Text
                        clAC.Fields("startdate") = rdpStartDate.SelectedDate
                        If Not rdpEndDate.SelectedDate Is Nothing Then
                            clAC.Fields("enddate") = rdpEndDate.SelectedDate
                        End If
                        If clAC.RowCount = 0 Then
                            clAC.AddRow()
                        End If
                        clAC.SaveDB()
                    Next
                End If
            Next
        End If
        rgDealerCommission.Rebind()
        rgEssentils.Rebind()
        rgExotic.Rebind()
        rgGuard.Rebind()
        rgHighLine.Rebind()
    End Sub

    Private Sub AddNoChargeBack(xClass As String)
        Dim clCH As New clsDBO
        Dim SQL As String
        SQL = "select * from commissions "
        SQL = SQL + "where dealerid = " & hfDealerID.Value & " "
        SQL = SQL + "and ratetypeid = 29 "
        SQL = SQL + "and commissionheaderid = " & hfCommissionHeaderID.Value & " "
        SQL = SQL + "and agentid = " & hfAgentID.Value & " "
        SQL = SQL + "and programid = " & cboProgram.SelectedValue & " "
        SQL = SQL + "and class = '" & xClass & "' "
        SQL = SQL + "and startdate = '" & rdpStartDate.SelectedDate & "' "
        clCH.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCH.RowCount = 0 Then
            clCH.NewRow()
        Else
            clCH.GetRow()
        End If
        clCH.Fields("ratetypeid") = 29
        clCH.Fields("dealerid") = hfDealerID.Value
        clCH.Fields("agentid") = hfAgentID.Value
        clCH.Fields("commissionheaderid") = hfCommissionHeaderID.Value
        clCH.Fields("programid") = cboProgram.SelectedValue
        clCH.Fields("class") = xClass
        If txtNCB.Text = "" Then
            clCH.Fields("amt") = (CDbl(txtAmount.Text) / 0.85) - CDbl(txtAmount.Text)
            GoTo MoveHere
        End If
        If txtNCB.Text = "0" Then
            clCH.Fields("amt") = (CDbl(txtAmount.Text) / 0.85) - CDbl(txtAmount.Text)
            GoTo MoveHere
        End If
        If CDbl(txtNCB.Text) < 2 Then
            clCH.Fields("amt") = (CDbl(txtAmount.Text) / (1 - CDbl(txtNCB.Text))) - CDbl(txtAmount.Text)
            GoTo MoveHere
        End If
        If CDbl(txtNCB.Text) > 2 Then
            clCH.Fields("amt") = txtNCB.Text
        End If
MoveHere:
        clCH.Fields("amt") = Math.Ceiling(CDbl(clCH.Fields("amt")))
        clCH.Fields("startdate") = rdpStartDate.SelectedDate
        If Not rdpEndDate.SelectedDate Is Nothing Then
            clCH.Fields("enddate") = rdpEndDate.SelectedDate
        End If
        If clCH.RowCount = 0 Then
            clCH.AddRow()
        End If
        clCH.SaveDB()
        rgEssentils.Rebind()
        rgExotic.Rebind()
        rgGuard.Rebind()
        rgHighLine.Rebind()
    End Sub

    Private Sub GetCommissionHeaderID()
        Dim SQL As String
        Dim clCH As New clsDBO
        SQL = "select max(commissionheaderid) as CH from commissionheader "
        SQL = SQL + "where creby = " & hfUserID.Value
        clCH.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCH.RowCount > 0 Then
            clCH.GetRow()
            hfCommissionHeaderID.Value = clCH.Fields("ch")
        End If
    End Sub

    Private Sub DeleteCommission()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "delete commissions "
        SQL = SQL + "where commissionheaderid = " & hfCommissionHeaderID.Value
        clR.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        SQL = "delete commissionheader "
        SQL = SQL + "where commissionheaderid = " & hfCommissionHeaderID.Value
        clR.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        rgDealerCommission.Rebind()
        rgEssentils.Rebind()
        rgExotic.Rebind()
        rgGuard.Rebind()
        rgHighLine.Rebind()
    End Sub

    Private Sub FillClass(xClass As String)
        Dim cnt As Long
        Dim cnt2 As Long
        Dim sSplit As String()
        If xClass = "All items checked" Then
            For cnt = 0 To cboClass.Items.Count - 1
                cboClass.Items(cnt).Checked = True
            Next
            Exit Sub
        End If
        sSplit = xClass.Split(",")
        For cnt = 0 To sSplit.Count - 1
            For cnt2 = 0 To cboClass.Items.Count - 1
                If cboClass.Items(cnt2).Text = sSplit(cnt).Trim Then
                    cboClass.Items(cnt2).Checked = True
                End If
            Next
        Next
    End Sub

    Private Sub btnSearchRateType_Click(sender As Object, e As EventArgs) Handles btnSearchRateType.Click
        rgRateType.Rebind()
        pnlCommissionDetail.Visible = False
        pnlSearchRateType.Visible = True
    End Sub

    Private Sub rgRateType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgRateType.SelectedIndexChanged
        hfRateTypeID.Value = rgRateType.SelectedValue
        txtRateType.Text = GetRateType(hfRateTypeID.Value)
        pnlCommissionDetail.Visible = True
        pnlSearchRateType.Visible = False
        If rgRateType.SelectedValue = 13 Then
            chkNoChargeBack.Visible = True
            trNCB.Visible = True
        End If
    End Sub

    Private Sub cboProgram_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboProgram.SelectedIndexChanged
        CalcClass()
    End Sub

    Private Sub CalcClass()
        Dim SQL As String
        Dim clPPT As New clsDBO
        Dim cnt As Long
        cboClass.Items.Clear()
        SQL = "select class from programtemplate "
        SQL = SQL + "where Programid = " & cboProgram.SelectedValue & " "
        SQL = SQL + "group by class "
        SQL = SQL + "order By class "
        clPPT.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clPPT.RowCount > 0 Then
            For cnt = 0 To clPPT.RowCount - 1
                clPPT.GetRowNo(cnt)
                cboClass.Items.Add(clPPT.Fields("class"))
            Next
        End If
    End Sub

    Private Sub btnAgent_Click(sender As Object, e As EventArgs) Handles btnAgent.Click
        rgAgent.Rebind()
        pnlSearchAgent.Visible = True
        pnlCommissionDetail.Visible = False
    End Sub

    Private Sub rgAgent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAgent.SelectedIndexChanged
        hfAgentID.Value = rgAgent.SelectedValue
        txtAgent.Text = GetAgentInfo(rgAgent.SelectedValue)
        pnlSearchAgent.Visible = False
        pnlCommissionDetail.Visible = True
    End Sub

    Private Sub rgSubAgent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgSubAgent.SelectedIndexChanged
        hfSubAgentID.Value = rgSubAgent.SelectedValue
        pnlCommissionDetail.Visible = True
        pnlSearchSubAgent.Visible = False
    End Sub

    Private Sub btnAddRateType_Click(sender As Object, e As EventArgs) Handles btnAddRateType.Click
        pnlSearchRateType.Visible = False
        pnlAddRateType.Visible = True
    End Sub

    Private Sub btnSaveCategory_Click(sender As Object, e As EventArgs) Handles btnSaveCategory.Click
        Dim SQL As String
        Dim clRT As New clsDBO
        SQL = "select * from ratetype "
        SQL = SQL + "where ratetypename = '" & txtRateType.Text & "' "
        clRT.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clRT.RowCount = 0 Then
            clRT.NewRow()
        Else
            clRT.GetRow()
        End If
        clRT.Fields("ratetypename") = txtRateTypeAdd.Text
        clRT.Fields("ratecategoryid") = cboRateCategoryAdd.SelectedValue
        If clRT.RowCount = 0 Then
            clRT.AddRow()
        End If
        clRT.SaveDB()
        rgRateType.Rebind()
        pnlSearchRateType.Visible = True
        pnlAddRateType.Visible = False
    End Sub

    Private Sub btnCancelCategory_Click(sender As Object, e As EventArgs) Handles btnCancelCategory.Click
        pnlSearchRateType.Visible = True
        pnlAddRateType.Visible = False
    End Sub

    Private Sub btnClearAgentInfo_Click(sender As Object, e As EventArgs) Handles btnClearAgentInfo.Click
        hfAgentID.Value = 0
        txtAgent.Text = ""
        pnlSearchAgent.Visible = False
        pnlCommissionDetail.Visible = True
    End Sub

    Private Sub btnClearSubAgentInfo_Click(sender As Object, e As EventArgs) Handles btnClearSubAgentInfo.Click
        hfSubAgentID.Value = 0
        pnlCommissionDetail.Visible = True
        pnlSearchSubAgent.Visible = False
    End Sub

    Private Sub btnDeleteCommission_Click(sender As Object, e As EventArgs) Handles btnDeleteCommission.Click
        pnlCommissionDetail.Visible = False
        pnlConfirmDelete.Visible = True
        lblProgramConfirm.Text = cboProgram.Text
        lblRateTypeConfirm.Text = txtRateType.Text
        lblAmtConfirm.Text = txtAmount.Text
    End Sub

    Private Sub btnNoConfirm_Click(sender As Object, e As EventArgs) Handles btnNoConfirm.Click
        pnlCommissionDetail.Visible = True
        pnlConfirmDelete.Visible = False
    End Sub

    Private Sub btnYesConfirm_Click(sender As Object, e As EventArgs) Handles btnYesConfirm.Click
        DeleteCommission()
        rgDealerCommission.Rebind()
        rgEssentils.Rebind()
        rgExotic.Rebind()
        rgGuard.Rebind()
        rgHighLine.Rebind()
        pnlCommission.Visible = True
        pnlConfirmDelete.Visible = False
    End Sub

    Private Sub btnCancelRateType_Click(sender As Object, e As EventArgs) Handles btnCancelRateType.Click
        pnlCommissionDetail.Visible = True
        pnlSearchRateType.Visible = False
    End Sub


End Class