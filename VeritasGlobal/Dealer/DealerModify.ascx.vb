﻿Public Class DealerModify
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsStates.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsDealerStatus.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsAgent.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsSubAgent.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()

        If Not IsPostBack Then
            pnlDetail.Visible = True
            pnlSearchAgent.Visible = False
            pnlSearchSubAgent.Visible = False
            hfDealerID.Value = Request.QueryString("dealerid")
            FillDealer()
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillDealer()
        Dim SQL As String
        Dim clD As New clsDBO
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & hfDealerID.Value
        clD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
            txtDealerNo.Text = clD.Fields("dealerno")
            txtDealerName.Text = clD.Fields("dealername")
            txtDealerEMail.Text = clD.Fields("email")
            txtPhone.Text = clD.Fields("phone")
            cboDealerStatus.SelectedValue = clD.Fields("dealerstatusid")
            hfAgentID.Value = clD.Fields("agentsid")
            txtAgentName.Text = GetAgentInfo(hfAgentID.Value)
            hfSubAgentID.Value = clD.Fields("subagentid")
            txtSubAgentName.Text = GetSubAgentInfo(hfSubAgentID.Value)
            txtAddr1.Text = clD.Fields("addr1")
            txtAddr2.Text = clD.Fields("addr2")
            txtCity.Text = clD.Fields("city")
            cboState.SelectedValue = clD.Fields("state")
            txtZip.Text = clD.Fields("zip")
            txtMoxyEntry.Text = clD.Fields("moxyentry")
            txtDateSigned.Text = clD.Fields("datesigned")
            lblCreBy.Text = GetUserInfo(clD.Fields("creby"))
            lblCreDate.Text = clD.Fields("credate")
            lblModBy.Text = clD.Fields("modby")
            lblModDate.Text = clD.Fields("moddate")
            txtDBA.Text = clD.Fields("dba")
            txtEIN.Text = clD.Fields("ein")
        Else
            txtDealerNo.Text = ""
            txtDealerEMail.Text = ""
            txtDealerName.Text = ""
            txtPhone.Text = ""
            cboDealerStatus.SelectedValue = 1
            hfAgentID.Value = 0
            txtAgentName.Text = ""
            hfSubAgentID.Value = 0
            txtSubAgentName.Text = ""
            txtAddr1.Text = ""
            txtAddr2.Text = ""
            txtCity.Text = ""
            cboState.SelectedValue = ""
            txtZip.Text = ""
            txtMoxyEntry.Text = ""
            txtDateSigned.Text = ""
            lblCreBy.Text = ""
            lblCreDate.Text = ""
            lblModBy.Text = ""
            lblModDate.Text = ""
            txtDBA.Text = ""
            txtEIN.Text = ""
        End If
    End Sub

    Private Sub btnSeekAgent_Click(sender As Object, e As EventArgs) Handles btnSeekAgent.Click
        pnlDetail.Visible = False
        pnlSearchAgent.Visible = True
    End Sub

    Private Sub rgAgent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAgent.SelectedIndexChanged
        hfAgentID.Value = rgAgent.SelectedValue
        txtAgentName.Text = GetAgentInfo(rgAgent.SelectedValue)
        pnlDetail.Visible = True
        pnlSearchAgent.Visible = False
    End Sub

    Private Sub btnSeekSubAgent_Click(sender As Object, e As EventArgs) Handles btnSeekSubAgent.Click
        pnlDetail.Visible = False
        pnlSearchSubAgent.Visible = True
    End Sub

    Private Sub rgSubAgent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgSubAgent.SelectedIndexChanged
        hfSubAgentID.Value = rgSubAgent.SelectedValue
        txtSubAgentName.Text = GetSubAgentInfo(rgSubAgent.SelectedValue)
        pnlDetail.Visible = True
        pnlSearchSubAgent.Visible = False
        Dim SQL As String
        Dim clA As New clsDBO
        SQL = "select * from subagents "
        SQL = SQL + "where subagentid = " & rgSubAgent.SelectedValue
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            hfAgentID.Value = clA.Fields("agentid")
            txtAgentName.Text = GetAgentInfo(clA.Fields("agentid"))
        End If

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        FillDealer()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clD As New clsDBO
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & hfDealerID.Value
        clD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
        Else
            clD.NewRow()
        End If
        clD.Fields("dealerno") = txtDealerNo.Text
        clD.Fields("dealername") = txtDealerName.Text
        clD.Fields("addr1") = txtAddr1.Text
        clD.Fields("addr2") = txtAddr2.Text
        clD.Fields("city") = txtCity.Text
        clD.Fields("state") = cboState.SelectedValue
        clD.Fields("zip") = txtZip.Text
        clD.Fields("phone") = txtPhone.Text
        clD.Fields("agentsid") = hfAgentID.Value
        clD.Fields("subagentid") = hfSubAgentID.Value
        If IsDate(txtMoxyEntry.Text) Then
            clD.Fields("moxyentry") = txtMoxyEntry.Text
        End If
        If IsDate(txtDateSigned.Text) Then
            clD.Fields("datesigned") = txtDateSigned.Text
        End If
        clD.Fields("email") = txtDealerEMail.Text
        clD.Fields("dba") = txtDBA.Text
        clD.Fields("ein") = txtEIN.Text
        clD.Fields("dealerstatusid") = cboDealerStatus.SelectedValue
        If clD.RowCount = 0 Then
            clD.Fields("creby") = hfUserID.Value
            clD.Fields("credate") = Today
            clD.AddRow()
        Else
            clD.Fields("moddate") = Today
            clD.Fields("modby") = hfUserID.Value
        End If
        clD.SaveDB()
        Redirectdealer

    End Sub

    Private Sub RedirectDealer()
        If hfDealerID.Value <> "0" Then
            Response.Redirect("dealer.aspx?sid=" & hfID.Value & "&dealerid=" & hfDealerID.Value)
        Else
            Response.Redirect("dealer.aspx?sid=" & hfID.Value & "&dealerid=" & getdealerid)
        End If
    End Sub

    Private Sub btnSubAgentClose_Click(sender As Object, e As EventArgs) Handles btnSubAgentClose.Click
        pnlDetail.Visible = True
        pnlSearchSubAgent.Visible = False
    End Sub

    Private Sub btnAgentClose_Click(sender As Object, e As EventArgs) Handles btnAgentClose.Click
        pnlDetail.Visible = True
        pnlSearchAgent.Visible = False
    End Sub

    Private Sub btnClearSubAgent_Click(sender As Object, e As EventArgs) Handles btnClearSubAgent.Click
        hfSubAgentID.Value = 0
        txtSubAgentName.Text = ""
        pnlDetail.Visible = True
        pnlSearchSubAgent.Visible = False
    End Sub

    Private Function GetDealerID() As Long
        GetDealerID = 0
        Dim SQL As String
        Dim clD As New clsDBO
        SQL = "select max(dealerid) as DealerID from dealer "
        SQL = SQL + "where creby = " & hfUserID.Value
        clD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
            GetDealerID = clD.Fields("dealerid")
        End If
    End Function
End Class