﻿Imports Telerik.Web.UI

Public Class Dealer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            FillDealer()

        End If
        If hfError.Value = "Visible" Then
            rwError.VisibleOnPageLoad = True
        Else
            rwError.VisibleOnPageLoad = False
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub FillDealer()
        Dim SQL As String
        Dim clD As New clsDBO
        tsDealer.Tabs(0).Selected = True
        If hfDealerID.Value = 0 Then
            tsDealer.Tabs(4).Selected = True
            pvModify.Selected = True
            Exit Sub
        End If
        pvContact.Selected = True
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & hfDealerID.Value
        clD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
            lblDealerNo.Text = clD.Fields("dealerno")
            lblDealerName.Text = clD.Fields("dealername")
            lblAddr1.Text = clD.Fields("addr1")
            lblAddr2.Text = clD.Fields("addr2")
            lblAddr3.Text = clD.Fields("city") & ", " & clD.Fields("state") & " " & clD.Fields("zip")
            FillSubAgent(clD.Fields("subagentid"))
            FillAgents(clD.Fields("agentsid"))
            FillStatus(clD.Fields("dealerstatusid"))
            lblDateSigned.Text = clD.Fields("datesigned")
            lblMoxyEntry.Text = clD.Fields("moxyentry")
            chkSalvage.Checked = CBool(clD.Fields("salvagetitle"))
        End If
    End Sub

    Private Sub FillStatus(xStatusID As Long)
        Dim SQL As String
        Dim clDS As New clsDBO
        SQL = "select * from dealerstatus "
        SQL = SQL + "where dealerstatusid = " & xStatusID
        clDS.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clDS.RowCount > 0 Then
            clDS.GetRow()
            lblStatus.Text = clDS.Fields("dealerstatus")
        End If
    End Sub

    Private Sub FillAgents(xAgencyID As Long)
        Dim clA As New clsDBO
        Dim SQL As String
        SQL = "select * from agents "
        SQL = SQL + "where agentid = " & xAgencyID
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            lblAgentNo.Text = clA.Fields("agentno")
            lblAgentName.Text = clA.Fields("agentname")
        End If
    End Sub

    Private Sub FillSubAgent(xAgentID As Long)
        Dim clA As New clsDBO
        Dim SQL As String
        SQL = "select * from subagents "
        SQL = SQL + "where subagentid = " & xAgentID
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            lblSubAgentName.Text = clA.Fields("subagentname")
            lblSubAgentNo.Text = clA.Fields("subagentno")
        End If


    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        hfDealerID.Value = Request.QueryString("dealerid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
    End Sub

    Private Sub tsDealer_TabClick(sender As Object, e As RadTabStripEventArgs) Handles tsDealer.TabClick
        If tsDealer.SelectedTab.Value = "Contact" Then
            pvContact.Selected = True
        End If
        If tsDealer.SelectedTab.Value = "Note" Then
            pvNote.Selected = True
        End If
        If tsDealer.SelectedTab.Value = "Contract" Then
            pvContract.Selected = True
        End If
        If tsDealer.SelectedTab.Value = "Documents" Then
            pvDocument.Selected = True
        End If
        If tsDealer.SelectedTab.Value = "Commission" Then
            pvCommission.Selected = True
        End If
        If tsDealer.SelectedTab.Value = "Modify" Then
            pvModify.Selected = True
        End If
        If tsDealer.SelectedTab.Value = "Overfund" Then
            pvOverfund.Selected = True
        End If
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub chkSalvage_CheckedChanged(sender As Object, e As EventArgs) Handles chkSalvage.CheckedChanged
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update dealer "
        If chkSalvage.Checked Then
            SQL = SQL + "set salvagetitle = " & 1 & " "
        Else
            SQL = SQL + "set salvagetitle = " & 0 & " "
        End If
        SQL = SQL + "where dealerid = " & hfDealerID.Value
        clR.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub
End Class