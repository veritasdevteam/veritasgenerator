﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ContractCancel.ascx.vb" Inherits="VeritasGlobal.ContractCancel" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Panel runat="server" ID="pnlCalcCancellation">
    <asp:Table runat="server">
        <asp:TableRow ID="trError">
            <asp:TableCell>
                <asp:Label ID="lblError" Font-Size="X-Large" ForeColor="Red" runat="server"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trMoxyCancel">
            <asp:TableCell  HorizontalAlign="Center">
                <asp:Button ID="btnMoxy" runat="server" Text="Moxy Cancelled Contract" BackColor="Red" ForeColor="Black" Font-Bold="true" Width="1000" Height="25"/>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trCancelCalc">
            <asp:TableCell>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">    
                            Cancel Eff. Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadDatePicker ID="rdpProcessDate" runat="server"></telerik:RadDatePicker>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Cancel Eff. Miles:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtMiles" NumberFormat-DecimalDigits="0" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="tcQuote">
                            <asp:Button ID="btnQuote" runat="server" Text="Quote"  BackColor="#1eabe2" />
                        </asp:TableCell>
                        <asp:TableCell ID="tcRequest">
                            <asp:Button ID="btnUnable" runat="server" Text="Unable to Process" BackColor="#1eabe2" />
                        </asp:TableCell>
                        <asp:TableCell ID="tcCancel">
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" BackColor="#1eabe2" />
                        </asp:TableCell>
                        <asp:TableCell ID="tcInvalid">
                            <asp:Button ID="btnInvalid" runat="server" Text="Invalid" BackColor="#1eabe2" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:HyperLink ID="hlWorksheet" Target="_blank" runat="server">View Worksheet</asp:HyperLink>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkDealerCancel" Text="Cancelled By Dealer" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trFL">
            <asp:TableCell>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            FL Cancellation Info:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:RadioButton ID="rbFLCustomer" Text="Customer/Dealer" GroupName="FLInfo" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:RadioButton ID="rbFLAdmin" Text="Admin Cancel" GroupName="FLInfo" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Cancel Percent:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCancelPercent" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Term Percent:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtTermPercent" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Mile Percent:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtMilePercent" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Cancel Status:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCancelStatus" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Gross Customer:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtGrossCustomer" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Gross Dealer:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtGrossDealer" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Gross Admin:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtGrossAdmin" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Claim Amt:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtClaimAmt" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Cancel Fee:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCancelFee" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Lienholder:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtLienholder" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Quote Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtQuoteDate" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Request Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtRequestDate" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Cancel Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCancelDate" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Calc Base:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCalcBase" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Customer Refund:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCustomerRefund" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            From Dealer:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtFromDealer" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            From Admin:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtFromAdmin" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="tcSaveCancel">
                            <asp:Button ID="btnSaveCancel" runat="server" BackColor="#1eabe2" Text="Save Cancel" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Payee:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:RadioButton ID="rbDealer" Text="Dealer" AutoPostBack="true" GroupName="Payee" Font-Bold="true" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:RadioButton ID="rbCustomer" Text="Customer" AutoPostBack="true" GroupName="Payee" Font-Bold="true" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:RadioButton ID="rbOther" Text="Other" AutoPostBack="true" GroupName="Payee" Font-Bold="true" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel runat="server" ID="pnlOther">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Company Name:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell Font-Bold="true">
                First Name:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell Font-Bold="true">
                Last Name:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Address 1:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtAddr1" runat="server"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell>
                Address 2:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtAddr2" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                City:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell>
                State:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                <asp:SqlDataSource ID="dsStates"
                ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
            </asp:TableCell>
            <asp:TableCell>
                Zip:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Phone:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel runat="server" ID="pnlPayment">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Payment Type:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadComboBox ID="cboPayType" DataSourceID="dsPayType" DataTextField="PayType" DataValueField="PayTypeID" runat="server"></telerik:RadComboBox>
                <asp:SqlDataSource ID="dsPayType"
                ProviderName="System.Data.SqlClient" SelectCommand="select paytypeid, paytype from paytype" runat="server"></asp:SqlDataSource>
            </asp:TableCell>
            <asp:TableCell Font-Bold="true">
                Payment Info:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtPaymentInfo" runat="server"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell Font-Bold="true">
                Payment Amount:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadTextBox ID="txtPaymentAmt" runat="server"></telerik:RadTextBox>
            </asp:TableCell>
            <asp:TableCell Font-Bold="true">
                Payment Date:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadDatePicker ID="txtPayDate" runat="server"></telerik:RadDatePicker>
            </asp:TableCell>
            <asp:TableCell ID="tcPayment">
                <asp:Button ID="btnPay" runat="server" Text="Update Payment Info" BackColor="#1eabe2" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel runat="server" ID="pnlNote">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Note:
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:TextBox ID="txtNote" TextMode="MultiLine" Width="1000" Height="100" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right" >
                <asp:Button ID="btnUpdateNote" runat="server" Text="Update Note" BackColor="#1eabe2"/>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnActivate" runat="server" Text="Activate Contract" BackColor="#1eabe2"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel ID="pnlPaylink" runat="server" BackColor="Yellow">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:TextBox ID="txtPaylink" BackColor="Yellow" TextMode="MultiLine" BorderStyle="None" Width="1000" Height="100"  runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnClose" runat="server" Text="Close Info" BackColor="#1eabe2" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel ID="pnlErrorAPPI" runat="server" Visible="false">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Error getting APPI Data. Will need to look at the APPI website to get info.
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Cancel Percent:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadNumericTextBox ID="txtErrorCancelPer" Value="0" NumberFormat-DecimalDigits="2" Type="Percent" runat="server"></telerik:RadNumericTextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Cancel Fee:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadNumericTextBox ID="txtErrorCancelFee" Value="0" NumberFormat-DecimalDigits="2" Type="Number" runat="server"></telerik:RadNumericTextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnErrorSave" BackColor="#1eabe2" runat="server" Text="Save" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnErrorCancel" BackColor="#1eabe2" runat="server" Text="Cancel" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>

        <asp:HiddenField ID="hfCancelPopup" runat="server" />
        <telerik:RadWindow ID="rwCancelPopup"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblConfirm" Text="Would you like to Cancel 100%?"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnYes" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="Yes" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnNo" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="No" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>

<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfContractID" runat="server" />
<asp:HiddenField ID="hfRateTypeID" runat="server" />
<asp:HiddenField ID="hfContractAmtID" runat="server" />
<asp:HiddenField ID="hfCancelInfo" runat="server" />
<asp:HiddenField ID="hfCancelType" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfState" runat="server" />
<asp:HiddenField ID="hfCancel100" runat="server" />
