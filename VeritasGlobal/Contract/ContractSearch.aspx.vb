﻿Public Class ContractSearch
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        dsStates.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()
        CheckToDo()
        CheckClaimPopup()
        GetAgentID()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
        End If

    End Sub

    Private Sub GetAgentID()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfAgentID.Value = clR.Fields("agentid")
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfInsCarrierID.Value = clSI.Fields("inscarrierid")
            btnUsers.Enabled = True
            hfVeroOnly.Value = CBool(clSI.Fields("veroonly"))
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        SearchContracts()
    End Sub

    Private Sub SearchContracts()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select contractid, contractno, dealername, fname, lname, c.city, c.state, VIN, make, model, saledate, a.agentname "
        SQL = SQL + "from contract c "
        SQL = SQL + "left join dealer d on c.dealerid = d.dealerid "
        SQL = SQL + "left join agents a on c.agentsid = a.agentid "
        SQL = SQL + "where contractid > 0 "
        If txtCity.Text.Length > 0 Then
            SQL = SQL + "and city like '%" & txtCity.Text & "%' "
        End If
        If txtContractNo.Text.Length > 0 Then
            SQL = SQL + "and contractno like '%" & txtContractNo.Text.Trim & "%' "
        End If
        If txtDealerNo.Text.Length > 0 Then
            SQL = SQL + "and dealerno like '%" & txtDealerNo.Text.Trim & "%' "
        End If
        If txtDealername.Text.Length > 0 Then
            SQL = SQL + "and dealername like '%" & txtDealername.Text.Trim & "%' "
        End If
        If txtAgentName.Text.Length > 0 Then
            SQL = SQL + "and agentname like '%" & txtAgentName.Text.Trim & "%' "
        End If
        If txtFName.Text.Length > 0 Then
            SQL = SQL + "and fname like '%" & txtFName.Text.Trim & "%' "
        End If
        If txtLName.Text.Length > 0 Then
            SQL = SQL + "and lname like '%" & txtLName.Text.Trim & "%' "
        End If
        If txtCity.Text.Length > 0 Then
            SQL = SQL + "and c.city like '%" & txtCity.Text.Trim & "%' "
        End If
        If cboState.Text.Length > 0 Then
            SQL = SQL + "and c.state like '%" & cboState.Text.Trim & "%' "
        End If
        If txtPhone.Text.Length > 0 Then
            SQL = SQL + "and c.phone like '%" & txtPhone.Text.Trim & "%' "
        End If
        If IsDate(txtSaleDate.Text) Then
            SQL = SQL + "and saledate = '" & txtSaleDate.Text.Trim & "' "
        End If
        If IsDate(txtPayDate.Text) Then
            SQL = SQL + "and datepaid = '" & txtPayDate.Text.Trim & "' "
        End If
        If txtVIN.Text.Length > 0 Then
            SQL = SQL + "and vin like '%" & txtVIN.Text.Trim & "%' "
        End If
        If txtCheckNo.Text.Length > 0 Then
            SQL = SQL + "and contractid in (select contractid from contractpayment where checkno like '%" & txtCheckNo.Text.Trim & "%' "
        End If
        If hfInsCarrierID.Value > 0 Then
            SQL = SQL + "and inscarrierid = " & hfInsCarrierID.Value & " "
        End If
        If hfVeroOnly.Value = "True" Then
            SQL = SQL + "and a.agentid = 54 "
        End If
        If hfAgentID.Value.Length > 0 Then
            If hfAgentID.Value <> 0 Then
                SQL = SQL + "and a.agentid = " & hfAgentID.Value & " "
            End If
        End If
        SQL = SQL + "order by lname "
        rgContractID.DataSource = clC.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        rgContractID.DataBind()
    End Sub

    Private Sub rgContractID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgContractID.SelectedIndexChanged
        Response.Redirect("~/contract/contract.aspx?sid=" & hfID.Value & "&contractid=" & rgContractID.SelectedValue)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub CheckClaimPopup()
        Dim clCA As New VeritasGlobalTools.clsClaimAssign
        If clCA.CheckUserInfo(hfUserID.Value) Then
            hfClaimID.Value = clCA.CheckClaimUnassigned
            If hfClaimID.Value > "0" Then
                hfClaimNo.Value = clCA.GetClaimNo(hfClaimID.Value)
                txtClaimNo.Text = hfClaimNo.Value & " is waiting to be accepted for processing."
                If clCA.IsContractAN(hfClaimID.Value) Then
                    If clCA.CheckANUser(hfUserID.Value) Then
                        ShowClaimAssign()
                    End If
                Else
                    If clCA.CheckANUser(hfUserID.Value) Then
                        ShowClaimAssign()
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub ShowClaimAssign()
        rwClaimAssign.Visible = True
        hfClaimAssign.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwClaimAssign.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwClaimAssign", script, True)
    End Sub

    Private Sub HideClaimAssign()
        hfClaimAssign.Value = ""
        Dim script As String = "function f(){$find(""" + rwClaimAssign.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
        rwClaimAssign.Visible = False
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        Dim clCA As New VeritasGlobalTools.clsClaimAssign
        If clCA.CheckClaimUnassigned2(hfClaimID.Value) > 0 Then
            clCA.AddAccepted(hfClaimID.Value, hfUserID.Value)
            clCA.UpdateClaimAssign(hfClaimID.Value, hfUserID.Value)
        End If
        HideClaimAssign()
    End Sub

    Private Sub btnDeny_Click(sender As Object, e As EventArgs) Handles btnDeny.Click
        Dim clCA As New VeritasGlobalTools.clsClaimAssign
        clCA.AddDeny(hfClaimID.Value, hfUserID.Value)
        HideClaimAssign()
    End Sub
End Class