﻿Imports System.Configuration.ConfigurationManager

Public Class ContractModification
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hfContractID.Value = Request.QueryString("contractid")
            GetServerInfo()
            ReadOnlyButtons()
            FillHistory()
            filldetail
        End If
    End Sub

    Private Sub FillDetail()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtSaleDate.SelectedDate = clR.Fields("saledate")
            txtSaleMile.Text = clR.Fields("salemile")
            txtLienholder.Text = clR.Fields("lienholder")
            txtDealerCost.Text = clR.Fields("moxydealercost")
            txtCustomerCost.Text = clR.Fields("customercost")
        End If
    End Sub

    Private Sub FillHistory()
        Dim clH As New clsDBO
        Dim SQL As String
        SQL = "select ContractHistoryID, ContractID, fieldname, oldvalue, newvalue, credate, username from contracthistory ch
        inner join UserInfo ui on ui.userid = ch.CreBy "
        SQL = SQL + "where contractid = " & hfContractID.Value & " "
        SQL = SQL + "order by credate desc "
        rgHistory.DataSource = clH.GetData(SQL, AppSettings("connstring"))
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If Not CBool(clR.Fields("contractmodification")) Then
                'btnUpdateSaleDate.Enabled = False
                'btnUpdateSaleMile.Enabled = False
                'btnSave.Visible = False
            End If
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    'Private Sub btnUpdateSaleMile_Click(sender As Object, e As EventArgs) Handles btnUpdateSaleMile.Click
    '    Dim clC As New clsDBO
    '    Dim SQL As String
    '    Dim lMileDiff As Long
    '    If txtNewSaleMile.Text.Length = 0 Then
    '        Exit Sub
    '    End If
    '    SQL = "select * from contract "
    '    SQL = SQL + "where contractid = " & hfContractID.Value
    '    clC.OpenDB(SQL, AppSettings("connstring"))
    '    If clC.RowCount > 0 Then
    '        clC.GetRow()
    '        lMileDiff = CLng(clC.Fields("effmile")) - CLng(clC.Fields("salemile"))
    '        UpdateHistory("SaleMile", clC.Fields("salemile"), txtNewSaleMile.Text)
    '        clC.Fields("salemile") = txtNewSaleMile.Text
    '        clC.Fields("effmile") = CLng(txtNewSaleMile.Text) + lMileDiff
    '        clC.Fields("expmile") = CLng(clC.Fields("effmile")) + CLng(clC.Fields("termmile"))
    '        clC.SaveDB()
    '        Response.Redirect("contract.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value)
    '    End If
    'End Sub

    'Private Sub btnUpdateSaleDate_Click(sender As Object, e As EventArgs) Handles btnUpdateSaleDate.Click
    '    Dim SQL As String
    '    Dim clC As New clsDBO
    '    Dim lDayDiff As Long
    '    If txtNewSaleDate.Text.Length = 0 Then
    '        Exit Sub
    '    End If
    '    SQL = "select * from contract "
    '    SQL = SQL + "where contractid = " & hfContractID.Value
    '    clC.OpenDB(SQL, AppSettings("connstring"))
    '    If clC.RowCount > 0 Then
    '        clC.GetRow()
    '        lDayDiff = DateDiff(DateInterval.Day, CDate(clC.Fields("saledate")), CDate(clC.Fields("effdate")))
    '        UpdateHistory("SaleDate", clC.Fields("saledate"), txtNewSaleDate.Text)
    '        clC.Fields("saledate") = txtNewSaleDate.Text
    '        clC.Fields("effdate") = DateAdd(DateInterval.Day, lDayDiff, CDate(clC.Fields("saledate")))
    '        clC.Fields("expdate") = DateAdd(DateInterval.Month, CLng(clC.Fields("termmonth")), CDate(clC.Fields("effdate")))
    '        clC.SaveDB()
    '        Response.Redirect("contract.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value)
    '    End If

    'End Sub

    Private Sub UpdateHistory(xFieldName As String, xOldValue As String, xNewValue As String)
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select * from contracthistory "
        SQL = SQL + "where contracthistoryid =  0 "
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        clC.NewRow()
        clC.Fields("contractid") = hfContractID.Value
        clC.Fields("fieldname") = xFieldName
        clC.Fields("OldValue") = xOldValue
        clC.Fields("newvalue") = xNewValue
        clC.Fields("creby") = hfUserID.Value
        clC.Fields("credate") = Today
        clC.AddRow()
        clC.SaveDB()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        Dim lMileDiff As Long
        Dim lDayDiff As Long
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("salemile") <> txtSaleMile.Text Then
                UpdateHistory("SaleMile", clR.Fields("salemile"), txtSaleMile.Text)
                lMileDiff = CLng(clR.Fields("effmile")) - CLng(clR.Fields("salemile"))
                clR.Fields("salemile") = txtSaleMile.Text
                clR.Fields("effmile") = CLng(txtSaleMile.Text) + lMileDiff
                clR.Fields("expmile") = CLng(clR.Fields("effmile")) + CLng(clR.Fields("termmile"))
            End If
            If CDate(clR.Fields("saledate")) <> CDate(txtSaleDate.SelectedDate) Then
                UpdateHistory("SaleDate", clR.Fields("saledate"), txtSaleDate.SelectedDate)
                lDayDiff = DateDiff(DateInterval.Day, CDate(clR.Fields("saledate")), CDate(clR.Fields("effdate")))
                clR.Fields("saledate") = txtSaleDate.SelectedDate
                clR.Fields("effdate") = DateAdd(DateInterval.Day, lDayDiff, CDate(clR.Fields("saledate")))
                clR.Fields("expdate") = DateAdd(DateInterval.Month, CLng(clR.Fields("termmonth")), CDate(clR.Fields("effdate")))
            End If
            If clR.Fields("lienholder") <> txtLienholder.Text Then
                UpdateHistory("Lienholder", clR.Fields("lienholder"), txtLienholder.Text)
                clR.Fields("lienholder") = txtLienholder.Text
            End If
            If CDbl(clR.Fields("moxydealercost")) <> CDbl(txtDealerCost.Text) Then
                UpdateHistory("DealerCost", clR.Fields("moxydealercost"), txtDealerCost.Text)
                clR.Fields("moxydealercost") = txtDealerCost.Text
            End If
            If CDbl(clR.Fields("customercost")) <> CDbl(txtCustomerCost.Text) Then
                UpdateHistory("customercost", clR.Fields("customercost"), txtCustomerCost.Text)
                clR.Fields("customercost") = txtCustomerCost.Text
            End If
            clR.SaveDB()
        End If
        Response.Redirect("contract.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value)
    End Sub
End Class