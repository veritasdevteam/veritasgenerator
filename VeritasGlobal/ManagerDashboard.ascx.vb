﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager

Public Class ManagerDashboard
    Inherits System.Web.UI.UserControl
    Public UserID As Long
    Public ID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Sub Update()
        hfID.Value = ID
        GetServerInfo()
        GetTeamInfo()
        SetDashboard()
    End Sub

    Private Sub GetTeamInfo()
        Dim clTI As New clsDBO
        Dim sSQL As String
        sSQL = "select TeamID, TeamLead from UserSecurityInfo where UserID = " + hfUserID.Value
        clTI.OpenDB(sSQL, AppSettings("connstring"))
        If clTI.RowCount > 0 Then
            clTI.GetRow()
            hfTeamID.Value = Long.Parse(clTI.Fields("TeamID"))
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)

        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If

    End Sub

    Public Sub SetDashboard()
        If hfTeamID.Value.Length = 0 Then
            Exit Sub
        End If
        Try
            Dim sSQL As String
            Dim clR As New clsDBO
            htmlClaimsOpened.Visible = True
            htmlInbound.Visible = True
            htmlNumOfClaimsMovedFromOpen.Visible = True
            htmlDnD.Visible = True
            htmlOutBound.Visible = True
            sSQL = "select ui.FName + ' ' +  ui.LName as names, ui.UserID from UserInfo ui left join UserSecurityInfo usi on usi.UserID = ui.UserID where TeamID =" + hfTeamID.Value + " and Active = 1"
            ddlAgents.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
            ddlAgents.DataBind()
            ddlAgents.DataTextField = "names"
            ddlAgents.DataValueField = "UserID"
            ddlAgents.DataBind()
            ddlAgents.Visible = True
            btnRevertGraphs.Visible = True
            btnChangeGraphs.Visible = True

            sSQL = "select CallDate, (sum(InboundCt))/(COUNT(*)) as count from VeritasPhone.dbo.AgentDailyCallSummary " +
                "where UserID in (select UserID from UserSecurityInfo where TeamID = " + hfTeamID.Value + " and TeamLead != 1) " +
                "and CallDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                "and CallDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' " +
                "group by CallDate"
            htmlInbound.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
            sSQL = "select CallDate, (sum(OutboundCt))/(COUNT(*)) as count from VeritasPhone.dbo.AgentDailyCallSummary " +
                "where UserID in (select UserID from UserSecurityInfo where TeamID = " + hfTeamID.Value + " and TeamLead != 1) " +
                "and CallDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                "and CallDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' " +
                "group by CallDate"
            htmlOutBound.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
            sSQL = "select date, (sum(TotalDuration/60000))/(COUNT(*)) as count from VeritasPhone.dbo.AgentDnD " +
                "where UserID in (select UserID from UserSecurityInfo where TeamID = " + hfTeamID.Value + " and TeamLead != 1) " +
                "and date <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                "and date > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' " +
                "group by date"
            htmlDnD.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
            sSQL = "select CreDate, count(*) as numOfClaimsOpened,0 as goal " +
                "from Claim where CreDate < '" + DateTime.Today + "' " +
                "and CreDate > '" + DateTime.Today.AddDays(-8) + "'" +
                "and CreBy in (select UserID from UserSecurityInfo where TeamID = " + hfTeamID.Value + " and TeamLead != 1) " +
                "group by CreDate"
            htmlClaimsOpened.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
            sSQL = "select CloseDate, count(*) as numOfClaimsClosed,0 as goal " +
                "from Claim where CloseDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                "and CloseDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "'" +
                "and CloseBy in (select UserID from UserSecurityInfo where TeamID = " + hfTeamID.Value + " and TeamLead != 1) " +
                "group by CloseDate"
            htmlNumOfClaimsMovedFromOpen.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
            htmlNumOfClaimsMovedFromOpen.DataBind()



            htmlInbound.ChartTitle.Text = "Inbound Average"
            htmlOutBound.ChartTitle.Text = "Outbound Average"
            htmlDnD.ChartTitle.Text = "DnD Average"
            htmlClaimsOpened.ChartTitle.Text = "Number of opened claims"

            htmlNumOfClaimsMovedFromOpen.ChartTitle.Text = "Number of claims moved from open"
            sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc, ui.fname + ' ' + ui.lname as Adjuster from Claim " +
           "cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID " +
           "left join userinfo ui on ui.userid = cl.CreBy " +
           "where Status = 'open' " +
           "and CreBy in (select userID from UserSecurityInfo where TeamID = " + hfTeamID.Value + ") order by ClaimAge desc "
            rgClaimOpen.Columns(6).Visible = True
            rgClaimOpen.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
            rgClaimOpen.DataBind()
            sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc, ui.fname + ' ' + ui.lname as Adjuster 
                        from Claim cl 
                        left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID 
                        left join userinfo ui on ui.userid = cl.CreBy 
                        where cl.ClaimActivityID = 3 
                        and status = 'Open' 
                        order by ClaimAge desc "
            rgClaimManagment.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
            rgClaimManagment.DataBind()
            rgClaimManagment.Visible = True
            htmlClaimsOpened.Visible = True
            htmlNumOfClaimsMovedFromOpen.Visible = True
            rgClaimManagment.MasterTableView.Caption = "Managers Claim Open"
        Catch ex As Exception

        End Try
    End Sub

    Private Sub rgClaimOpen_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimOpen.SelectedIndexChanged
        Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + Convert.ToString(rgClaimOpen.SelectedValue))
    End Sub

    Private Sub rgClaimManagment_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimManagment.SelectedIndexChanged
        Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + Convert.ToString(rgClaimManagment.SelectedValue))
    End Sub

    Private Sub rgClaimOpen_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles rgClaimOpen.NeedDataSource
        SetDashboard()
    End Sub

    Private Sub rgClaimManagment_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles rgClaimManagment.NeedDataSource
        SetDashboard()
    End Sub

    Private Sub btnChangeGraphs_Click(sender As Object, e As EventArgs) Handles btnChangeGraphs.Click
        Dim sSQL As String
        Dim clR As New clsDBO
        Dim clSI As New clsDBO
        Dim sStartDate As String
        Dim sEndDate As String
        sStartDate = DateTime.Today
        sEndDate = DateTime.Today.AddDays(1)
        Dim iSelectedAgent As Int32
        Dim iSelectedIndex As Int32
        iSelectedAgent = Convert.ToInt32(ddlAgents.SelectedValue)
        iSelectedIndex = ddlAgents.SelectedIndex
        Dim sSelectedAgent As String
        sSelectedAgent = ddlAgents.SelectedItem.Text
        sSQL = "select ui.FName + ' ' +  ui.LName as names, ui.UserID from UserInfo ui left join UserSecurityInfo usi on usi.UserID = ui.UserID where TeamID =" + hfTeamID.Value + " and Active = 1"
        ddlAgents.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
        ddlAgents.DataBind()
        ddlAgents.DataTextField = "names"
        ddlAgents.DataValueField = "UserID"
        ddlAgents.DataBind()
        ddlAgents.Visible = True
        ddlAgents.SelectedIndex = iSelectedIndex
        btnRevertGraphs.Visible = True
        btnChangeGraphs.Visible = True



        sSQL = "select UserID, CallDate,'Inbound Calls' as title ,InboundCt as count, " +
                "(case when (select count(goal) from UserGoals where GoalsID = 1 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 1 and UserID = ia.UserID) end )as goal " +
                " from VeritasPhone.dbo.AgentDailyCallSummary ia where UserID = '" & iSelectedAgent & "' and CallDate < '" & DateTime.Today & "' and CallDate > '" & DateTime.Today.AddDays(-8) & "' and ((DATEPART(dw, CallDate) + @@DATEFIRST) % 7) NOT IN (0, 1)"
        htmlInbound.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
        sSQL = "select UserID, CallDate,'Outbound Calls' as title ,OutboundCt as count," +
            "(case when (select count(goal) from UserGoals where GoalsID = 2 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 2 and UserID = ia.UserID) end )as goal " +
            " from VeritasPhone.dbo.AgentDailyCallSummary ia where UserID = '" & iSelectedAgent & "' and CallDate < '" & DateTime.Today & "' and CallDate > '" & DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") & "' and ((DATEPART(dw, CallDate) + @@DATEFIRST) % 7) NOT IN (0, 1)"
        htmlOutBound.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
        sSQL = "select userID, date as CallDate, 'DnD Total' as title, (TotalDuration/60000) as count," +
            "(case when (select count(goal) from UserGoals where GoalsID = 3 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 3 and UserID = ia.UserID) end )as goal " +
            " from VeritasPhone.dbo.AgentDnD ia where UserID = '" & iSelectedAgent & "' and date < '" + DateTime.Today + "' and date > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' and ((DATEPART(dw, date) + @@DATEFIRST) % 7) NOT IN (0, 1)"
        htmlDnD.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
        sSQL = "select CreDate, count(*) as numOfClaimsOpened " +
               "from Claim where CreDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                "and CreDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "'" +
                "and CreBy in (" & iSelectedAgent & ") " +
                "group by CreDate"
        htmlClaimsOpened.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
        sSQL = "select CloseDate, count(*) as numOfClaimsClosed " +
            "from Claim where CloseDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
            "and CloseDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "'" +
            "and CloseBy in (" & iSelectedAgent & ") " +
            "group by CloseDate"
        htmlNumOfClaimsMovedFromOpen.DataSource = clR.GetData(sSQL, AppSettings("connstring"))

        htmlInbound.ChartTitle.Text = "Inbound Stats"
        htmlOutBound.ChartTitle.Text = "Outbound Average"
        htmlDnD.ChartTitle.Text = "DnD Average"
        htmlClaimsOpened.ChartTitle.Text = "Number of opened claims Stats"
        htmlNumOfClaimsMovedFromOpen.ChartTitle.Text = "Number of claims moved from open"
        sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc, ui.fname + ' ' + ui.lname as Adjuster from Claim " +
           "cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID " +
           "left join userinfo ui on ui.userid = cl.CreBy " +
           "where Status = 'open' " +
            "and CreBy = " & iSelectedAgent & " order by ClaimAge desc "
        htmlInbound.Visible = True
        htmlOutBound.Visible = True
        htmlDnD.Visible = True
        rgClaimOpen.Visible = True
        htmlClaimsOpened.Visible = True
        htmlNumOfClaimsMovedFromOpen.Visible = True
        rgClaimOpen.DataSource = clR.GetData(sSQL, AppSettings("connstring"))

        sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc, ui.fname + ' ' + ui.lname as Adjuster from Claim cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID left join userinfo ui on ui.userid = cl.CreBy where cl.ClaimActivityID = 3 order by ClaimAge desc "
        rgClaimManagment.Visible = True
        rgClaimManagment.DataSource = clR.GetData(sSQL, AppSettings("connstring"))
        rgClaimManagment.MasterTableView.Caption = "Managers Claim Open"
        rgClaimOpen.MasterTableView.Caption = "Claims Open"
        Try
            'tables
            rgClaimOpen.DataBind()
            rgClaimManagment.DataBind()

            'graphs
            htmlInbound.DataBind()
            htmlOutBound.DataBind()
            htmlDnD.DataBind()
            htmlClaimsOpened.DataBind()
            htmlNumOfClaimsMovedFromOpen.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnRevertGraphs_Click(sender As Object, e As EventArgs) Handles btnRevertGraphs.Click
        SetDashboard()
    End Sub
End Class