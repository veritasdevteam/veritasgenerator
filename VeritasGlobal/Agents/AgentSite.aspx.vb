﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager

Public Class AgentSite
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SqlDataSource1.ConnectionString = AppSettings("connstring")
        SqlDataSource2.ConnectionString = AppSettings("connstring")
        SqlDataSource3.ConnectionString = AppSettings("connstring")
        GetServerInfo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            pnlList.Visible = True
            pnlDetail.Visible = False
            pnlAgentSearch.Visible = False
            pnlSubAgentSeek.Visible = False
            btnSend.Visible = False
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub rgAgents_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAgents.SelectedIndexChanged

        Dim SQL As String
        Dim clR As New clsDBO
        hfAgentID.Value = rgAgents.SelectedValue
        SQL = "select * from veritasagent.dbo.userinfo "
        SQL = SQL + "where userid = " & hfAgentID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            pnlDetail.Visible = True
            pnlList.Visible = False
            clR.GetRow()
            txtEMail.Text = clR.Fields("email")
            txtFName.Text = clR.Fields("fname")
            txtLName.Text = clR.Fields("lname")
            txtPassword.Text = clR.Fields("password")
            chkActive.Checked = CBool(clR.Fields("active"))
            txtAgent.Text = GetAgentInfo(clR.Fields("agentid"))
            If clR.Fields("subagentid").Length > 0 Then
                txtSubAgent.Text = GetSubAgentInfo(clR.Fields("subagentid"))
            Else
                txtSubAgent.Text = ""
            End If
            btnSend.Visible = True
        End If
    End Sub

    Private Sub btnSeekAgent_Click(sender As Object, e As EventArgs) Handles btnSeekAgent.Click
        pnlDetail.Visible = False
        pnlAgentSearch.Visible = True
    End Sub

    Private Sub btnAgentSeekCancel_Click(sender As Object, e As EventArgs) Handles btnAgentSeekCancel.Click
        pnlAgentSearch.Visible = False
        pnlDetail.Visible = True
    End Sub

    Private Sub rgAgentSeek_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAgentSeek.SelectedIndexChanged
        hfAgentSeekID.Value = rgAgentSeek.SelectedValue
        pnlAgentSearch.Visible = False
        pnlDetail.Visible = True
        txtAgent.Text = GetAgentInfo(hfAgentSeekID.Value)
    End Sub

    Private Sub btnSeekSubAgent_Click(sender As Object, e As EventArgs) Handles btnSeekSubAgent.Click
        pnlDetail.Visible = False
        pnlSubAgentSeek.Visible = True
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub btnSubAgentSeekCancel_Click(sender As Object, e As EventArgs) Handles btnSubAgentSeekCancel.Click
        pnlSubAgentSeek.Visible = False
        pnlDetail.Visible = True
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        pnlDetail.Visible = True
        pnlList.Visible = False
        txtAgent.Text = ""
        txtEMail.Text = ""
        txtFName.Text = ""
        txtLName.Text = ""
        txtPassword.Text = ""
        txtSubAgent.Text = ""
        btnSend.Visible = False
        hfAgentID.Value = 0
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from veritasagent.dbo.userinfo "
        SQL = SQL + "where userid = " & hfAgentID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
        Else
            clR.GetRow()
        End If
        clR.Fields("email") = txtEMail.Text
        clR.Fields("password") = txtPassword.Text
        clR.Fields("fname") = txtFName.Text
        clR.Fields("lname") = txtLName.Text
        clR.Fields("active") = chkActive.Checked
        clR.Fields("agentid") = hfAgentSeekID.Value
        clR.Fields("subagentid") = hfSubAgentSeekID.Value
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
        rgAgents.Rebind()
        pnlDetail.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub rgSubAgents_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgSubAgents.SelectedIndexChanged
        hfSubAgentSeekID.Value = rgSubAgents.SelectedValue
        pnlDetail.Visible = True
        pnlSubAgentSeek.Visible = False
        txtSubAgent.Text = GetSubAgentInfo(hfSubAgentSeekID.Value)
    End Sub

    Private Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        Dim clA As New VeritasGlobalTools.clsEMailAgentSite
        clA.UserID = hfAgentID.Value
        clA.SendEmail()
        pnlDetail.Visible = False
        pnlList.Visible = True
    End Sub
End Class