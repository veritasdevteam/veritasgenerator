﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SubAgent.aspx.vb" Inherits="VeritasGlobal.SubAgent" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="SubAgentNote.ascx" TagName="SubAgentNote" TagPrefix="uc" %>
<%@ Register Src="SubAgentDealers.ascx" TagName="SubAgentDealer" TagPrefix="uc" %>
<%@ Register Src="SubAgentContracts.ascx" TagName="SubAgentContracts" TagPrefix="uc" %>
<%@ Register Src="SubAgentModify.ascx" TagName="SubAgentModify" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Sub Agent</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadButton ID="btnHome" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                            <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                        </telerik:RadButton>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <hr />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadButton ID="btnSubAgent" runat="server" Text="Sub Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                            <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                        </telerik:RadButton>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadButton ID="btnAgentSite" runat="server" Text="Agent Site"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                            <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                        </telerik:RadButton>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <hr />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadButton ID="btnAgents" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                            <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                        </telerik:RadButton>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadButton ID="btnDealer" runat="server" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                            <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                        </telerik:RadButton>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadButton ID="btnContract" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                            <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                        </telerik:RadButton>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadButton ID="btnClaim" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                            <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                        </telerik:RadButton>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadButton ID="btnAccounting" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                            <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                        </telerik:RadButton>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadButton ID="btnReports" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                            <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                        </telerik:RadButton>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                            <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                        </telerik:RadButton>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadButton ID="btnUsers" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                            <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                        </telerik:RadButton>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadButton ID="btnLogOut" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                            <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                        </telerik:RadButton>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:Panel>
                </asp:TableCell>
                <asp:TableCell VerticalAlign="Top">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true" Font-Size="Large">
                                            Sub Agent No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Label ID="lblSubAgentNo" runat="server" Font-Size="Large" Text=""></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true" Font-Size="Large">
                                            Sub Agent Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Label ID="lblSubAgentName" runat="server" Font-Size="Large" Text=""></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell VerticalAlign="Top">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Address 1:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblAddr1" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Address 2:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblAddr2" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        City, State, Zip:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblAddr3" runat="server"  Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        E-Mail:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblEMail" runat="server"  Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Phone:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblPhone" runat="server"  Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                        <asp:TableCell VerticalAlign="Top">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        DBA:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblDBA" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        EIN:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblEIN" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Agent:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblAgent" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Status:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                        <asp:TableCell VerticalAlign="Top">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Create By:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblCreBy" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Create Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblCreDate" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Modified By:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblModifyBy" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Modified Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblModifyDate" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadTabStrip ID="tsAgent" runat="server">
                                    <Tabs>
                                        <telerik:RadTab Text="Notes" Value="Note"></telerik:RadTab>
                                        <telerik:RadTab Text="Dealers" Value="Dealers"></telerik:RadTab>
                                        <telerik:RadTab Text="Contracts" Value="Contracts"></telerik:RadTab>
                                        <telerik:RadTab Text="Modify" Value="Modify"></telerik:RadTab>
                                    </Tabs>
                                </telerik:RadTabStrip>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadMultiPage ID="rmSubAgent" runat="server">
                                    <telerik:RadPageView ID="pvNote" runat="server">
                                        <uc:SubAgentNote runat="server" />
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="pvDealers" runat="server">
                                        <uc:SubAgentDealer runat="server" />
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="pvContracts" runat="server">
                                        <uc:SubAgentContracts runat="server" />
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="pvModify" runat="server">
                                        <uc:SubAgentModify runat="server" />
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>
            </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>
       
        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfSubAgentID" runat="server" />
    </form>
</body>
</html>
