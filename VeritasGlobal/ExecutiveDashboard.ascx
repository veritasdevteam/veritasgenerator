﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ExecutiveDashboard.ascx.vb" Inherits="VeritasGlobal.ExecutiveDashboard" %>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <asp:Table runat="server">
            <asp:TableRow>
                <asp:TableCell>         
                    <telerik:RadHtmlChart ID="htmlNewClaims" runat="server" Visible="true" Width="400" Height="400" Transitions="false">
                        <PlotArea>
                            <Series>                                                            
                                <telerik:LineSeries DataFieldY ="numOfClaims" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= dataItem.numOfClaims# <br /> #=dataItem.CreateDate#" />
                                </telerik:LineSeries>
                            </Series>
                            <XAxis DataLabelsField="CreateDate">
                                <LabelsAppearance RotationAngle="90" ></LabelsAppearance>
                                <MinorGridLines Visible="false"></MinorGridLines>
                                <MajorGridLines Visible="false"></MajorGridLines>
                            </XAxis>
                            <YAxis >                                                            
                                <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                <MinorGridLines Visible="false"></MinorGridLines>                                                            
                            </YAxis>
                        </PlotArea>
                        <Legend>
                            <Appearance Visible="false"></Appearance>
                        </Legend>
                        <ChartTitle Text="New claims"></ChartTitle>
                    </telerik:RadHtmlChart>  
                    <telerik:RadRadioButtonList runat="server" ID="rblNewClaims" RepeatDirection="Horizontal" Direction="Horizontal">
                            <Items>
                                <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                <telerik:ButtonListItem Text="30 Days"/>
                                <telerik:ButtonListItem Text="90 Days"/>
                                <telerik:ButtonListItem Text="1 Year"/>
                            </Items>
                        </telerik:RadRadioButtonList>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:DropDownList ID="ddlDates" runat="server">
                                    <asp:ListItem Selected="True" Value="All Time">All Time</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Button ID="btnUpdateDates" runat="server" Text="Set Date"/>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <label id="lblClaimOpenCount" runat="server">Claims Currently Open:</label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="txtClaimOpen"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <label id="lblSeverity" runat="server">Paid Severity:</label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="txtSeverity"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <label id="lblClaimsDividedByTotalAuth" runat="server">Authorized Severity:</label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="txtClaimsOverAuth"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
                <asp:TableCell>
                    <telerik:RadGrid ID="rgClaimsPaid" runat="server" AutoGenerateColumns="true" AllowFilteringByColumn="false" AllowSorting="true" ShowFooter="true" ShowHeader="true" AllowPaging="true" PagerStyle-HorizontalAlign="Left" PagerStyle-Position="bottom" >
                        <MasterTableView AutoGenerateColumns="true" PageSize="15" ShowFooter="true">
                        </MasterTableView>
                        <ClientSettings EnablePostBackOnRowClick="true">
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <telerik:RadGrid ID="rgClaimsAuthorized" runat="server" AutoGenerateColumns="true" AllowFilteringByColumn="false" AllowSorting="true" ShowFooter="true" ShowHeader="true" AllowPaging="true" PagerStyle-HorizontalAlign="Left" PagerStyle-Position="bottom" >
                        <MasterTableView AutoGenerateColumns="true" PageSize="15" ShowFooter="true">
                        </MasterTableView>
                        <ClientSettings EnablePostBackOnRowClick="true">
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <telerik:RadRadioButtonList runat="server" ID="rblClaimsInfo" RepeatDirection="Horizontal" Direction="Horizontal">
                            <Items>
                                <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                <telerik:ButtonListItem Text="30 Days"/>
                                <telerik:ButtonListItem Text="90 Days"/>
                                <telerik:ButtonListItem Text="1 Year"/>
                            </Items>
                        </telerik:RadRadioButtonList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <telerik:RadHtmlChart ID="htmlAverageClose" runat="server" Visible="true" Width="400" Height="400" Transitions="false">
                        <PlotArea>
                            <Series>
                                <telerik:LineSeries DataFieldY="average"></telerik:LineSeries>
                            </Series>
                            <XAxis DataLabelsField="closeDates">
                                <LabelsAppearance RotationAngle="90" ></LabelsAppearance>
                                <MinorGridLines Visible="false"></MinorGridLines>
                                <MajorGridLines Visible="false"></MajorGridLines>
                            </XAxis>
                            <YAxis>
                                <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                <MinorGridLines Visible="false"></MinorGridLines>
                            </YAxis>
                        </PlotArea>
                        <Legend>
                            <Appearance Visible="false"></Appearance>
                        </Legend>
                        <ChartTitle Text="Average close"></ChartTitle>
                    </telerik:RadHtmlChart>
                    <telerik:RadRadioButtonList runat="server" ID="rblAverageClose" RepeatDirection="Horizontal" Direction="Horizontal" >
                            <Items>
                                <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                <telerik:ButtonListItem Text="30 Days"/>
                                <telerik:ButtonListItem Text="90 Days"/>
                                <telerik:ButtonListItem Text="1 Year"/>
                            </Items>
                        </telerik:RadRadioButtonList>
                </asp:TableCell>
                <asp:TableCell>
                    <telerik:RadHtmlChart ID="htmlHeadCount" runat="server" Visible="true" Width="400" Height="400" Transitions="false">
                        <PlotArea>
                            <Series>
                                <telerik:LineSeries DataFieldY="numberOfUsers"></telerik:LineSeries>
                            </Series>
                            <XAxis DataLabelsField="date">
                                <LabelsAppearance RotationAngle="90" ></LabelsAppearance>
                                <MinorGridLines Visible="false"></MinorGridLines>
                                <MajorGridLines Visible="false"></MajorGridLines>
                            </XAxis>
                            <YAxis>
                                <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                <MinorGridLines Visible="false"></MinorGridLines>
                            </YAxis>
                        </PlotArea>
                        <Legend>
                            <Appearance Visible="false"></Appearance>
                        </Legend>
                        <ChartTitle Text="Head Count"></ChartTitle>
                    </telerik:RadHtmlChart>
                    <telerik:RadRadioButtonList runat="server" ID="rblHeadCount" RepeatDirection="Horizontal" Direction="Horizontal">
                            <Items>
                                <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                <telerik:ButtonListItem Text="30 Days"/>
                                <telerik:ButtonListItem Text="90 Days"/>
                                <telerik:ButtonListItem Text="1 Year"/>
                            </Items>
                        </telerik:RadRadioButtonList>
                </asp:TableCell>                                           
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <telerik:RadHtmlChart ID="htmlSales" runat="server" Visible="true" Width="400" Height="400" Transitions="false">
                        <PlotArea>
                            <Series>
                                <telerik:ColumnSeries Name="AN" DataFieldY="ANCnt" Stacked="true" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.ANCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.salesDate#" />
                                </telerik:ColumnSeries>
                                <telerik:ColumnSeries Name="Vero" DataFieldY="VeroCnt" Stacked="true" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.VeroCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.salesDate#" />
                                </telerik:ColumnSeries>
                                <telerik:ColumnSeries Name="EP" DataFieldY="EPCnt" Stacked="true" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.EPCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.salesDate#" />
                                </telerik:ColumnSeries>
                                <telerik:ColumnSeries Name="All Others" DataFieldY="OtherCnt" Stacked="true" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.OtherCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.salesDate#" />
                                </telerik:ColumnSeries>                      
                                <telerik:LineSeries Name="Average" DataFieldY ="average" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="Average: #= dataItem.average# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.salesDate#" />
                                </telerik:LineSeries>
                            </Series>
                            <XAxis DataLabelsField="salesDate">
                                <LabelsAppearance RotationAngle="90" ></LabelsAppearance>
                                <MinorGridLines Visible="false"></MinorGridLines>
                                <MajorGridLines Visible="false"></MajorGridLines>
                            </XAxis>
                            <YAxis>
                                <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                <MinorGridLines Visible="false"></MinorGridLines>
                            </YAxis>
                        </PlotArea>
                        <Legend>
                            <Appearance Visible="true"></Appearance>
                        </Legend>
                        <ChartTitle Text="Sales"></ChartTitle>
                    </telerik:RadHtmlChart>
                    <telerik:RadRadioButtonList runat="server" ID="rblSales" RepeatDirection="Horizontal" Direction="Horizontal">
                            <Items>
                                <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                <telerik:ButtonListItem Text="30 Days"/>
                                <telerik:ButtonListItem Text="90 Days"/>
                                <telerik:ButtonListItem Text="1 Year"/>
                            </Items>
                        </telerik:RadRadioButtonList>
                </asp:TableCell>
                <asp:TableCell>
                    <telerik:RadHtmlChart ID="htmlPaid" runat="server" Visible="true" Width="400" Height="400" Transitions="false">
                        <PlotArea>
                            <Series>
                                <telerik:ColumnSeries Name="AN" DataFieldY="ANCnt" Stacked="true" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.ANCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.paidDates#" />
                                </telerik:ColumnSeries>
                                <telerik:ColumnSeries Name="Vero" DataFieldY="VeroCnt" Stacked="true" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.VeroCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.paidDates#" />
                                </telerik:ColumnSeries>
                                <telerik:ColumnSeries Name="EP" DataFieldY="EPCnt" Stacked="true" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.EPCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.paidDates#" />
                                </telerik:ColumnSeries>
                                <telerik:ColumnSeries Name="All Others" DataFieldY="OtherCnt" Stacked="true" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.OtherCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.paidDates#" />
                                </telerik:ColumnSeries>              
                                <telerik:LineSeries Name="Average" DataFieldY ="average" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="Average: #= dataItem.average# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.paidDates#" />
                                </telerik:LineSeries>
                            </Series>
                            <XAxis DataLabelsField="paidDates" >
                                <LabelsAppearance RotationAngle="90" ></LabelsAppearance>
                                <MinorGridLines Visible="false"></MinorGridLines>
                                <MajorGridLines Visible="false"></MajorGridLines>
                            </XAxis>
                            <YAxis>
                                <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                <MinorGridLines Visible="false"></MinorGridLines>
                            </YAxis>
                        </PlotArea>
                        <Legend>
                            <Appearance Visible="true"></Appearance>
                        </Legend>
                        <ChartTitle Text="Paid"></ChartTitle>
                    </telerik:RadHtmlChart>
                    <telerik:RadRadioButtonList runat="server" ID="rblPaid" RepeatDirection="Horizontal" Direction="Horizontal">
                            <Items>
                                <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                <telerik:ButtonListItem Text="30 Days"/>
                                <telerik:ButtonListItem Text="90 Days"/>
                                <telerik:ButtonListItem Text="1 Year"/>
                            </Items>
                        </telerik:RadRadioButtonList>
                </asp:TableCell>
                <asp:TableCell>
                    <telerik:RadHtmlChart ID="htmlCancel" runat="server" Visible="true" Width="400" Height="400" Transitions="false">
                        <PlotArea>
                            <Series>               
                                <telerik:ColumnSeries Name="AN" DataFieldY="ANCnt" Stacked="true" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.ANCnt# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.CancelDate#" />
                                </telerik:ColumnSeries>
                                <telerik:ColumnSeries Name="Vero" DataFieldY="VeroCnt" Stacked="true" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.VeroCnt# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.CancelDate#" />
                                </telerik:ColumnSeries>
                                <telerik:ColumnSeries Name="EP" DataFieldY="EPCnt" Stacked="true" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.EPCnt# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.CancelDate#" />
                                </telerik:ColumnSeries>
                                <telerik:ColumnSeries Name="All Others" DataFieldY="OtherCnt" Stacked="true" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.OtherCnt# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.CancelDate#" />
                                </telerik:ColumnSeries>         
                                <telerik:LineSeries Name="Average" DataFieldY ="average" >
                                    <LabelsAppearance Visible="false"></LabelsAppearance>
                                    <TooltipsAppearance ClientTemplate="Average: #= dataItem.average# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.CancelDate#" />
                                </telerik:LineSeries>
                            </Series>
                            <XAxis DataLabelsField="CancelDate">
                                <LabelsAppearance RotationAngle="90" ></LabelsAppearance>
                                <MinorGridLines Visible="false"></MinorGridLines>
                                <MajorGridLines Visible="false"></MajorGridLines>
                            </XAxis>
                            <YAxis >
                                <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                <MinorGridLines Visible="false"></MinorGridLines>
                            </YAxis>
                        </PlotArea>
                        <Legend>
                            <Appearance Visible="true"></Appearance>
                        </Legend>
                        <ChartTitle Text="Cancels"></ChartTitle>
                    </telerik:RadHtmlChart>
                    <telerik:RadRadioButtonList runat="server" ID="rblCancel" RepeatDirection="Horizontal" Direction="Horizontal">
                            <Items>
                                <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                <telerik:ButtonListItem Text="30 Days"/>
                                <telerik:ButtonListItem Text="90 Days"/>
                                <telerik:ButtonListItem Text="1 Year"/>
                            </Items>
                        </telerik:RadRadioButtonList>
                </asp:TableCell>
            </asp:TableRow>                                        
        </asp:Table>
        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfID" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>