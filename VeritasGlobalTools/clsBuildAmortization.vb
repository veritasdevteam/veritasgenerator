﻿

Public Class clsBuildAmortization
    Private lClaimGapID As Long
    Private dPaymentAmt As Double
    Private dRatePerMonth As Double
    Private dLoanAmt As Double
    Private dLoanAmtInt As Double
    Private lNoPayment As Long
    Private sStartDate As String
    Private bMonthly As Boolean
    Private bBiWeekly As Boolean

    Public Property ClaimGapID As Long
        Get
            ClaimGapID = lClaimGapID
        End Get
        Set(value As Long)
            lClaimGapID = value
        End Set
    End Property

    Public Property LoanAmt As Double
        Get
            LoanAmt = dLoanAmt
        End Get
        Set(value As Double)
            dLoanAmt = value
        End Set
    End Property

    Public Property LoanAmtInt As Double
        Get
            LoanAmtInt = dLoanAmtInt
        End Get
        Set(value As Double)
            dLoanAmtInt = value
        End Set
    End Property

    Public Property StartDate As String
        Get
            StartDate = sStartDate
        End Get
        Set(value As String)
            sStartDate = value
        End Set
    End Property

    Public Property NoPayment As Long
        Get
            NoPayment = lNoPayment
        End Get
        Set(value As Long)
            lNoPayment = value
        End Set
    End Property

    Public Property PaymentAmt As Double
        Get
            PaymentAmt = dPaymentAmt
        End Get
        Set(value As Double)
            dPaymentAmt = value
        End Set
    End Property

    Public Property RatePerMonth As Double
        Get
            RatePerMonth = dRatePerMonth
        End Get
        Set(value As Double)
            dRatePerMonth = value
        End Set
    End Property

    Public Property Monthly As Boolean
        Get
            Monthly = bMonthly
        End Get
        Set(value As Boolean)
            bMonthly = value
        End Set
    End Property

    Public Property BiWeekly As Boolean
        Get
            BiWeekly = bBiWeekly
        End Get
        Set(value As Boolean)
            bBiWeekly = value
        End Set
    End Property

    Public Sub BuildIt()
        Dim cnt As Long
        Dim SQL As String
        Dim clR As New clsDBO
        Dim sMonth As String
        Dim dOldBalance As Double
        Dim dOldBalanceInt As Double
        Dim dTotInt As Double = 0
        dOldBalanceInt = dLoanAmtInt
        dOldBalance = dLoanAmt
        For cnt = 1 To lNoPayment
            SQL = "select * from claimgapamortization "
            SQL = SQL + "where claimgapid = " & lClaimGapID & " "
            SQL = SQL + "and paymentno = " & cnt
            clR.OpenDB(SQL, sCON)
            If clR.RowCount = 0 Then
                clR.NewRow()
                clR.Fields("claimgapid") = lClaimGapID
                clR.Fields("paymentno") = cnt
            Else
                clR.GetRow()
            End If
            If bMonthly Then
                sMonth = DateAdd(DateInterval.Month, cnt - 1, CDate(sStartDate))
            End If
            If bBiWeekly Then
                sMonth = DateAdd(DateInterval.Day, (cnt - 1) * 14, CDate(sStartDate))
            End If


            clR.Fields("monthdate") = sMonth
            clR.Fields("payment") = dPaymentAmt
            clR.Fields("interest") = Format(dOldBalance * dRatePerMonth, "#.00")
            clR.Fields("principal") = dPaymentAmt - CDbl(clR.Fields("interest"))
            dTotInt = dTotInt + CDbl(clR.Fields("interest"))
            clR.Fields("totalinterest") = dTotInt
            dOldBalance = dOldBalance - CDbl(clR.Fields("principal"))
            clR.Fields("balremaining") = dOldBalance
            dOldBalanceInt = dOldBalanceInt - dPaymentAmt
            clR.Fields("loanremaining") = dOldBalanceInt
            If clR.RowCount = 0 Then
                clR.AddRow()
            End If
            clR.SaveDB()
        Next
    End Sub
End Class
