﻿Imports System.IO

Public Class clsWISZipCode
    Private AuthUser As New WIS.AuthenticateHeader
    Private sUserName As String = "redshield"
    Private sPassword As String = "rs@@p120gh"
    Private dAvg As Double
    Private dHigh As Double
    Private dLow As Double
    Dim clZ As New clsDBO
    Dim clR As New clsDBO
    Dim SQL As String
    Private sZip As String

    Public Property ZipCode As String
        Get
            ZipCode = sZip
        End Get
        Set(value As String)
            sZip = value
        End Set
    End Property

    Public Sub RunZip()
        dAvg = 0
        dHigh = 0
        dLow = 0
        Try
            AuthUser.Username = sUserName
            AuthUser.Password = sPassword
            Dim Service As New WIS.wiswebserviceSoapClient
            Dim ZipRequest As New WIS.LaborRatesByZipRequest
            ZipRequest.AuthenticateHeader = AuthUser
            ZipRequest.Term = 12
            ZipRequest.Zip = sZip
            Dim ZipRep As New WIS.LaborRatesByZipResponse
            ZipRep = Service.WIS_wiswebserviceSoap_LaborRatesByZip(ZipRequest)
            Dim ds As DataSet
            ds = ZipRep.LaborRatesByZipResult
            Dim dt As DataTable
            dt = ds.Tables(0)
            Dim dr As DataRow
            dr = dt.Rows(0)
            If Not IsDBNull(dr(1)) Then
                dAvg = dr(1)
            End If
            If Not IsDBNull(dr(2)) Then
                dHigh = dr(2)
            End If
            If Not IsDBNull(dr(3)) Then
                dLow = dr(3)
            End If
            SQL = "select * from laborrate "
            SQL = SQL + "where zipcode = '" & sZip & "' "
            clR.OpenDB(SQL, sCON)
            If clR.RowCount = 0 Then
                clR.NewRow()
                clR.Fields("zipcode") = sZip
                clR.Fields("avgrate") = dAvg
                clR.Fields("maxrate") = dHigh
                clR.Fields("minRate") = dLow
                clR.Fields("credate") = Now
                clR.AddRow()
            Else
                clR.GetRow()
                clR.Fields("avgrate") = dAvg
                clR.Fields("maxrate") = dHigh
                clR.Fields("minRate") = dLow
                clR.Fields("credate") = Now
            End If
            clR.SaveDB()
        Catch ex As Exception

        End Try

    End Sub
End Class
