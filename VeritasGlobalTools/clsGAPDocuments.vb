﻿Imports Microsoft.Office.Interop

Public Class clsGAPDocuments
    Private lClaimGapID As Long
    Private sDirLoc As String
    Private clCL As New clsDBO
    Private clC As New clsDBO
    Private sFileName As String
    Private lUserID As Long
    Private clL As New clsDBO
    Private clGC As New clsDBO

    Public Property UserID As Long
        Get
            UserID = lUserID
        End Get
        Set(value As Long)
            lUserID = value
        End Set
    End Property

    Public Property DirLoc As String
        Get
            DirLoc = sDirLoc
        End Get
        Set(value As String)
            sDirLoc = value
        End Set
    End Property

    Public Property ClaimGapID As Long
        Get
            ClaimGapID = lClaimGapID
        End Get
        Set(value As Long)
            lClaimGapID = value
        End Set
    End Property

    Public Sub CreateGAPClaimNotice()
        Dim app As New Word.Application
        Dim doc As Word.Document
        Dim lDocNum As Long = 1
        doc = app.Documents.Open(sDirLoc & "GAPDocs\GAPClaimNotice.docx")
        GetClaim()
        If clCL.RowCount > 0 Then
            GetContract()
            If clC.RowCount > 0 Then
                doc.Bookmarks("HolderName").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
                doc.Bookmarks("HolderName2").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
                doc.Bookmarks("addr1").Range.Text = clC.Fields("addr1")
                If clC.Fields("addr2").Length > 0 Then
                    doc.Bookmarks("addr2").Range.Text = clC.Fields("addr2")
                    doc.Bookmarks("addr3").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
                Else
                    doc.Bookmarks("addr2").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
                    doc.Bookmarks("addr3").Range.Text = ""
                End If
                doc.Bookmarks("VIN").Range.Text = clC.Fields("vin")
            End If
            doc.Bookmarks("ClaimNo1").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("ClaimNo2").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("ClaimNo3").Range.Text = clCL.Fields("claimno")
            sFileName = "documents\gap\" & clCL.Fields("claimno") & "-ClaimNotice.docx"
        End If
        doc.Bookmarks("VeritasAddr").Range.Text = "3550 N Central Ave, STE 800, Phoenix, AZ, 85012"
        doc.Bookmarks("Fax").Range.Text = "913-904-3450"
        doc.Bookmarks("TodayDate").Range.Text = Today
        doc.Bookmarks("GapPhone").Range.Text = "888-572-4310"
        doc.Bookmarks("EMail").Range.Text = GetUserEMail()
        doc.Bookmarks("EMail2").Range.Text = GetUserEMail()

MoveHere:
        If System.IO.File.Exists("sDirLoc & sFileName") Then
            lDocNum = lDocNum + 1
            sFileName = "documents\gap\" & clCL.Fields("claimno") & lDocNum & "-ClaimNotice.docx"
            GoTo MoveHere
        End If
        doc.SaveAs2(sDirLoc & sFileName)
        AddDocument(7)
        adddocstatus(7)
    End Sub

    Public Sub CreateGAP60DayLetter()
        Dim app As New Word.Application
        Dim doc As Word.Document
        Dim lDocNum As Long = 1
        doc = app.Documents.Open(sDirLoc & "GAPDocs\GAP60DayLetter.docx")
        GetClaim()
        GetContract()
        If clC.RowCount > 0 Then
            doc.Bookmarks("HolderName").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
            doc.Bookmarks("HolderName2").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
            doc.Bookmarks("addr1").Range.Text = clC.Fields("addr1")
            If clC.Fields("addr2").Length > 0 Then
                doc.Bookmarks("addr2").Range.Text = clC.Fields("addr2")
                doc.Bookmarks("addr3").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
            Else
                doc.Bookmarks("addr2").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
                doc.Bookmarks("addr3").Range.Text = ""
            End If
            doc.Bookmarks("VIN").Range.Text = clC.Fields("vin")
            doc.Bookmarks("ClaimNo1").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("ClaimNo2").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("ClaimNo3").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("LossMile").Range.Text = clCL.Fields("lossmile")
            doc.Bookmarks("LossDate").Range.Text = clCL.Fields("lossdate")
            sFileName = "documents\gap\" & clCL.Fields("claimno") & "-GAP60DayLetter.docx"
        End If
        doc.Bookmarks("VeritasAddr").Range.Text = "3550 N Central Ave, STE 800, Phoenix, AZ, 85012"
        doc.Bookmarks("Fax").Range.Text = "913-904-3450"
        doc.Bookmarks("TodayDate").Range.Text = Today
        doc.Bookmarks("GapPhone").Range.Text = "888-285-5950"
        doc.Bookmarks("EMail").Range.Text = GetUserEMail()
        doc.Bookmarks("EMail2").Range.Text = GetUserEMail()
        OpenLoanInfo()
        If clL.RowCount > 0 Then
            doc.Bookmarks("BankName").Range.Text = clL.Fields("bankname")
            doc.Bookmarks("BankAddr1").Range.Text = clL.Fields("bankaddr1")
            If clL.Fields("bankaddr2").Length > 0 Then
                doc.Bookmarks("BankAddr2").Range.Text = clL.Fields("bankaddr2")
                doc.Bookmarks("BankAddr3").Range.Text = clL.Fields("city") & " " & clL.Fields("state") & " " & clL.Fields("zip")
            Else
                doc.Bookmarks("BankAddr2").Range.Text = clL.Fields("city") & " " & clL.Fields("state") & " " & clL.Fields("zip")
                doc.Bookmarks("BankAddr3").Range.Text = ""
            End If
        End If
MoveHere:
        If System.IO.File.Exists("sDirLoc & sFileName") Then
            lDocNum = lDocNum + 1
            sFileName = "documents\gap\" & clCL.Fields("claimno") & lDocNum & "-GAP60DayLetter.docx"
            GoTo MoveHere
        End If
        doc.SaveAs2(sDirLoc & sFileName)
        AddDocument(8)
        AddDocStatus(8)

    End Sub

    Public Sub CreateDenialLetter()
        Dim app As New Word.Application
        Dim doc As Word.Document
        Dim lDocNum As Long = 1
        doc = app.Documents.Open(sDirLoc & "GAPDocs\GAPDenialLetter.docx")
        GetClaim()
        GetContract()
        If clC.RowCount > 0 Then
            doc.Bookmarks("HolderName").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
            doc.Bookmarks("HolderName2").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
            doc.Bookmarks("addr1").Range.Text = clC.Fields("addr1")
            If clC.Fields("addr2").Length > 0 Then
                doc.Bookmarks("addr2").Range.Text = clC.Fields("addr2")
                doc.Bookmarks("addr3").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
            Else
                doc.Bookmarks("addr2").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
                doc.Bookmarks("addr3").Range.Text = ""
            End If
            doc.Bookmarks("VIN").Range.Text = clC.Fields("vin")
            doc.Bookmarks("ClaimNo1").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("ClaimNo2").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("LossMile").Range.Text = clCL.Fields("lossmile")
            doc.Bookmarks("LossDate").Range.Text = clCL.Fields("lossdate")
            sFileName = "documents\gap\" & clCL.Fields("claimno") & "-GAPDenialLetter.docx"
        End If
        doc.Bookmarks("TodayDate").Range.Text = Today
        doc.Bookmarks("GapPhone").Range.Text = "888-285-5950"
        doc.Bookmarks("EMail").Range.Text = GetUserEMail()
        OpenLoanInfo()
        If clL.RowCount > 0 Then
            doc.Bookmarks("BankName").Range.Text = clL.Fields("bankname")
            doc.Bookmarks("BankAddr1").Range.Text = clL.Fields("bankaddr1")
            If clL.Fields("bankaddr2").Length > 0 Then
                doc.Bookmarks("BankAddr2").Range.Text = clL.Fields("bankaddr2")
                doc.Bookmarks("BankAddr3").Range.Text = clL.Fields("city") & " " & clL.Fields("state") & " " & clL.Fields("zip")
            Else
                doc.Bookmarks("BankAddr2").Range.Text = clL.Fields("city") & " " & clL.Fields("state") & " " & clL.Fields("zip")
                doc.Bookmarks("BankAddr3").Range.Text = ""
            End If
        End If
MoveHere:
        If System.IO.File.Exists("sDirLoc & sFileName") Then
            lDocNum = lDocNum + 1
            sFileName = "documents\gap\" & clCL.Fields("claimno") & lDocNum & "-GAPDenialLetter.docx"
            GoTo MoveHere
        End If
        doc.SaveAs2(sDirLoc & sFileName)
        AddDocument(9)
        AddDocStatus(9)
    End Sub

    Public Sub CreateMissedOptions()
        Dim app As New Word.Application
        Dim doc As Word.Document
        Dim lDocNum As Long = 1
        doc = app.Documents.Open(sDirLoc & "GAPDocs\GAPMissedOptions.docx")
        GetClaim()
        GetContract()
        If clC.RowCount > 0 Then
            doc.Bookmarks("HolderName").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
            doc.Bookmarks("HolderName2").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
            doc.Bookmarks("addr1").Range.Text = clC.Fields("addr1")
            If clC.Fields("addr2").Length > 0 Then
                doc.Bookmarks("addr2").Range.Text = clC.Fields("addr2")
                doc.Bookmarks("addr3").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
            Else
                doc.Bookmarks("addr2").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
                doc.Bookmarks("addr3").Range.Text = ""
            End If
            doc.Bookmarks("VIN").Range.Text = clC.Fields("vin")
            doc.Bookmarks("ClaimNo1").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("ClaimNo2").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("ClaimNo3").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("LossMile").Range.Text = clCL.Fields("lossmile")
            doc.Bookmarks("LossDate").Range.Text = clCL.Fields("lossdate")
            sFileName = "documents\gap\" & clCL.Fields("claimno") & "-GAPMissedOptions.docx"
        End If
        doc.Bookmarks("VeritasAddr").Range.Text = "3550 N Central Ave, STE 800, Phoenix, AZ, 85012"
        doc.Bookmarks("Fax").Range.Text = "913-904-3450"
        doc.Bookmarks("TodayDate").Range.Text = Today
        doc.Bookmarks("GapPhone").Range.Text = "888-285-5950"
        doc.Bookmarks("EMail").Range.Text = GetUserEMail()
        OpenLoanInfo()
        If clL.RowCount > 0 Then
            doc.Bookmarks("BankName").Range.Text = clL.Fields("bankname")
            doc.Bookmarks("BankAddr1").Range.Text = clL.Fields("bankaddr1")
            If clL.Fields("bankaddr2").Length > 0 Then
                doc.Bookmarks("BankAddr2").Range.Text = clL.Fields("bankaddr2")
                doc.Bookmarks("BankAddr3").Range.Text = clL.Fields("city") & " " & clL.Fields("state") & " " & clL.Fields("zip")
            Else
                doc.Bookmarks("BankAddr2").Range.Text = clL.Fields("city") & " " & clL.Fields("state") & " " & clL.Fields("zip")
                doc.Bookmarks("BankAddr3").Range.Text = ""
            End If
        End If
MoveHere:
        If System.IO.File.Exists("sDirLoc & sFileName") Then
            lDocNum = lDocNum + 1
            sFileName = "documents\gap\" & clCL.Fields("claimno") & lDocNum & "-GAPMissedOptions.docx"
            GoTo MoveHere
        End If
        doc.SaveAs2(sDirLoc & sFileName)
        AddDocument(10)
        AddDocStatus(10)
    End Sub

    Public Sub CreateGAPNoGAPDueLetter()
        Dim app As New Word.Application
        Dim doc As Word.Document
        Dim lDocNum As Long = 1
        doc = app.Documents.Open(sDirLoc & "GAPDocs\GAPNoGAPDueLetter.docx")
        GetClaim()
        GetContract()
        If clC.RowCount > 0 Then
            doc.Bookmarks("HolderName").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
            doc.Bookmarks("HolderName2").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
            doc.Bookmarks("addr1").Range.Text = clC.Fields("addr1")
            If clC.Fields("addr2").Length > 0 Then
                doc.Bookmarks("addr2").Range.Text = clC.Fields("addr2")
                doc.Bookmarks("addr3").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
            Else
                doc.Bookmarks("addr2").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
                doc.Bookmarks("addr3").Range.Text = ""
            End If
            doc.Bookmarks("VIN").Range.Text = clC.Fields("vin")
            doc.Bookmarks("ClaimNo1").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("LossMile").Range.Text = clCL.Fields("lossmile")
            doc.Bookmarks("LossDate").Range.Text = clCL.Fields("lossdate")
            sFileName = "documents\gap\" & clCL.Fields("claimno") & "-GAPNoGAPDueLetter.docx"
        End If
        doc.Bookmarks("TodayDate").Range.Text = Today
        doc.Bookmarks("GapPhone").Range.Text = "888-285-5950"
        doc.Bookmarks("EMail").Range.Text = GetUserEMail()
        OpenLoanInfo()
        If clL.RowCount > 0 Then
            doc.Bookmarks("BankName").Range.Text = clL.Fields("bankname")
            doc.Bookmarks("BankName2").Range.Text = clL.Fields("bankname")
            doc.Bookmarks("BankAddr1").Range.Text = clL.Fields("bankaddr1")
            If clL.Fields("bankaddr2").Length > 0 Then
                doc.Bookmarks("BankAddr2").Range.Text = clL.Fields("bankaddr2")
                doc.Bookmarks("BankAddr3").Range.Text = clL.Fields("city") & " " & clL.Fields("state") & " " & clL.Fields("zip")
            Else
                doc.Bookmarks("BankAddr2").Range.Text = clL.Fields("city") & " " & clL.Fields("state") & " " & clL.Fields("zip")
                doc.Bookmarks("BankAddr3").Range.Text = ""
            End If
        End If
        OpenGapCalc()
        If clGC.RowCount > 0 Then
            doc.Bookmarks("ScheduleLoanBalance").Range.Text = clGC.Fields("ScheduleLoanBalance")
            doc.Bookmarks("InterestEarned").Range.Text = clGC.Fields("InterestEarned")
            doc.Bookmarks("MechanicalContractRefunds").Range.Text = clGC.Fields("MechanicalContractRefunds")
            doc.Bookmarks("CreditInsRefunds").Range.Text = clGC.Fields("CreditInsRefunds")
            doc.Bookmarks("ACV").Range.Text = clGC.Fields("ACV")
            doc.Bookmarks("OwnerSalvage").Range.Text = clGC.Fields("OwnerSalvage")
            doc.Bookmarks("NADALoss").Range.Text = clGC.Fields("NADALoss")
            doc.Bookmarks("AmountExceding150").Range.Text = clGC.Fields("AmountExceding150")
            doc.Bookmarks("UnearnedGapRefund").Range.Text = clGC.Fields("UnearnedGapRefund")
            doc.Bookmarks("InsDeduct").Range.Text = clGC.Fields("InsDeduct")
            doc.Bookmarks("OtherDeduct").Range.Text = clGC.Fields("OtherDeduct")
            doc.Bookmarks("GapPayout").Range.Text = clGC.Fields("GapPayout")
        End If
MoveHere:
        If System.IO.File.Exists("sDirLoc & sFileName") Then
            lDocNum = lDocNum + 1
            sFileName = "documents\gap\" & clCL.Fields("claimno") & lDocNum & "-GAPNoGAPDueLetter.docx"
            GoTo MoveHere
        End If
        doc.SaveAs2(sDirLoc & sFileName)
        AddDocument(11)
        AddDocStatus(11)

    End Sub

    Public Sub CreatePaymentLetter()
        Dim app As New Word.Application
        Dim doc As Word.Document
        Dim lDocNum As Long = 1
        doc = app.Documents.Open(sDirLoc & "GAPDocs\GAPPaymentLetter.docx")
        GetClaim()
        GetContract()
        If clC.RowCount > 0 Then
            doc.Bookmarks("HolderName").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
            doc.Bookmarks("HolderName2").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
            doc.Bookmarks("addr1").Range.Text = clC.Fields("addr1")
            If clC.Fields("addr2").Length > 0 Then
                doc.Bookmarks("addr2").Range.Text = clC.Fields("addr2")
                doc.Bookmarks("addr3").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
            Else
                doc.Bookmarks("addr2").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
                doc.Bookmarks("addr3").Range.Text = ""
            End If
            doc.Bookmarks("VIN").Range.Text = clC.Fields("vin")
            doc.Bookmarks("ClaimNo1").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("LossMile").Range.Text = clCL.Fields("lossmile")
            doc.Bookmarks("LossDate").Range.Text = clCL.Fields("lossdate")
            sFileName = "documents\gap\" & clCL.Fields("claimno") & "-GAPPaymentLetter.docx"
        End If
        doc.Bookmarks("TodayDate").Range.Text = Today
        doc.Bookmarks("GapPhone").Range.Text = "888-285-5950"
        doc.Bookmarks("EMail").Range.Text = GetUserEMail()
        OpenLoanInfo()
        If clL.RowCount > 0 Then
            doc.Bookmarks("BankName").Range.Text = clL.Fields("bankname")
            doc.Bookmarks("BankName2").Range.Text = clL.Fields("bankname")
            doc.Bookmarks("BankAddr1").Range.Text = clL.Fields("bankaddr1")
            If clL.Fields("bankaddr2").Length > 0 Then
                doc.Bookmarks("BankAddr2").Range.Text = clL.Fields("bankaddr2")
                doc.Bookmarks("BankAddr3").Range.Text = clL.Fields("city") & " " & clL.Fields("state") & " " & clL.Fields("zip")
            Else
                doc.Bookmarks("BankAddr2").Range.Text = clL.Fields("city") & " " & clL.Fields("state") & " " & clL.Fields("zip")
                doc.Bookmarks("BankAddr3").Range.Text = ""
            End If
        End If
        OpenGapCalc()
        If clGC.RowCount > 0 Then
            doc.Bookmarks("ScheduleLoanBalance").Range.Text = clGC.Fields("ScheduleLoanBalance")
            doc.Bookmarks("InterestEarned").Range.Text = clGC.Fields("InterestEarned")
            doc.Bookmarks("MechanicalContractRefunds").Range.Text = clGC.Fields("MechanicalContractRefunds")
            doc.Bookmarks("CreditInsRefunds").Range.Text = clGC.Fields("CreditInsRefunds")
            doc.Bookmarks("ACV").Range.Text = clGC.Fields("ACV")
            doc.Bookmarks("OwnerSalvage").Range.Text = clGC.Fields("OwnerSalvage")
            doc.Bookmarks("NADALoss").Range.Text = clGC.Fields("NADALoss")
            doc.Bookmarks("AmountExceding150").Range.Text = clGC.Fields("AmountExceding150")
            doc.Bookmarks("UnearnedGapRefund").Range.Text = clGC.Fields("UnearnedGapRefund")
            doc.Bookmarks("InsDeduct").Range.Text = clGC.Fields("InsDeduct")
            doc.Bookmarks("OtherDeduct").Range.Text = clGC.Fields("OtherDeduct")
            doc.Bookmarks("GapPayout").Range.Text = clGC.Fields("GapPayout")
        End If
MoveHere:
        If System.IO.File.Exists("sDirLoc & sFileName") Then
            lDocNum = lDocNum + 1
            sFileName = "documents\gap\" & clCL.Fields("claimno") & lDocNum & "-GAPPaymentLetter.docx"
            GoTo MoveHere
        End If
        doc.SaveAs2(sDirLoc & sFileName)
        AddDocument(12)
        AddDocStatus(12)
    End Sub

    Public Sub CreateStatusLetter()
        Dim app As New Word.Application
        Dim doc As Word.Document
        Dim lDocNum As Long = 1
        doc = app.Documents.Open(sDirLoc & "GAPDocs\GAP60DayLetter.docx")
        GetClaim()
        GetContract()
        If clC.RowCount > 0 Then
            doc.Bookmarks("HolderName").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
            doc.Bookmarks("HolderName2").Range.Text = clC.Fields("fname") & " " & clC.Fields("lname")
            doc.Bookmarks("addr1").Range.Text = clC.Fields("addr1")
            If clC.Fields("addr2").Length > 0 Then
                doc.Bookmarks("addr2").Range.Text = clC.Fields("addr2")
                doc.Bookmarks("addr3").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
            Else
                doc.Bookmarks("addr2").Range.Text = clC.Fields("city") & " " & clC.Fields("state") & " " & clC.Fields("zip")
                doc.Bookmarks("addr3").Range.Text = ""
            End If
            doc.Bookmarks("VIN").Range.Text = clC.Fields("vin")
            doc.Bookmarks("ClaimNo1").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("ClaimNo2").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("ClaimNo3").Range.Text = clCL.Fields("claimno")
            doc.Bookmarks("LossMile").Range.Text = clCL.Fields("lossmile")
            doc.Bookmarks("LossDate").Range.Text = clCL.Fields("lossdate")
            sFileName = "documents\gap\" & clCL.Fields("claimno") & "-GAP60DayLetter.docx"
        End If
        doc.Bookmarks("VeritasAddr").Range.Text = "3550 N Central Ave, STE 800, Phoenix, AZ, 85012"
        doc.Bookmarks("Fax").Range.Text = "913-904-3450"
        doc.Bookmarks("TodayDate").Range.Text = Today
        doc.Bookmarks("GapPhone").Range.Text = "888-285-5950"
        doc.Bookmarks("GapPhone2").Range.Text = "888-285-5950"
        doc.Bookmarks("EMail").Range.Text = GetUserEMail()
        doc.Bookmarks("EMail2").Range.Text = GetUserEMail()
        OpenLoanInfo()
        If clL.RowCount > 0 Then
            doc.Bookmarks("BankName").Range.Text = clL.Fields("bankname")
            doc.Bookmarks("BankAddr1").Range.Text = clL.Fields("bankaddr1")
            If clL.Fields("bankaddr2").Length > 0 Then
                doc.Bookmarks("BankAddr2").Range.Text = clL.Fields("bankaddr2")
                doc.Bookmarks("BankAddr3").Range.Text = clL.Fields("city") & " " & clL.Fields("state") & " " & clL.Fields("zip")
            Else
                doc.Bookmarks("BankAddr2").Range.Text = clL.Fields("city") & " " & clL.Fields("state") & " " & clL.Fields("zip")
                doc.Bookmarks("BankAddr3").Range.Text = ""
            End If
        End If
MoveHere:
        If System.IO.File.Exists("sDirLoc & sFileName") Then
            lDocNum = lDocNum + 1
            sFileName = "documents\gap\" & clCL.Fields("claimno") & lDocNum & "-GAP60DayLetter.docx"
            GoTo MoveHere
        End If
        doc.SaveAs2(sDirLoc & sFileName)
        AddDocument(13)
        AddDocStatus(13)
    End Sub

    Private Sub AddDocStatus(xDocType As Long)
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgapdocstatus "
        SQL = SQL + "where claimgapid = " & lClaimGapID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimgapid") = lClaimGapID
        Else
            clR.GetRow()
        End If
        If xDocType = 7 Then
            clR.Fields("gapclaimnotice") = True
        End If
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
    End Sub

    Private Sub AddDocument(xDocType As Long)
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgapdocument "
        SQL = SQL + "where claimgapid = " & lClaimGapID
        clR.OpenDB(SQL, sCON)
        clR.NewRow()
        clR.Fields("claimgapid") = lClaimGapID
        clR.Fields("claimgapdoctypeid") = xDocType
        If xDocType = 7 Then
            clR.Fields("DocumentName") = "Claim Notice"
            clR.Fields("documentdesc") = "Claim Notice"
        End If
        clR.Fields("documentlink") = "~\" & sFileName
        clR.Fields("creby") = lUserID
        clR.Fields("credate") = Today
        clR.AddRow()
        clR.SaveDB()
    End Sub

    Private Sub OpenGapCalc()
        Dim SQL As String
        SQL = "select * from claimgapcalc "
        SQL = SQL + "where claimgapid = " & lClaimGapID
        clGC.OpenDB(SQL, sCON)
        If clGC.RowCount > 0 Then
            clGC.GetRow()
        End If
    End Sub

    Private Sub OpenLoanInfo()
        Dim SQL As String
        SQL = "select * from claimgaploaninfo "
        SQL = SQL + "where claimgapid = " & lClaimGapID
        clL.OpenDB(SQL, sCON)
        If clL.RowCount > 0 Then
            clL.GetRow()
        End If
    End Sub

    Private Sub GetClaim()
        Dim SQL As String
        SQL = "select * from claimgap "
        SQL = SQL + "where claimgapid = " & lClaimGapID
        clCL.OpenDB(SQL, sCON)
        If clCL.RowCount > 0 Then
            clCL.GetRow()
        End If
    End Sub

    Private Sub GetContract()
        Dim SQL As String
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & clCL.Fields("contractid")
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
        End If
    End Sub

    Private Function GetUserEMail() As String
        Dim SQL As String
        Dim clR As New clsDBO
        GetUserEMail = ""
        SQL = "select email from userinfo "
        SQL = SQL + "where userid = " & lUserID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetUserEMail = clR.Fields("email")
        End If
    End Function
End Class
