﻿
Public Class ForteParts
    Private sPartNo As String
    Private sVIN As String
    Private dRetailAmt As Double
    Private sPartDesc As String


    Public Property PartNo As String
        Get
            PartNo = sPartNo
        End Get
        Set(value As String)
            sPartNo = value
        End Set
    End Property

    Public Property VIN As String
        Get
            VIN = sVIN
        End Get
        Set(value As String)
            sVIN = value
        End Set
    End Property

    Public Property RetailAmt As Double
        Get
            RetailAmt = dRetailAmt
        End Get
        Set(value As Double)
            dRetailAmt = value
        End Set
    End Property

    Public Property PartDesc As String
        Get
            PartDesc = sPartDesc
        End Get
        Set(value As String)
            sPartDesc = value
        End Set
    End Property

End Class
