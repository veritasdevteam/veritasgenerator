﻿Public Class clsClaimDetail
    Private SQL As String
    Private bRefresh As Boolean

    Public ReadOnly Property Refresh As Boolean
        Get
            Refresh = bRefresh
        End Get
    End Property

    Public Sub ProcessClaim(xClaimID As Long)
        Dim sStatus As String
        Dim clR As New clsDBO
        bRefresh = False
        sStatus = "Closed"
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailstatus = 'Authorized' "
        SQL = SQL + "and claimid = " & xClaimID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            sStatus = "Adjudicated"
        End If
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailstatus = 'Approved' "
        SQL = SQL + "and claimid = " & xClaimID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            sStatus = "Adjudicated"
        End If
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailstatus = 'Transmitted' "
        SQL = SQL + "and claimid = " & xClaimID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            sStatus = "Adjudicated"
        End If
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailstatus = 'Requested' "
        SQL = SQL + "and claimid = " & xClaimID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            sStatus = "Open"
        End If

        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & xClaimID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("status") <> sStatus Then
                bRefresh = True
            End If
        End If
        SQL = "update claim "
        SQL = SQL + "set status = '" & sStatus & "' "
        SQL = SQL + "where claimid = " & xClaimID
        clR.RunSQL(SQL, sCON)
        If sStatus = "Closed" Then
            SQL = "select * from claimdetail "
            SQL = SQL + "where claimdetailstatus = 'Paid' "
            SQL = SQL + "and claimid = " & xClaimID
            clR.OpenDB(SQL, sCON)
            If clR.RowCount > 0 Then
                SQL = "update claim "
                SQL = SQL + "set claimactivityid = 56 "
                SQL = SQL + "where claimid = " & xClaimID
                clR.RunSQL(SQL, sCON)
            End If
        End If
    End Sub

End Class
