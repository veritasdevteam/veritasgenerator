﻿Public Class clsClaimAssign
    Private SQL As String

    Public Function CheckUserInfo(xUserID As Long) As Boolean
        Dim clUI As New clsDBO
        CheckUserInfo = False
        SQL = "select * from UserInfo "
        SQL = SQL + "where userid in (select UserID from UserSecurityInfo where teamid in ( "
        SQL = SQL + "select TeamID from UserTeam where AgentDash <> 0)) "
        SQL = SQL + "and userid = " & xUserID & " "
        SQL = SQL + "and active <> 0 "
        clUI.OpenDB(SQL, sCON)
        If clUI.RowCount > 0 Then
            CheckUserInfo = True
        End If
    End Function

    Public Function CheckClaimUnassigned() As Long
        Dim clR As New clsDBO
        CheckClaimUnassigned = 0
        SQL = "select * from claim
        where AssignedTo = 0
        and CreDate > '8/1/2022' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            CheckClaimUnassigned = clR.Fields("claimid")
        End If
    End Function

    Public Function CheckClaimUnassigned2(xClaimID As Long) As Long
        Dim clR As New clsDBO
        CheckClaimUnassigned2 = 0
        SQL = "select * from claim
        where AssignedTo = 0
        and CreDate > '8/1/2022' "
        SQL = SQL + "and claimid = " & xClaimID & " "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            CheckClaimUnassigned2 = clR.Fields("claimid")
        End If
    End Function

    Public Function IsContractAN(xClaimID As Long) As Boolean
        IsContractAN = False
        Dim clR As New clsDBO
        SQL = "select * from contract c "
        SQL = SQL + "inner join dealer d on d.dealerid = c.dealerid "
        SQL = SQL + "inner join claim cl on cl.contractid = c.contractid "
        SQL = SQL + "where cl.claimid = " & xClaimID & " "
        SQL = SQL + "and dealerno like '2%' "
        SQL = SQL + "and not dealerno like '%cc' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            IsContractAN = True
        End If
    End Function

    Public Function CheckANUser(xUserID As Long) As Boolean
        CheckANUser = False
        Dim clR As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where userid in ("
        SQL = SQL + "select userid from usersecurityinfo where teamid = 2) "
        SQL = SQL + "and userid = " & xUserID & " "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            CheckANUser = True
        End If
    End Function

    Public Function CheckNonANUser(xUserID As Long) As Boolean
        CheckNonANUser = False
        Dim clR As New clsDBO
        SQL = "select * from userid "
        SQL = SQL + "where userid in ("
        SQL = SQL + "select userid from usersecurityinfo where teamid <> 2 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            CheckNonANUser = True
        End If
    End Function

    Public Function GetClaimNo(xClaimID As Long) As String
        GetClaimNo = ""
        Dim clR As New clsDBO
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & xClaimID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetClaimNo = clR.Fields("claimno")
        End If
    End Function

    Public Sub AddDeny(xClaimID As Long, xUserID As Long)
        Dim clR As New clsDBO
        SQL = "insert into claimassign "
        SQL = SQL + "(claimid, userid, clickdate, denied) "
        SQL = SQL + "values (" & xClaimID & ", "
        SQL = SQL & xUserID & ", "
        SQL = SQL & "'" & Now & "', "
        SQL = SQL & "1) "
        clR.RunSQL(SQL, sCON)
    End Sub

    Public Sub AddAccepted(xClaimID As Long, xUserID As Long)
        Dim clR As New clsDBO
        SQL = "insert into claimassign "
        SQL = SQL + "(claimid, userid, clickdate, accepted) "
        SQL = SQL + "values (" & xClaimID & ", "
        SQL = SQL & xUserID & ", "
        SQL = SQL & "'" & Now & "', "
        SQL = SQL & "1) "
        clR.RunSQL(SQL, sCON)
    End Sub

    Public Sub UpdateClaimAssign(xClaimID As Long, xUserID As Long)
        Dim clR As New clsDBO
        SQL = "update claim "
        SQL = SQL + "set assignedto = " & xUserID & " "
        SQL = SQL + "where claimid = " & xClaimID
        clR.RunSQL(SQL, sCON)
    End Sub
End Class
