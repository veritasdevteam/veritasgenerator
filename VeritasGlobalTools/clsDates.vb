﻿Public Class clsDates

    Private sStartDate As String
    Private sEndDate As String

    Public Property StartDate As String
        Get
            StartDate = sStartDate
        End Get
        Set(value As String)
            sStartDate = value
        End Set
    End Property

    Public Property EndDate As String
        Get
            EndDate = sEndDate
        End Get
        Set(value As String)
            sEndDate = value
        End Set
    End Property

    Public Sub SetDefault()
        sStartDate = Today
        sEndDate = Today.AddDays(-30)
    End Sub

    Public Sub SetDates(DateType As Long)
        sStartDate = Today
        If DateType = 0 Then
            sEndDate = Today.AddDays(-7)
        ElseIf DateType = 1 Then
            sEndDate = Today.AddDays(-30)
        ElseIf DateType = 2 Then
            sEndDate = Today.AddDays(-90)
        ElseIf DateType = 3 Then
            sEndDate = Today.AddYears(-1)
        Else
            sEndDate = Today.AddDays(-7)
        End If
    End Sub

    Public Function BetweenDates(sDate As String, DateType As Long) As String
        BetweenDates = ""
        Dim sTemp As String()
        If sDate = "All Time" Then
            Exit Function
        End If
        sTemp = sDate.Split("/")
        sStartDate = sTemp(0) & "/1/" & sTemp(1)
        sEndDate = CDate(sStartDate).AddMonths(1)

        If DateType = 0 Then
            BetweenDates = "and cd.datepaid >= '" & sStartDate & "' and cd.datepaid < '" & sEndDate & "' "
        Else
            BetweenDates = "and authdate >= '" & sStartDate & "' and authdate < '" & sEndDate & "' "
        End If
    End Function

End Class
