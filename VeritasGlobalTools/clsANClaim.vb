﻿Imports System.Net.Mail

Public Class clsANClaim
    Private sClaimID As String
    Private lActivityID As Long
    Private SQL As String
    Private sEmail As String
    Private sActivity As String
    Private sClaimNo As String
    Private lContractID As Long
    Private sAdvisorName As String
    Private lUserID As Long

    Public Sub ProcessANClaim(xClaimid As String, xActivityID As Long, xUserID As Long)
        lUserID = xUserID
        lActivityID = xActivityID
        sClaimID = xClaimid
        If Not CheckAN() Then
            Exit Sub
        End If
        GetActivity()
        SendEmail()
        SavetoDB()

    End Sub

    Private Sub SavetoDB()
        Dim sHeader As String
        Dim sBody As String
        Dim sSentDate As String
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & lContractID
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
        Else
            Exit Sub
        End If
        sHeader = sClaimNo
        sBody = "Claim Number: " & sClaimNo & vbCr
        sBody = sBody & "Contract No: " & clC.Fields("contractno") & vbCr
        sBody = sBody & "Customer Name: " & clC.Fields("fname") & " " & clC.Fields("lname") & vbCr
        sBody = sBody & "Vehicle Make and Model: " & clC.Fields("make") & " " & clC.Fields("model") & vbCr
        sBody = sBody & "VIN: " & clC.Fields("VIN") & vbCr
        sBody = sBody & vbCr & vbCr
        sBody = sBody & "Claim Activity has been changed to " & sActivity & vbCr
        sBody = sBody & vbCr & vbCr
        sBody = sBody + sAdvisorName & vbCr
        sBody = sBody + "Claims Adjuster" & vbCr
        sBody = sBody + "VERITAS GLOBAL PROTECTION" & vbCr
        sSentDate = Now
        SQL = "insert into claimanemail 
        (claimid, messageto, messageheader, messagebody, sentdate) 
        values ("
        SQL = SQL + sClaimID + ", "
        SQL = SQL + "'" & sEmail & "', "
        SQL = SQL + "'" + sHeader + "', "
        SQL = SQL + "'" + sBody + "', "
        SQL = SQL + "'" + sSentDate + "') "
        clC.RunSQL(SQL, sCON)
    End Sub

    Private Sub SendEmail()
        Dim sBody As String
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & lContractID
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
        Else
            Exit Sub
        End If
        Try
            GetUserName()
            Dim client As New SmtpClient("smtp.office365.com", 587)
            client.UseDefaultCredentials = False
            client.Credentials = New System.Net.NetworkCredential("info@veritasglobal.com", "SilverSurfer3")
            client.EnableSsl = True

            Dim mail As New MailMessage
            mail.To.Add(sEmail)
            mail.Bcc.Add("srhanna@veritasglobal.com")
            mail.From = New MailAddress("info@veritasglobal.com")
            mail.Subject = sClaimNo
            mail.IsBodyHtml = True
            sBody = "Claim Number: " & sClaimNo & "</br>"
            sBody = sBody & "Contract No: " & clC.Fields("contractno") & "</br>"
            sBody = sBody & "Customer Name: " & clC.Fields("fname") & " " & clC.Fields("lname") & "</br>"
            sBody = sBody & "Vehicle Make and Model: " & clC.Fields("make") & " " & clC.Fields("model") & "</br>"
            sBody = sBody & "VIN: " & clC.Fields("VIN") & "</br>"
            sBody = sBody & "</br></br>"
            sBody = sBody & "Claim Activity has been changed to " & sActivity & "</br>"
            sBody = sBody & "</br></br>"
            sBody = sBody & "Please upload final Repair Order to process payment request via the electronic claim’s website "
            sBody = sBody + "<a href="
            sBody = sBody + """https://veritasglobalclaim.com/autonation/"
            sBody = sBody + """ target=""_blank"">"
            sBody = sBody + "https://veritasglobalclaim.com/autonation/"
            sBody = sBody + "</a>"
            sBody = sBody & "</br></br>"

            sBody = sBody + "<b>" & sAdvisorName & "</b>" & "</br>"
            sBody = sBody + "Claims Adjuster" & "</br>"
            sBody = sBody + "VERITAS GLOBAL PROTECTION" & "</br>"

            mail.Body = sBody
            client.Send(mail)
        Catch ex As Exception
            Dim s As String
            s = ex.Message
        End Try

    End Sub
    Private Sub GetUserName()
        Dim clUI As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & lUserID & " "
        clUI.OpenDB(SQL, sCON)
        If clUI.RowCount > 0 Then
            clUI.GetRow()
            sAdvisorName = clUI.Fields("fname") & " " & clUI.Fields("lname")
        End If
    End Sub


    Private Sub GetActivity()
        Dim clR As New clsDBO
        SQL = "select * from claimactivity "
        SQL = SQL + "where claimactivityid = " & lActivityID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            sActivity = clR.Fields("activitydesc")
        End If
    End Sub

    Private Function CheckAN() As Boolean
        Dim clR As New clsDBO
        CheckAN = False
        SQL = "select cl.contractid, cl.claimno, dealerno, sccontactemail from claim cl 
        inner join servicecenter sc on cl.servicecenterid = sc.servicecenterid
        where cl.claimid = " & sClaimID & " 
        and dealerno like '2%' 
        and not sccontactemail is null "

        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            CheckAN = True
            lContractID = clR.Fields("contractid")
            sClaimNo = clR.Fields("claimno")
            sEmail = clR.Fields("sccontactemail")
        End If

    End Function
End Class
